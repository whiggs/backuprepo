      Add-LocalGroupMember -Group "administrators" -Member "LL_Main\wmh02" -confirm:$false
Add-LocalGroupMember -Group "Remote Desktop Users" -Member "LL_Main\wmh02" -confirm:$false
$RegPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
Set-ItemProperty $RegPath "AutoAdminLogon" -Value "0" -type String -force -confirm:$false
Remove-ItemProperty $RegPath "DefaultUsername" -force -confirm:$false
Remove-ItemProperty $RegPath "DefaultPassword" -force -confirm:$false
Remove-ItemProperty $RegPath -Name "ForceAutoLogon" -Force -confirm:$false
Set-ItemProperty $RegPath -Name "ForceUnlockLogon" -Value 0 -Force -confirm:$false
Remove-ItemProperty $RegPath "AutoLogonCount" -force -confirm:$false
