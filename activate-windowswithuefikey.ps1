function Use-RunAs
{
# Check if script is running as Adminstrator and if not use RunAs
# Use Check Switch to check if admin

param([Switch]$Check)

$IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()`
).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")

if ($Check) { return $IsAdmin }

if ($MyInvocation.ScriptName -ne "")
{
if (-not $IsAdmin)
{
try
{
$arg = "-file `"$($MyInvocation.ScriptName)`""
Start-Process "$psHome\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop'
}
catch
{
Write-Warning "Error - Failed to restart script with runas"
break
}
exit # Quit this session of powershell
}
}
else
{
Write-Warning "Error - Script must be saved as a .ps1 file first"
break
}
}
Use-RunAs
$ob = Get-WmiObject -Namespace root\cimv2 -Class SoftwareLicensingService -Property OA3xOriginalProductKey
Write-host "The embedded product key for this machine is $($ob.OA3xOriginalProductKey)"
Start-Process "cscript.exe" -ArgumentList "slmgr.vbs -ipk $($ob.OA3xOriginalProductKey)" -WindowStyle Hidden -Wait
Start-Process "cscript.exe" -ArgumentList "slmgr.vbs -ato" -WindowStyle Hidden -Wait
Write-host "The embedded product key has been installed and windows has been activated."
Start-Sleep 3