      function Use-RunAs 
{    
    # Check if script is running as Adminstrator and if not use RunAs 
    # Use Check Switch to check if admin 
     
    param([Switch]$Check) 
     
    $IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()` 
        ).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") 
         
    if ($Check) { return $IsAdmin }     
 
    if ($MyInvocation.ScriptName -ne "") 
    {  
        if (-not $IsAdmin)  
        {  
            try 
            {  
                $arg = "-file `"$($MyInvocation.ScriptName)`"" 
                Start-Process "$psHome\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop'  
            } 
            catch 
            { 
                Write-Warning "Error - Failed to restart script with runas"  
                break               
            } 
            exit # Quit this session of powershell 
        }  
    }  
    else  
    {  
        Write-Warning "Error - Script must be saved as a .ps1 file first"  
        break  
    }  
}
Use-RunAs
Try
    {
        Install-module Carbon -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Carbon -Force -AllowClobber
}
Try
    {
        Install-module Autoload -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Autoload -Force -AllowClobber
    }
    Try
    {
        Install-module Beaver -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Beaver -Force -AllowClobber
    }
    Try
    {
        Install-module BurntToast -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module BurntToast -Force -AllowClobber
    }
    Try
    {
        Install-module CertificatePS -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module CertificatePS -Force -AllowClobber
    }
    Try
    {
        Install-module cMDT -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module cMDT -Force -AllowClobber
    }
    Try
    {
        Install-module CustomizeWindows10 -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module CustomizeWindows10 -Force -AllowClobber
    }
    Try
    {
        Install-module DeployImage -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module DeployImage -Force -AllowClobber
    }
    Try
    {
        Install-module Dimmo -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Dimmo -Force -AllowClobber
    }
    Try
    {
        Install-module IseHg -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module IseHg -Force -AllowClobber
    }
    Try
    {
        Install-module ISEScriptingGeek -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module ISEScriptingGeek -Force -AllowClobber
    }
    Try
    {
        Install-module Kickstart -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Kickstart -Force -AllowClobber
    }
    Try
    {
        Install-module MarkdownPS -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module MarkdownPS -Force -AllowClobber
    }
    Try
    {
        Install-module OneDrive -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module OneDrive -Force -AllowClobber
    }
    Try
    {
        Install-module Pester -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Pester -Force -AllowClobber
    }
    Try
    {
        Install-module platyPS -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module platyPS -Force -AllowClobber
    }
    Try
    {
        Install-module PowerShellCookbook -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PowerShellCookbook -Force -AllowClobber
    }
    Try
    {
        Install-module PowerShellGet -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PowerShellGet -Force -AllowClobber
    }
    Try
    {
        Install-module PowerShellGetGUI -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PowerShellGetGUI -Force -AllowClobber
    }
    Try
    {
        Install-module PowerShellISE-preview -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PowerShellISE-preview -Force -AllowClobber
    }
    Try
    {
        Install-module Proxx.Toast -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Proxx.Toast -Force -AllowClobber
    }
    Try
    {
        Install-module PSBookmark -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSBookmark -Force -AllowClobber
    }
    Try
    {
        Install-module PSConfig -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSConfig -Force -AllowClobber
    }
    Try
    {
        Install-module Pscx -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Pscx -Force -AllowClobber
    }
    Try
    {
        Install-module PSDeploy -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSDeploy -Force -AllowClobber
    }
    Try
    {
        Install-module PSGist -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSGist -Force -AllowClobber
    }
    Try
    {
        Install-module PsGoogle -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PsGoogle -Force -AllowClobber
    }
    Try
    {
        Install-module PsISEProjectExplorer -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PsISEProjectExplorer -Force -AllowClobber
    }
    Try
    {
        Install-module PSReadline -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSReadline -Force -AllowClobber
    }
    Try
    {
        Install-module PSScriptAnalyzer -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSScriptAnalyzer -Force -AllowClobber
    }
    Try
    {
        Install-module PSSudo -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSSudo -Force -AllowClobber
    }
    Try
    {
        Install-module PSWindowsUpdate -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSWindowsUpdate -Force -AllowClobber
    }
    Try
    {
        Install-module PSWord -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSWord -Force -AllowClobber
    }
    Try
    {
        Install-module ISESteroids -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module ISESteroids -Force -AllowClobber
    }
    Try
    {
        Install-module SendEmail -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module SendEmail -Force -AllowClobber
    }
    Try
    {
        Install-module ShowUI -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module ShowUI -Force -AllowClobber
    }
    Try
    {
        Install-module WakeOnLan -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module WakeOnLan -Force -AllowClobber
    }
    Try
    {
        Install-module WindowsImageTools -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module WindowsImageTools -Force -AllowClobber
    }
    Try
    {
        Install-module Winformsps -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Winformsps -Force -AllowClobber
}
Install-Script -Name MessageCenter
Install-Script -Name ADLockoutViewer
Install-Script -Name Test-Credential
Install-Script -Name Update-Windows
Install-Script -Name Install-WinrmHttps
Install-Script -Name POSH-DiskHealth 
Install-Script -Name Get-ModuleConflict
Install-Script -Name Connect-Mstsc
Install-Script -Name Get-Parameter
Install-Script -Name New-PSSchtask
