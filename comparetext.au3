#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

; Script Start - Add your code below here
#include <Array.au3>
#include <File.au3>
#include <Console.au3>
#include <Constants.au3>
#Region --- CodeWizard generated code Start ---

;MsgBox features: Title=Yes, Text=Yes, Buttons=Yes and No, Icon=None
$clip = True
Cout ( "Would you like to use the text in your clipboard for comparison?" & @CRLF & "(Y)es or (N)o"  )
$cli = Getch ()
If $cli = "y" Then
	$arr = StringSplit ( ClipGet (), @CRLF, $STR_NOCOUNT )
	$clip = True
ElseIf $cli = "n" Then
	$one = FileOpenDialog ( "Select the first file", "", "All (*.*)" )
	$ext = _GetFilenameExt ( $one )
	$arr = FileReadToArray ( $one )
	$clip = False
Else
	Cout ( @CRLF & "You input an invalid response.  Application will now exit." )
	Exit
EndIf
Cout ( @CRLF )

$two = FileOpenDialog ( "Select the second file", "", "All (*.*)" )
Cout ("")
$ext2 = _GetFilenameExt ( $two )
$arr2 = FileReadToArray ( $two )
$search2 = _ArraySearch($arr, "; Script Start - Add your code below here")
	If @error Then
		SetError(0)
	Else
		_ArrayDelete($arr, "0-" & $search2)
	EndIf
$search = _ArraySearch ( $arr2, "; Script Start - Add your code below here" )
If @error Then
	SetError ( 0 )
Else
	_ArrayDelete ( $arr2, "0-" & $search )
EndIf

$hol = ""
$hol2 = ""
If $clip = True Then
	Cout ( "Reading text from clipboard." & @CRLF )
Else
	Cout ( "Reading text from " & _GetFilename ( $one ) & "." & _GetFilenameExt ( $one ) & @CRLF )
EndIf

For $i = 0 To Ubound ( $arr ) - 1 Step 1
	If StringIsSpace ( $arr[$i] ) Then
		$hol = $hol & $i & ";"
		ContinueLoop
	Else
		$arr[$i] = StringStripWS ( $arr[$i], 3 )
		ContinueLoop
	EndIf
Next
_ArrayDelete ( $arr, StringTrimRight ( $hol, 1 ) )
Cout ( "Reading text from " & _GetFilename ( $two ) & "." & _GetFilenameExt ( $two ) & @CRLF )
For $i = 0 To Ubound ( $arr2 ) - 1 Step 1
	If StringIsSpace ( $arr2[$i] ) Then
		$hol2 = $hol2 & $i & ";"
		ContinueLoop
	Else
		$arr2[$i] = StringStripWS ( $arr2[$i], 3 )
		ContinueLoop
	EndIf
Next
_ArrayDelete ( $arr2, StringTrimRight ( $hol2, 1 ) )


If Ubound ( $arr ) <> UBound ( $arr2 ) Then
	Cout ( "The files are not the same.  Range different" & @CRLF )
Else
	$same = True
	$where = 0
	$val1 = ""
	$val2 = ""
	For $i = 0 To Ubound ( $arr2 ) - 1 Step 1
		If $arr[$i] <> $arr2[$i] Then
			$same = False
			$where = $i
			$val1 = $arr[$i]
			$val2 = $arr2[$i]
			ExitLoop
		Else
			ContinueLoop
		EndIf
	Next
	If $same Then
		Cout ( "The files are the same" & @CRLF )
	Else
		Cout ( "The files are not the same.  Difference at " & $where & @CRLF & "Text in " & _GetFilename ( $one ) & "." & _GetFilenameExt ( $one ) & ":" & @CRLF & $val1 & @CRLF & "Text in " & _GetFilename ( $two ) & "." & _GetFilenameExt ( $two ) & ":" & @CRLF & $val2 & @CRLF )
	EndIf
EndIf
Cout ( "Would you like to save the files as new files with all empty lines and trailing and leading whitespace removed?" & @CRLF & "(Y)es or (N)o" & @CRLF )
$rep = Getch()
If $rep = "y" Then
	If $clip = True Then
		$save = FileSaveDialog ( "Save the first file", "", "Original File (*." & $ext2 & ")", 18 )
	Else
		$save = FileSaveDialog ( "Save the first file", "", "Original File (*." & $ext & ")", 18 )
	EndIf

	If $save <> "" Then
		_FileWriteFromArray ( $save, $arr )
	EndIf
	$save2 = FileSaveDialog ( "Save the second file", "", "Original File (*." & $ext2 & ")", 18 )
	If $save2 <> "" Then
		_FileWriteFromArray ( $save2, $arr2 )
	EndIf
Else
	Exit
EndIf


Func _GetFilename($sFilePath)
    Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
    Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
    If IsObj($oColFiles) Then
        For $oObjectFile In $oColFiles
            Return $oObjectFile.FileName
        Next
    EndIf
    Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilename


Func _GetFilenameExt($sFilePath)
    Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
    Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
    If IsObj($oColFiles) Then
        For $oObjectFile In $oColFiles
            Return $oObjectFile.Extension
        Next
    EndIf
    Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenameExtDialog ( "Select the second file", "", "All (*.*)" )
Cout ("")
$ext = _GetFilenameExt ( $one )
$ext2 = _GetFilenameExt ( $two )
$arr = FileReadToArray ( $one )
$arr2 = FileReadToArray ( $two )
$hol = ""
$hol2 = ""
Cout ( "Reading text from " & _GetFilename ( $one ) & "." & _GetFilenameExt ( $one ) & @CRLF )
For $i = 0 To Ubound ( $arr ) - 1 Step 1
	If StringIsSpace ( $arr[$i] ) Then
		$hol = $hol & $i & ";"
		ContinueLoop
	Else
		$arr[$i] = StringStripWS ( $arr[$i], 3 )
		ContinueLoop
	EndIf
Next
_ArrayDelete ( $arr, StringTrimRight ( $hol, 1 ) )
Cout ( "Reading text from " & _GetFilename ( $two ) & "." & _GetFilenameExt ( $two ) & @CRLF )
For $i = 0 To Ubound ( $arr2 ) - 1 Step 1
	If StringIsSpace ( $arr2[$i] ) Then
		$hol2 = $hol2 & $i & ";"
		ContinueLoop
	Else
		$arr2[$i] = StringStripWS ( $arr2[$i], 3 )
		ContinueLoop
	EndIf
Next
_ArrayDelete ( $arr2, StringTrimRight ( $hol2, 1 ) )


If Ubound ( $arr ) <> UBound ( $arr2 ) Then
	Cout ( "The files are not the same.  Range different" & @CRLF )
Else
	$same = True
	$where = 0
	$val1 = ""
	$val2 = ""
	For $i = 0 To Ubound ( $arr2 ) - 1 Step 1
		If $arr[$i] <> $arr2[$i] Then
			$same = False
			$where = $i
			$val1 = $arr[$i]
			$val2 = $arr2[$i]
			ExitLoop
		Else
			ContinueLoop
		EndIf
	Next
	If $same Then
		Cout ( "The files are the same" & @CRLF )
	Else
		Cout ( "The files are not the same.  Difference at " & $where & @CRLF & "Text in " & _GetFilename ( $one ) & "." & _GetFilenameExt ( $one ) & ":" & @CRLF & $val1 & @CRLF & "Text in " & _GetFilename ( $two ) & "." & _GetFilenameExt ( $two ) & ":" & @CRLF & $val2 & @CRLF )
	EndIf
EndIf
Cout ( "Would you like to save the files as new files with all empty lines and trailing and leading whitespace removed?" & @CRLF & "(Y)es or (N)o" & @CRLF )
$rep = Getch()
If $rep = "y" Then
	$save = FileSaveDialog ( "Save the first file", "", "Original File (*." & $ext & ")", 18 )
	If $save <> "" Then
		_FileWriteFromArray ( $save, $arr )
	EndIf
	$save2 = FileSaveDialog ( "Save the second file", "", "Original File (*." & $ext2 & ")", 18 )
	If $save2 <> "" Then
		_FileWriteFromArray ( $save2, $arr2 )
	EndIf
Else
	Exit
EndIf

