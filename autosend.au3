      #Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_Language=1033
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
; *** Start added by AutoIt3Wrapper ***
#include <AutoItConstants.au3>
; *** End added by AutoIt3Wrapper ***
; *** Start added by AutoIt3Wrapper ***
#include <MsgBoxConstants.au3>
; *** End added by AutoIt3Wrapper ***
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.15.0 (Beta)
 Author:        William Higgs

 Script Function:
	Provides a graphical user interface for the command line utility "Cmail", which lets one send emails via command line.  I wrote this
	specificially to reduced the ammount of time needed to send messages to potential employers.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <File.au3>
#include <Array.au3>
#include <GuiEdit.au3>
#include <WordEx.au3>
#include <GuiListBox.au3>
#include <Constants.au3>

Global $trans = False
_WordErrorHandlerRegister()

Global $oWordApp = _WordCreate("", 0, 0, 0)
Global $oDoc = $oWordApp.ActiveDocument
Global $oRange = $oDoc.Range
Global $oSpellCollection, $oAlternateWords
HotKeySet ( "^d", "stylish" )
$Form2 = GUICreate("Cmail email sender", 406, 462, 345, 299, BitOR($GUI_SS_DEFAULT_GUI,$DS_SETFOREGROUND), BitOR($WS_EX_TOPMOST,$WS_EX_WINDOWEDGE))
Global $handle = WinGetHandle ( $Form2 )
$Input1 = GUICtrlCreateInput("", 70, 32, 273, 21)
$Input2 = GUICtrlCreateInput("", 74, 88, 265, 21)
$Label1 = GUICtrlCreateLabel("Email Address", 128, 0, 130, 29)
GUICtrlSetFont(-1, 16, 400, 0, "MS Sans Serif")
$Label2 = GUICtrlCreateLabel("Job Title", 163, 56, 79, 29)
GUICtrlSetFont(-1, 16, 400, 0, "MS Sans Serif")
$Checkbox1 = GUICtrlCreateCheckbox("Resume", 64, 112, 89, 25)
$Checkbox2 = GUICtrlCreateCheckbox("References", 264, 112, 81, 25)
$Checkbox3 = GUICtrlCreateCheckbox("Character sheet", 64, 144, 105, 25)
$Checkbox4 = GUICtrlCreateCheckbox("Misc", 264, 144, 81, 25)
$Label4 = GUICtrlCreateLabel("Subject (optional)", 124, 168, 157, 29)
GUICtrlSetFont(-1, 16, 400, 0, "MS Sans Serif")
$Input3 = GUICtrlCreateInput("", 32, 200, 337, 21)
$Label3 = GUICtrlCreateLabel("Message (optional)", 116, 224, 172, 29)
GUICtrlSetFont(-1, 16, 400, 0, "MS Sans Serif")
$Edit1 = GUICtrlCreateEdit("", 32, 256, 345, 153, BitOR($ES_WANTRETURN, $WS_VSCROLL))
$Button1 = GUICtrlCreateButton("Send", 48, 416, 113, 33, $BS_NOTIFY)
GUICtrlSetCursor (-1, 0)
$Button2 = GUICtrlCreateButton("Cancel", 216, 416, 105, 33, $BS_NOTIFY)
GUICtrlSetCursor (-1, 0)
$aRect = _GUICtrlEdit_GetRECT($Edit1)
$aRect[0] += 10
$aRect[1] += 10
$aRect[2] -= 10
$aRect[3] -= 10
_GUICtrlEdit_SetRECT($Edit1, $aRect)
$Form1 = GUICreate("Spell check", 345, 251, 302, 218)
Global $handle2 = WinGetHandle ( $Form1 )
$ListBox1 = GUICtrlCreateList("", 8, 8, 137, 149, $LBS_NOTIFY + $WS_VSCROLL )
$ListBox2 = GUICtrlCreateList("", 200, 8, 137, 149, $LBS_NOTIFY + $WS_VSCROLL )
$Button3 = GUICtrlCreateButton("Send", 32, 201, 75, 25, $BS_NOTIFY)
GUICtrlSetCursor (-1, 0)
$Button4 = GUICtrlCreateButton("&Cancel", 240, 201, 75, 25, $BS_NOTIFY)
GUICtrlSetCursor (-1, 0)
$Button5 = GUICtrlCreateButton("Correct spelling", 128, 201, 83, 25, $BS_NOTIFY)
GUICtrlSetState(-1, $GUI_DISABLE)
GUICtrlSetCursor (-1, 0)
GUISetState( @SW_DISABLE, $Form1 )
GUISetState( @SW_SHOW, $Form2 )
While 1
	Global $nMsg = GUIGetMsg(1)
	Switch $nMsg[1]
		Case $Form2
			Switch $nMsg[0]
				Case $GUI_EVENT_CLOSE
					_Exit()
				Case $Button1
					If _GUICtrlEdit_GetTextLen ( $Edit1 ) = 0 Then
						_GUICtrlEdit_SetText ( $Edit1, "Hello," & @CRLF & "My name is William Higgs, and I am interested in applying for the " & GUICtrlRead ( $Input2 ) & " position that was posted online.  Attached is my resume, which also has my contact information.  Please reach out to me at your earliest convenience should you want to speak to me concerning the position.  Thank you." & @CRLF & "William Higgs" )
					EndIf
					_SpellCheck ()
				Case $Button2
					_Exit()
			EndSwitch
		Case $Form1
			Switch $nMsg[0]
				Case $GUI_EVENT_CLOSE
					GUISetState ( @SW_HIDE, $Form1 )
					GUISetState ( @SW_DISABLE, $Form1 )
					GUISwitch ( $Form2 )
					GUISetState ( @SW_ENABLE, $Form2 )
					GUISetState ( @SW_SHOW, $Form2 )
				Case $ListBox1
					_SpellingSuggestions()
				Case $ListBox2
					GUICtrlSetState($Button5, $GUI_ENABLE)
				Case $Button3
					SendMessage ()
				Case $Button4
					GUISetState ( @SW_HIDE, $Form1 )
					GUISetState ( @SW_DISABLE, $Form1 )
					GUISwitch ( $Form2 )
					GUISetState ( @SW_ENABLE, $Form2 )
					GUISetState ( @SW_SHOW, $Form2 )
				Case $Button5
					_ReplaceWord()
			EndSwitch
	EndSwitch
WEnd
Func stylish ()
	$thestyle = GUIGetStyle ( $handle )
	If $trans = False Then
		GUISetStyle ( -1, $thestyle[1] + 32, $handle )
		WinSetTrans ( $handle, "", 170 )
		$trans = True
	Else
		GUISetStyle ( -1, $thestyle[1] - 32, $handle )
		WinSetTrans ( $handle, "", 255 )
		$trans = False
	EndIf
EndFunc
Func GetHoveredHwnd()
    Local $iRet = DllCall("user32.dll", "int", "WindowFromPoint", "long", MouseGetPos(0), "long", MouseGetPos(1))
    If IsArray($iRet) Then Return HWnd($iRet[0])
    Return SetError(1, 0, 0)
EndFunc
Func _SpellCheck()
	Local $sText, $tText, $sWord

	$sText = GUICtrlRead($Edit1)
	$oRange = $oWordApp.ActiveDocument.Range
	$oRange.Delete
	$oRange.InsertAfter($sText)
	_SetLanguage()

	$oSpellCollection = $oRange.SpellingErrors
	If $oSpellCollection.Count > 0 Then
		If BitAND ( WinGetState ( $handle ), 2 ) Then
			If $trans = True Then
				stylish ()
			EndIf
			GUISetState ( @SW_DISABLE, $Form2 )
			GUISetState ( @SW_HIDE, $Form2 )
			GUISwitch ( $Form1 )
			GUISetState ( @SW_SHOW, $Form1 )
			GUISetState ( @SW_ENABLE, $Form1 )
		EndIf

			;
		_GUICtrlListBox_ResetContent($ListBox1)
		_GUICtrlListBox_ResetContent($ListBox2)
		GUICtrlSetState($Button5, $GUI_DISABLE)
			For $i = 1 To $oSpellCollection.Count
				$sWord = $oSpellCollection.Item($i).Text
				_GUICtrlListBox_AddString($ListBox1, $sWord)
			Next
		GUICtrlSetData($Edit1, $oRange.Text)
	Else
		SendMessage ()
	EndIf
EndFunc   ;==>_SpellCheck

Func _SpellingSuggestions()
	Local $iWord, $sWord
	;
	_GUICtrlListBox_ResetContent($ListBox2)
	GUICtrlSetState($Button5, $GUI_DISABLE)

	$iWord = _GUICtrlListBox_GetCurSel($ListBox1) + 1
	$sWord = $oSpellCollection.Item($iWord).Text

	$oAlternateWords = $oWordApp.GetSpellingSuggestions($sWord)
	If $oAlternateWords.Count > 0 Then
		For $i = 1 To $oAlternateWords.Count
			_GUICtrlListBox_AddString($ListBox2, $oAlternateWords.Item($i).Name)
		Next
	Else
		_GUICtrlListBox_AddString($ListBox2, "No suggestions.")
	EndIf
EndFunc   ;==>_SpellingSuggestions

Func _HighlightWord()
	Local $sText, $iWord, $sWord, $iEnd, $iStart
	;
	$iWord = _GUICtrlListBox_GetCurSel($ListBox1) + 1
	$sWord = $oSpellCollection.Item($iWord).Text
	$sText = $oRange.Text

	$iStart = ($oSpellCollection.Item($iWord).Start)
	$iEnd = ($oSpellCollection.Item($iWord).End)
	_GUICtrlEdit_SetSel($Edit1, $iStart, $iEnd)
EndFunc   ;==>_HighlightWord

Func _ReplaceWord()
	Local $iWord, $iNewWord, $sWord, $sNewWord, $sText, $sNewText
	;
	$iWord = _GUICtrlListBox_GetCurSel($ListBox1) + 1
	$iNewWord = _GUICtrlListBox_GetCurSel($ListBox2) + 1
	If $iWord == $LB_ERR Or $iNewWord == $LB_ERR Then
		MsgBox(48, "Error", "You must first select a word to replace, then a replacement word.")
		Return
	EndIf
	$oSpellCollection.Item($iWord).Text = $oAlternateWords.Item($iNewWord).Name

	GUICtrlSetData($Edit1, $oRange.Text)

	_SpellCheck()

	GUICtrlSetState($Button5, $GUI_DISABLE)
EndFunc   ;==>_ReplaceWord

Func _SetLanguage()
	$sLang = "English"
	If $sLang <> "" Then
		$oWordApp.CheckLanguage = False
		$WdLangID = Number(1033)

		If $WdLangID Then
			With $oRange
				.LanguageID = $WdLangID
				.NoProofing = False
			EndWith
		EndIf
	Else
		$oWordApp.CheckLanguage = True
	EndIf
EndFunc   ;==>_SetLanguage

Func SendMessage ()
	If BitAND ( WinGetState ( $handle2 ), 2 ) Then
		GUISetState ( @SW_HIDE, $Form1 )
		GUISetState ( @SW_DISABLE, $Form1 )
		GUISwitch ( $Form2 )
		GUISetState ( @SW_ENABLE, $Form2 )
		GUISetState ( @SW_SHOW, $Form2 )
	EndIf
	$body = GUICtrlRead ( $Edit1 )
	_GUICtrlEdit_SetText ( $Edit1, "" )

	$attach = Null
	If GUICtrlRead ( $Checkbox1 ) = 1 Then
		$attach = $attach & '"C:\Users\whiggs\OneDrive\Documents\Matt_Real_Resume(revised)_career.docx" '
	EndIf
	If GUICtrlRead ( $Checkbox2 ) = 1 Then
		$attach = $attach & '"C:\Users\whiggs\OneDrive\Documents\Professional_references_shorter.docx" '
	EndIf
	If GUICtrlRead ( $Checkbox3 ) = 1 Then
		$attach = $attach & '"C:\Users\whiggs\OneDrive\Documents\__JOB_STRENGTHS.docx" "C:\Users\whiggs\OneDrive\Documents\3CiConsultant-Mindset-Completed.pdf" '
	EndIf
	If GUICtrlRead ( $Checkbox4 ) = 1 Then
		$attach = $attach & '"C:\Users\whiggs\OneDrive\Documents\Copy_of_Will.xlsx" "C:\Users\whiggs\OneDrive\Documents\Copy_of_Will_(Nov).xlsx" "C:\Users\whiggs\OneDrive\Documents\Copy_of_Will_Scorecard_2015.xlsx" "C:\Users\whiggs\OneDrive\Documents\example1.pdf" "C:\Users\whiggs\OneDrive\Documents\example2.pdf" "C:\Users\whiggs\OneDrive\Documents\example3.pdf" '
	EndIf
	If GUICtrlRead ( $Checkbox1 ) = 1 Or GUICtrlRead ( $Checkbox2 ) = 1 Or GUICtrlRead ( $Checkbox3 ) = 1 Or GUICtrlRead ( $Checkbox4 ) = 1 Then
		$attach = StringStripWS ( $attach, 2 )
	EndIf

	;$attach = StringStripCR ( $attach )
	If StringLen ( GUICtrlRead ( $Input3 ) ) = 0 Then
		$subject = "Interested in the posted position: " & GUICtrlRead ( $Input2 )
	Else
		$subject = GUICtrlRead ( $Input3 )
	EndIf
	If $attach <> Null Then
		$split = StringSplit ( $attach, ' ' )
		_ArrayDisplay ( $split )
		If @error Then
			SetError ( 0 )
			$attach = '-a:' & $attach & ' '
		Else
			$attach = Null
			For $i = 1 To $split[0] Step 1
				$attach = $attach & '-a:' & $split[$i] & ' '
			Next
		EndIf
	EndIf
	If $attach = Null Then
		$proc = Run ( 'CMail -from:whiggs.ITPRO@gmail.com:"William Higgs" -to:' & GUICtrlRead ( $Input1 ) & ' -subject:"' & $subject & '" -body:"' & $body & '" -host:whiggs.ITPRO@gmail.com:passwordhere@smtp.gmail.com:587 -starttls -requiretls -d', 'C:\ProgramData\chocolatey\bin', @SW_SHOW, $STDOUT_CHILD )
	Else
		$proc = Run ( 'CMail -from:whiggs.ITPRO@gmail.com:"William Higgs" -to:' & GUICtrlRead ( $Input1 ) & ' -subject:"' & $subject & '" -body:"' & $body & '" ' & $attach & '-host:whiggs.ITPRO@gmail.com:passwordhere@smtp.gmail.com:587 -starttls -requiretls -d', 'C:\ProgramData\chocolatey\bin', @SW_SHOW, $STDOUT_CHILD )
	EndIf

	ProcessWaitClose ( $proc )
	$text = StdoutRead ( $proc )
	ClipPut ( $text )
	MsgBox($MB_OK + $MB_ICONASTERISK,"Sent!","Message Sent!!")
EndFunc

Func _Exit()
	_WordQuit($oWordApp, 0)
	Exit
EndFunc   ;==>_Exit
