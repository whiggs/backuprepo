      #this script snippet shows how you can successfully register a vsts package management feed as your own private powershell gallery, allowing you to publish modules to and install
#modules from VSTS as you would the public powershell gallery.  

#region Only needs to be run first time

#install nuget commandline tool with chocolatey
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco install nuget.commandline
#install and import popwershellget module version 1.5.0.  Latest version doesn't work for some reason
Install-Module Powershellget -RequiredVersion 1.5.0 -Force
Remove-Module powershellget -Force
Import-Module powershellget -RequiredVersion 1.5.0.0 -Force
Install-module Packagemanagement -allowclobber -force
#path to the folder containing the module you wish to publish
$module = "C:\Users\wmh02\Documents\GitHub\Technology%20Operations\EE PowerShell\DepartModule"
#whatever you want to name the repository to reference in powershell when installing the module
$repository = "Example"
#in vsts, if you hover over your user icon in top right corner of screen and select "security" from the drop down menu, you can generate a personal access token, which can act as login
#credentials to access vsts resources with all or specifically selected permissions associated with your user account.  Generate a PAT with the needed permissions (I just keep
#all of the permissions to make it simple) and assign the description and access key to the variables below.
$user = "<PAT Description>"
$pat = "<PAT KEY>"
#this is the part that gets tricky.  If you go to a feed in VSTS and select "connect to feed", the package source url actually reads 
#"https://<company>.pkgs.visualstudio.com/_packaging/EE/nuget/v3/".  However, from what I understand, the powershellget and packagemanagement cmdlets are not compatable
#with version 3 nuget packages as of now, so in order for this to work, the source url needs to be registered as a feed which hosts version 2 nuget packages.
$sourceurl = "https://<company>.pkgs.visualstudio.com/_packaging/EE/nuget/v2/"
#convert username and PAT generated in vsts into a credential object so it can be passed to the powershell get cmdlets.
$password = ConvertTo-SecureString -String $PAT -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential ($user, $password)

Get-PackageProvider -Name 'NuGet' -ForceBootstrap -Force
#configuring nuget by adding vsts as a package source for both publishing and retrieving packages
& nuget.exe Sources Add -Name $repository -Source $SourceUrl -Username $User -Password $pat -storePasswordInClearText

# Check new NuGet Source is registered
& nuget.exe Sources List
#register the vsts repository for use in powershellget
Register-PSRepository -Name $repository -SourceLocation $sourceurl -PublishLocation $sourceurl -InstallationPolicy Trusted -Credential $credential -PackageManagementProvider Nuget -Verbose
# Check new PowerShell Repository is registered
Get-PSRepository -Name $repository
#endregion Only needs to be run first time

#Publish the module.
Publish-Module -Path $module -Repository $repository -NuGetApiKey "VSTS" -Force -Verbose
