      #Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Change2CUI=y
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Res_requestedExecutionLevel=highestAvailable
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.15.0 (Beta)
 Author:  William Higgs

 Script Function:
	This script is meant to scan the html bookmark file exported by any internet browser, scan the file for any duplicate bookmark items,
	remove them, cleanup any empty leftover groups, then create a new html bookmark file that can then be imported back into your browser,
	removing all duplicate bookmark links.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <Array.au3>
#include <Constants.au3>
$open = FileOpenDialog ( "Select html containing exported bookmarks", "", "HTML file (*.html)", 3 )
$save = FileSaveDialog ( "Where do you want to save modified bookmark file?", "", "HTML file (*.html)", 18 )
ProgressOn ( "Please wait", "Scanning bookmark file for bookmarks", "", Default, Default, 18 )
$oIE = FileReadToArray ( $open )
$num = @extended
Local $old[$num]
For $h = 0 To $num - 1 Step 1
	$old[$h] = StringStripWS ( $oIE[$h], 3 )
Next
Local $old2[$num]
$num2 = 0
For $i = 0 To $num - 1 Step 1
	If StringCompare ( StringLeft ( $old[$i], 6 ), "<DT><A" ) = 0 Then
		$begin = StringInStr ( $old[$i], '"' )
		$end = StringInStr ( $old[$i], '"', Default, 2 )
		$old2[$i] = StringMid ( $old[$i], $begin + 1, $end - $begin )
	Else
		$old2[$i] = "NA"
	EndIf
Next
$index = _ArrayFindAll ( $old2, "NA" )
Local $temp[UBound ( $index ) + 1]
$temp[0] = UBound ( $index )
For $i = 0 To UBound ( $index ) - 1 Step 1
	$temp[$i+1] = $index[$i]
Next

$newnum = _ArrayDelete ( $old2, $temp )
$old2 = _ArrayUnique ( $old2 )

For $i = 1 To $old2[0] Step 1
	ProgressSet ( ( $i / $old2[0] ) * 100, $i & " out of " & $old2[0], "Removing duplicates..." )
	$indexi = _ArrayFindAll ( $old, $old2[$i], Default, Default, Default, 1 )
	If UBound ( $indexi ) > 1 Then
		Local $delete[UBound ( $indexi )]
		$delete[0] = UBound ( $indexi ) - 1
		For $t = 1 To UBound ( $indexi ) - 1 Step 1
			$delete[$t] = $indexi[$t]
		Next
		_ArrayDelete ( $old, $delete )
	Else
		ContinueLoop
	EndIf
Next

ProgressSet ( 0, "Will begin shortly.", "Cleaning up the file." )
cleanup ( $old )

$hold = 0
$file = FileOpen ( $save, 2 )
ProgressSet ( 0, "Will begin shortly.", "Writing new bookmarks file." )
For $i = 0 To UBound ( $old ) - 1 Step 1
	ProgressSet ( ( $i / UBound ( $old ) - 1 ) * 100, $i & " out of " & UBound ( $old ) - 1, "Writing new bookmarks file." )
	If StringCompare ( StringLeft ( $old[$i], 7 ), "<DL><p>" ) = 0 Then
		If $hold = 0 Then
			FileWriteLine ( $file, $old[$i] )
			$hold += 1
		Else
			For $o = 1 To $hold Step 1
				FileWrite ( $file, @TAB )
			Next
			FileWriteLine ( $file, $old[$i] )
			$hold += 1
		EndIf
	ElseIf StringCompare ( StringLeft ( $old[$i], 8 ), "</DL><p>" ) = 0 Then
		If $hold = 1 Then
			$hold -= 1
			FileWriteLine ( $file, $old[$i] )
		Else
			$hold -= 1
			For $o = 1 To $hold Step 1
				FileWrite ( $file, @TAB )
			Next
			FileWriteLine ( $file, $old[$i] )
		EndIf
	Else
		If $hold = 0 Then
			FileWriteLine ( $file, $old[$i] )
		Else
			For $o = 1 To $hold Step 1
				FileWrite ( $file, @TAB )
			Next
			FileWriteLine ( $file, $old[$i] )
		EndIf
	EndIf
Next
FileClose ( $file )
ProgressOff ()
#Region --- CodeWizard generated code Start ---

;MsgBox features: Title=Yes, Text=Yes, Buttons=OK, Icon=Info
MsgBox($MB_OK + $MB_ICONASTERISK,"Finished",'Your new bookmark html file can be found at "' & $save & '" and imported back into your browser.')
#EndRegion --- CodeWizard generated code End ---


Func cleanup ( ByRef $the )
	$restart = False
	For $i = 0 To UBound ( $the ) - 1 Step 1
		If StringCompare ( StringLeft ( $the[$i], 8 ), "<DT><H3>" ) = 0 Then
			If StringCompare ( StringLeft ( $the[$i+2], 8 ), "</DL><p>" ) = 0 Then
				_ArrayDelete ( $the, $i & "-" & $i+2 )
				$restart = True
				ExitLoop
			Else
				ContinueLoop
			EndIf
		Else
			ContinueLoop
		EndIf
	Next
	If $restart = True Then
		cleanup ( $the )
	EndIf
EndFunc

