      $Boxstarter.RebootOk=$true # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot
Update-ExecutionPolicy Unrestricted
Install-WindowsUpdate -getUpdatesFromMS -acceptEula -SuppressReboots
if (Test-PendingReboot) { Invoke-Reboot }
#Invoke-ChocolateyBoxstarter https://gist.githubusercontent.com/DemonDante/a864d503af61a439064326d339844cb7/raw/bd677b52d5fd54d44ed6af256379188c944b7cd2/workchocoinstalls.txt
#if (Test-PendingReboot) { Invoke-Reboot }
