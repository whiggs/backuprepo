      #cs
This script is the script that I used to correct the Windows Activation errors on the tablet devices to which I deployed Windows 10
for the Paravue project when I worked at Merial.  The issue has to do with the fact that, starting with devices pre-installed with
Windows 8, the OEM product key for Microsoft Windows is embedded in the device's BIOS.  However, this requires that the device boot
into the pre-installed operating system and activate Windows at least once before installing a new operating system on the device.
However, since I was deploying 75 + tablets a week with a new operating system, Windows would fail to activate on the device because
I had not boot into the pre-installed os and activated Windows prior to deploying the OS.  This script resolves this problem, as
it uses Windows Management Instrumentation to query the embedded OEM product key from the bios, and then installs the key to the
operating system, and activates windows with it.  Was also able to deploy it to all of the other devices I had previously completed
using Mirosoft Intune.
#ce

$proc = Run ( "wmic path softwarelicensingservice get OA3xOriginalProductKey", @SystemDir, Default, $STDOUT_CHILD )
ProcessWaitClose ( $proc )
$text = StdoutRead ( $proc )
$one = StringInStr ( $text, "-" )
$key = StringMid ( $text, Int ( $one ) - 5, 29 )
RunWait ( @ComSpec & ' /c cscript //B "' & @WindowsDir & '\system32\slmgr.vbs" -ipk ' & $key )
RunWait ( @ComSpec & ' /c cscript //B "' & @WindowsDir & '\system32\slmgr.vbs" /ato' )
