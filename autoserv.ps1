      function Use-RunAs 
{    
    # Check if script is running as Adminstrator and if not use RunAs 
    # Use Check Switch to check if admin 
     
    param([Switch]$Check) 
     
    $IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()` 
        ).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") 
         
    if ($Check) { return $IsAdmin }     
 
    if ($MyInvocation.ScriptName -ne "") 
    {  
        if (-not $IsAdmin)  
        {  
            try 
            {  
                $arg = "-file `"$($MyInvocation.ScriptName)`"" 
                Start-Process "$psHome\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop'  
            } 
            catch 
            { 
                Write-Warning "Error - Failed to restart script with runas"  
                break               
            } 
            exit # Quit this session of powershell 
        }  
    }  
    else  
    {  
        Write-Warning "Error - Script must be saved as a .ps1 file first"  
        break  
    }  
} 
 
 function Test-LocalCredential {
    [CmdletBinding()]
    Param
    (
        [string]$UserName,
        [string]$ComputerName = $env:COMPUTERNAME,
        [string]$Password
    )
    if (!($UserName) -or !($Password)) {
        Write-Warning 'Test-LocalCredential: Please specify both user name and password'
    } else {
        Add-Type -AssemblyName System.DirectoryServices.AccountManagement
        $DS = New-Object System.DirectoryServices.AccountManagement.PrincipalContext('machine',$ComputerName)
        $DS.ValidateCredentials($UserName, $Password)
    }
}
 
 Function Get-PendingReboot
{
<#

#>

[CmdletBinding()]
param(
	[Parameter(Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
	[Alias("CN","Computer")]
	[String[]]$ComputerName="$env:COMPUTERNAME",
	[String]$ErrorLog
	)

Begin {  }## End Begin Script Block
Process {
  Foreach ($Computer in $ComputerName) {
	Try {
	    ## Setting pending values to false to cut down on the number of else statements
	    $CompPendRen,$PendFileRename,$Pending,$SCCM = $false,$false,$false,$false
                        
	    ## Setting CBSRebootPend to null since not all versions of Windows has this value
	    $CBSRebootPend = $null
						
	    ## Querying WMI for build version
	    $WMI_OS = Get-WmiObject -Class Win32_OperatingSystem -Property BuildNumber, CSName -ComputerName $Computer -ErrorAction Stop

	    ## Making registry connection to the local/remote computer
	    $HKLM = [UInt32] "0x80000002"
	    $WMI_Reg = [WMIClass] "\\$Computer\root\default:StdRegProv"
						
	    ## If Vista/2008 & Above query the CBS Reg Key
	    If ([Int32]$WMI_OS.BuildNumber -ge 6001) {
		    $RegSubKeysCBS = $WMI_Reg.EnumKey($HKLM,"SOFTWARE\Microsoft\Windows\CurrentVersion\Component Based Servicing\")
		    $CBSRebootPend = $RegSubKeysCBS.sNames -contains "RebootPending"		
	    }
							
	    ## Query WUAU from the registry
	    $RegWUAURebootReq = $WMI_Reg.EnumKey($HKLM,"SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\")
	    $WUAURebootReq = $RegWUAURebootReq.sNames -contains "RebootRequired"
						
	    ## Query PendingFileRenameOperations from the registry
	    $RegSubKeySM = $WMI_Reg.GetMultiStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\Session Manager\","PendingFileRenameOperations")
	    $RegValuePFRO = $RegSubKeySM.sValue

	    ## Query JoinDomain key from the registry - These keys are present if pending a reboot from a domain join operation
	    $Netlogon = $WMI_Reg.EnumKey($HKLM,"SYSTEM\CurrentControlSet\Services\Netlogon").sNames
	    $PendDomJoin = ($Netlogon -contains 'JoinDomain') -or ($Netlogon -contains 'AvoidSpnSet')

	    ## Query ComputerName and ActiveComputerName from the registry
	    $ActCompNm = $WMI_Reg.GetStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\ComputerName\ActiveComputerName\","ComputerName")            
	    $CompNm = $WMI_Reg.GetStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName\","ComputerName")

	    If (($ActCompNm -ne $CompNm) -or $PendDomJoin) {
	        $CompPendRen = $true
	    }
						
	    ## If PendingFileRenameOperations has a value set $RegValuePFRO variable to $true
	    If ($RegValuePFRO) {
		    $PendFileRename = $true
	    }

	    ## Determine SCCM 2012 Client Reboot Pending Status
	    ## To avoid nested 'if' statements and unneeded WMI calls to determine if the CCM_ClientUtilities class exist, setting EA = 0
	    $CCMClientSDK = $null
	    $CCMSplat = @{
	        NameSpace='ROOT\ccm\ClientSDK'
	        Class='CCM_ClientUtilities'
	        Name='DetermineIfRebootPending'
	        ComputerName=$Computer
	        ErrorAction='Stop'
	    }
	    ## Try CCMClientSDK
	    Try {
	        $CCMClientSDK = Invoke-WmiMethod @CCMSplat
	    } Catch [System.UnauthorizedAccessException] {
	        $CcmStatus = Get-Service -Name CcmExec -ComputerName $Computer -ErrorAction SilentlyContinue
	        If ($CcmStatus.Status -ne 'Running') {
	            Write-Warning "$Computer`: Error - CcmExec service is not running."
	            $CCMClientSDK = $null
	        }
	    } Catch {
	        $CCMClientSDK = $null
	    }

	    If ($CCMClientSDK) {
	        If ($CCMClientSDK.ReturnValue -ne 0) {
		        Write-Warning "Error: DetermineIfRebootPending returned error code $($CCMClientSDK.ReturnValue)"          
		    }
		    If ($CCMClientSDK.IsHardRebootPending -or $CCMClientSDK.RebootPending) {
		        $SCCM = $true
		    }
	    }
            
	    Else {
	        $SCCM = $null
	    }

	    ## Creating Custom PSObject and Select-Object Splat
	    $SelectSplat = @{
	        Property=(
	            'Computer',
	            'CBServicing',
	            'WindowsUpdate',
	            'CCMClientSDK',
	            'PendComputerRename',
	            'PendFileRename',
	            'PendFileRenVal',
	            'RebootPending'
	        )}
	    New-Object -TypeName PSObject -Property @{
	        Computer=$WMI_OS.CSName
	        CBServicing=$CBSRebootPend
	        WindowsUpdate=$WUAURebootReq
	        CCMClientSDK=$SCCM
	        PendComputerRename=$CompPendRen
	        PendFileRename=$PendFileRename
	        PendFileRenVal=$RegValuePFRO
	        RebootPending=($CompPendRen -or $CBSRebootPend -or $WUAURebootReq -or $SCCM -or $PendFileRename)
	    } | Select-Object @SelectSplat

	} Catch {
	    Write-Warning "$Computer`: $_"
	    ## If $ErrorLog, log the file to a user specified location/path
	    If ($ErrorLog) {
	        Out-File -InputObject "$Computer`,$_" -FilePath $ErrorLog -Append
	    }				
	}			
  }## End Foreach ($Computer in $ComputerName)			
}## End Process

End {  }## End End

}## End Function Get-PendingReboot
 
 
# Code goes at very bottom
Use-RunAs
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
$log = $env:APPDATA + '\hold.txt'
If (!(Test-path -LiteralPath $log)) {
  DISM /Online /Enable-Feature /FeatureName:NetFx3 /All
  $text = "1"
  $text | out-file $log }
$ret = Get-Content $log
If ($ret -eq "1") {
echo "cd /d %~dp0" 'PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command ".\boxstarter.ps1"' | Out-file -filepath "$env:APPDATA\go.bat" -Encoding ascii
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
C:\ProgramData\chocolatey\helpers\functions\Update-SessionEnvironment.ps1
choco install boxstarter -y
choco install dotnet4.5 -y
$obj = Get-WmiObject -Class "Win32_TerminalServiceSetting" -Namespace root\cimv2\terminalservices
    $obj.SetAllowTsConnections(1,1) | out-null
    $obj2 = Get-WmiObject -class Win32_TSGeneralSetting -Namespace root\cimv2\terminalservices -ComputerName . -Filter "TerminalName='RDP-tcp'"
$obj2.SetUserAuthenticationRequired(0) | out-null
choco install powershell -y
del $log
$text = "2"
  $text | out-file $log
Restart-Computer -Force
}
ElseIf ($ret -eq "2") {
        Get-PackageProvider -Name NuGet -ForceBootstrap
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
    Install-module Carbon
    Set-RegistryKeyValue -Path 'hkcu:\Software\Microsoft\Windows\CurrentVersion\Run' -Name "deploy" -String '%APPDATA%\go.bat' -Expand
    Install-Module PSWindowsupdate
    Add-WUServiceManager -ServiceID "7971f918-a847-4430-9279-4a52d1efe18d" -erroraction SilentlyContinue
    $text = "3"
$text | Out-file $log
Restart-Computer -force
}
ElseIf ($ret -eq "3") {
    Get-WUInstall -MicrosoftUpdate -NotCategory "Driver" -AutoReboot -AcceptAll
    Start-Sleep 5
    del $log
$text = "4"
$text | Out-file $log
Restart-Computer -force
}

Elseif ($ret -eq "4") {
    Install-module Autoload
Install-module Beaver
Install-module BurntToast
Install-module CertificatePS
Install-module cMDT
Install-module CustomizeWindows10
Install-module DeployImage
Install-module Dimmo
Install-module IseHg
Install-module ISEScriptingGeek
Install-module Kickstart
Install-module MarkdownPS
Install-module OneDrive
Install-module Pester -Force
Install-module platyPS
Install-module PowerShellCookbook
Install-module PowerShellGet
Install-module PowerShellGetGUI
Install-module PowerShellISE-preview
Install-module Proxx.Toast
Install-module PSBookmark
Install-module PSConfig
Install-module Pscx
Install-module PSDeploy
Install-module PSGist
Install-module PsGoogle
Install-module PsISEProjectExplorer
Install-module PSReadline -Force
Install-module PSScriptAnalyzer
Install-module PSSudo
Install-module PSWord
Install-module ISESteroids
Install-module SendEmail
Install-module WakeOnLan
Install-module WindowsImageTools
del $log
$text = "5"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "5") {
Invoke-WebRequest "https://www.autoitscript.com/files/autoit3/autoit-v3-setup.exe" -OutFile "$env:USERPROFILE\Downloads\autoita.exe"
Invoke-WebRequest "https://www.autoitscript.com/autoit3/files/beta/autoit/autoit-v3.3.15.0-beta-setup.exe" -OutFile "$env:USERPROFILE\Downloads\autoitb.exe"
Invoke-WebRequest "https://www.autoitscript.com/autoit3/scite/download/SciTE4AutoIt3.exe" -OutFile "$env:USERPROFILE\Downloads\scite.exe"
Start-Process -FilePath "$env:USERPROFILE\Downloads\autoita.exe" -ArgumentList "/S" -Wait
Start-Process -FilePath "$env:USERPROFILE\Downloads\autoitb.exe" -ArgumentList "/S" -Wait
Start-Process -FilePath "$env:USERPROFILE\Downloads\scite.exe" -ArgumentList "/S" -Wait
Invoke-Webrequest "https://gist.githubusercontent.com/DemonDante/6fdd9962040ddfea6c095d5feb2450e8/raw/8a48807c8bb68515c123655a564afcc269840325/uvk.au3" -outfile "$env:APPDATA\uvk.au3"
    Start-Process -Filepath "${env:ProgramFiles(x86)}\AutoIt3\Aut2Exe\Aut2exe.exe" -ArgumentList "/in $env:APPDATA\uvk.au3" -Wait
Start-Process -FilePath "$env:APPDATA\uvk.exe" -Wait
del $log
$text = "6"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "6") {
     $another = "http://www.carifred.com/tech_tool_store/TechToolStore.exe"
$outward = $env:USERPROFILE + '\Downloads\toolstore.exe'
Invoke-WebRequest $another -OutFile $outward
Start-Process -FilePath $outward -Wait
 del $log
$text = "7"
$text | Out-file $log
Restart-Computer -force
}
ElseIf ($ret -eq "7") {
    choco install sysinternals -y
choco install adobereader -y
choco install lockhunter -y
choco install sudo -y
choco install conemu -y
del $log
$text = "done"
$text | Out-file $log
Restart-Computer -force
}
Else {
    Add-Type -AssemblyName System.Windows.Forms

$mess = [System.Windows.Forms.MessageBox]::Show('Installation complete')
If (Test-Path -Path "$env:APPDATA\select.txt") {Set-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name EnableLUA  -Value 1}
Remove-RegistryKeyValue -Path 'hkcu:\Software\Microsoft\Windows\CurrentVersion\Run' -Name "deploy"
}
