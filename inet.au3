      #Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Add_Constants=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.15.0 (Beta)
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <IE.au3>

$oIE = _IECreate ( "http://ninite.com/firefox-chrome-operaChromium-klitecodecs-spotify-cccp-java8-.net4.6.2-silverlight-air-shockwave-irfanview-xnview-greenshot-sharex-foxit-sumatrapdf-cutepdf-pdfcreator-malwarebytes-dropbox-googledrive-bittorrentsync-evernote-keepass2-everything-revo-glary-realvnc-7zip-winrar-python-filezilla-notepadplusplus-jdk8-winscp-putty-winmerge-eclipse-vscode" )
_IELoadWait ( $oIE )
Sleep ( 2000 )
Send ( "{TAB 2}" & "{ENTER}", 0 )
Sleep ( 5000 )
$find = FileFindFirstFile ( @UserProfileDir & "\Downloads\ninite*.exe" )
$filename = FileFindNextFile ( $find )
ShellExecute ( $filename, "", @UserProfileDir & "\Downloads" )
$win = WinWait ( "Ninite", "Close" )
WinActivate ( $win )
WinWaitActive ( $win )
ControlClick ( $win, "", 2 )
_IEQuit ( $oIE )
