      If (!(Test-Path "$env:ProgramData\boxlib"))
{
    New-item -Name "boxlib" -Path $env:PROGRAMDATA -ItemType Directory -Force
}
If (!(Test-Path "$env:ProgramData\boxlib\Box.Dependencies\1.0.0\lib\net45\Box.V2.dll"))
{
    Get-PackageProvider -Name NuGet -ForceBootstrap
Get-PackageProvider -Name Powershellget -ForceBootstrap
Register-PackageSource -Name "Box.Dependencies" -Location "https://www.myget.org/F/boxdependency/api/v2" -ProviderName "PowershellGet" -Trusted -Force
save-Package -Name Box.dependencies -path "$env:ProgramData\boxlib" -ProviderName "PowershellGet"
}
Get-childitem "$env:ProgramData\boxlib\*.dll" -Recurse | % {[reflection.assembly]::LoadFrom($_.FullName)}
#Authentication file not included for obvious reasons
$cli = Get-Content "$env:PROGRAMDATA\boxlib\box-cli.json" | ConvertFrom-Json
#retrieves date 6 months prior to todays date
$date = Get-Date -Format "yyyy-MM-dd"
$date = ([DateTime]$date).AddMonths(-6)

#use box windows sdk to generate admin token and admin client.
$boxconfig = New-Object -TypeName Box.V2.Config.Boxconfig($cli.boxAppSettings.clientID, $cli.boxAppSettings.clientSecret, $cli.enterpriseID, $cli.boxAppSettings.appAuth.privateKey, $cli.boxAppSettings.appAuth.passphrase, $cli.boxAppSettings.appAuth.publicKeyID)
$boxJWT = New-Object -TypeName Box.V2.JWTAuth.BoxJWTAuth($boxconfig)
$boxjwt
$tokenreal = $boxJWT.AdminToken()
$adminclient = $boxjwt.AdminClient($tokenreal)
$adminclient

#array to store event objects
$allevents = New-Object System.Collections.Generic.List[object]
#call box event api which will return all Login events starting from the date calculated above until current date
$enteventurl = "https://api.box.com/2.0/events?stream_type=admin_logs&event_type=Login&limit=500&created_after=$date"
$headers = @{}
$headers.Add("Authorization", "Bearer $tokenreal")
$headers.Add("Accept-Encoding", "gzip, deflate")
$resultevents = Invoke-RestMethod -Uri $enteventurl -Method Get -Headers $headers
#add all event entries to array
$allevents.Add($resultevents.entries)
#repeat process until the "chunk_size" value (which holds the number of returned entries) is less than 10
Do
{
	$streampos = $resultevents.next_stream_position
	$enteventurl = "https://api.box.com/2.0/events?stream_type=admin_logs&event_type=Login&limit=500&created_after=$date&stream_position=$streampos"
	$resultevents = Invoke-RestMethod -Uri $enteventurl -Method Get -Headers $headers
	$allevents.Add($resultevents.entries) > $null
}
Until ($resultevents.chunk_size -lt 10)
#create another array that will hold all of the users who are associated with the returned events
$userarray = New-Object System.Collections.ArrayList
#loop through all events and add users associated with the events in the event array
foreach ($item in $allevents)
{
	$tempvar = $item.source
	Foreach ($stick in $tempvar)
	{
		$userarray.Add($stick) > $null
	}
}

#obtain a list of all Acuity Brands box users.
$hold = 0
$limit = 1000
$userhold = New-Object System.Collections.ArrayList
[string[]]$fields = "role","login","id","name"
[System.Collections.Generic.IEnumerable[String]]$conv = $fields
$result = $adminclient.UsersManager.GetEnterpriseUsersAsync($null, $hold, $limit, $conv)
$result.Wait()
$totalcount = $result.result.totalcount
$result.result.entries | % {$userhold.Add($_)} > $null
Do
{
    $hold = $hold + $limit
    $result = $adminclient.UsersManager.GetEnterpriseUsersAsync($null, $hold, $limit, $conv)
    $result.Wait()
    $result.result.entries | % {$userhold.Add($_)} > $null
}
While ($hold + $limit -lt $totalcount)

#loops through all of the Acuity brands box users and will delete the user's box account if they are not in the array of users associated with any of the returned login events and are not admins.
Foreach ($user in $userhold)
{
	Write-Host "Analyzing user $($user.name)."
    #this portion of if statement executes when the user being analyzed is not found in the array of the users who generated a login event for the past 6 months and if the user is not an admin
	If (($userarray.id -notcontains $user.id) -and ($user.role -notlike "*admin*"))
	{
		Write-Host "$($user.name) has not been active in over 6 months.  Attempting to delete box account."
		$mail = $user.login
        #uses the box sdk to attempt to delete the user's box account.
        $deletetask = $adminclient.UsersManager.DeleteEnterpriseUserAsync($user.id, $false, $false)
        Try
        {
            $deletetask.Wait()
        }
        Catch
        {
        }
        If ($deletetask.IsFaulted -eq $false)
        {
            echo "$($user.name)'s account was deleted.  Now removing from Federate-Box AD group." | out-file "$env:DESKTOP\reclaim.txt" -Append
            Try
			{
                #if the account for the user being analyzed was deleted, the script will then attempt to modify the ad account of that user and remove it from the "Federated - Box" Group
				$aduser = Get-ADUser -Filter "Mail -like `"$mail`"" -ErrorAction Stop
				Remove-ADGroupMember -Identity "Federate - Box" -Members $aduser -Confirm:$false -ErrorAction Stop
                echo "$($user.name)'s ad account has been removed from the Federate - box group" | out-file "$env:DESKTOP\reclaim.txt" -Append
			}
			Catch
			{
				echo "$($user.name)'s ad account is not present in the Box - Federate Group.  Moving on...." | out-file "$env:DESKTOP\reclaim.txt" -Append
                Continue
			}
        }
        Else
        {
            #if the user account could not be deleted using the box sdk, this information is written to the console and the next user in the array will start processing.
            echo "Failed to delete $($user.name).  The user probably has managed content in their box account." | out-file "$env:DESKTOP\reclaim.txt" -Append
            Continue
        }
	}
    #this code runs if the user being analyzed is in the array of users with login events or if the user is an admin.
	Else
	{
		Write-Host "$($user.name)'s account has shown activity in the past 6 months.  Skipping...."
        Continue
	}
}

