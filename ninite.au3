      #Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Add_Constants=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.15.0 (Beta)
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

#include <IE.au3>
#include <Constants.au3>

Local $oIE = _IECreate("www.ninite.com")
If Not IsObj($oIE) Then Exit ConsoleWrite("Error in $oIE" & @CRLF)

Sleep(5000) ; just to make sure

Local $oTeamViewer11 = _IEGetObjById($oIE, "cb_bd6x4")
If Not IsObj($oTeamViewer11) Then Exit ConsoleWrite("Error in $oTeamViewer11" & @CRLF)

Local $o7Zip = _IEGetObjById($oIE, "cb_bcaaf")
If Not IsObj($o7Zip) Then Exit ConsoleWrite("Error in $o7Zip" & @CRLF)

Local $oChrome = _IEGetObjById($oIE, "cb_kvedr")
If Not IsObj($oChrome) Then Exit ConsoleWrite("Error in $oChrome" & @CRLF)

Local $oOpera = _IEGetObjById($oIE, "cb_dqbak")
If Not IsObj($oOpera) Then Exit ConsoleWrite("Error in $oOpera" & @CRLF)

Local $oFirefox = _IEGetObjById($oIE, "cb_22s4s")
If Not IsObj($oFirefox) Then Exit ConsoleWrite("Error in $oFirefox" & @CRLF)

Local $oVLC = _IEGetObjById($oIE, "cb_5hco5")
If Not IsObj($oVLC) Then Exit ConsoleWrite("Error in $oVLC" & @CRLF)

Local $oKlite = _IEGetObjById($oIE, "cb_w53be")
If Not IsObj($oKlite) Then Exit ConsoleWrite("Error in $oKlite" & @CRLF)

Local $oSpotify = _IEGetObjById($oIE, "cb_tlvlq")
If Not IsObj($oSpotify) Then Exit ConsoleWrite("Error in $oSpotify" & @CRLF)

Local $oCCCP = _IEGetObjById($oIE, "cb_xsupa")
If Not IsObj($oCCCP) Then Exit ConsoleWrite("Error in $oCCCP" & @CRLF)

Local $ojava = _IEGetObjById($oIE, "cb_za42k")
If Not IsObj($ojava) Then Exit ConsoleWrite("Error in $ojava" & @CRLF)

Local $oNet = _IEGetObjById($oIE, "cb_4e5lw")
If Not IsObj($oNet) Then Exit ConsoleWrite("Error in $oNet" & @CRLF)

Local $oSilver = _IEGetObjById($oIE, "cb_sutoa")
If Not IsObj($oSilver) Then Exit ConsoleWrite("Error in $oSilver" & @CRLF)

Local $oAir = _IEGetObjById($oIE, "cb_hrcyq")
If Not IsObj($oAir) Then Exit ConsoleWrite("Error in $oAir" & @CRLF)

Local $oShock = _IEGetObjById($oIE, "cb_rci2j")
If Not IsObj($oShock) Then Exit ConsoleWrite("Error in $oShock" & @CRLF)

Local $oIrfan = _IEGetObjById($oIE, "cb_2n6do")
If Not IsObj($oIrfan) Then Exit ConsoleWrite("Error in $oIrfan" & @CRLF)

Local $oXn = _IEGetObjById($oIE, "cb_hzubg")
If Not IsObj($oXn) Then Exit ConsoleWrite("Error in $oXn" & @CRLF)

Local $oGreen = _IEGetObjById($oIE, "cb_wq43l")
If Not IsObj($oGreen) Then Exit ConsoleWrite("Error in $oGreen" & @CRLF)

Local $oShare = _IEGetObjById($oIE, "cb_nfdvi")
If Not IsObj($oShare) Then Exit ConsoleWrite("Error in $oShare" & @CRLF)

Local $oFoxit = _IEGetObjById($oIE, "cb_n3l7s")


Local $oSuma = _IEGetObjById($oIE, "cb_m4qxn")


Local $oCute = _IEGetObjById($oIE, "cb_g2ba4")


Local $oMal = _IEGetObjById($oIE, "cb_mgdaa")


Local $oSpy = _IEGetObjById($oIE, "cb_fflxm")



Local $oDrop = _IEGetObjById($oIE, "cb_f4z4i")


Local $oDrive = _IEGetObjById($oIE, "cb_v4sqq")



Local $oBit = _IEGetObjById($oIE, "cb_mwfrt")



Local $oEver = _IEGetObjById($oIE, "cb_i2ycj")



Local $oKey = _IEGetObjById($oIE, "cb_jrd6u")



Local $oEvery = _IEGetObjById($oIE, "cb_gew3n")


Local $oRevo = _IEGetObjById($oIE, "cb_lpcec")


Local $oGlary = _IEGetObjById($oIE, "cb_h4enj")


Local $oRar = _IEGetObjById($oIE, "cb_nxciq")

Local $oPython = _IEGetObjById($oIE, "cb_epxow")


Local $oFile = _IEGetObjById($oIE, "cb_xqkaw")

Local $oNote = _IEGetObjById($oIE, "cb_j2gws")

Local $oJdk = _IEGetObjById($oIE, "cb_azvh4")

Local $oWinScp = _IEGetObjById($oIE, "cb_3nmgs")

Local $oPutty = _IEGetObjById($oIE, "cb_uriix")

Local $oWinMerge = _IEGetObjById($oIE, "cb_a7h2w")

Local $oEclipse = _IEGetObjById($oIE, "cb_nn5wk")

$oForm = _IEFormGetCollection ( $oIE, 0 )
#cs
; you can click

$oTeamViewer11.click()
$o7Zip.click()
#ce
; or you can use checked

$oTeamViewer11.checked = True
$o7Zip.checked = True
$oChrome.checked = True
$oOpera.checked = True
$oFirefox.checked = True
$oVLC.checked = True
$oKlite.checked = True
$oSpotify.checked = True
$oCCCP.checked = True
$ojava.checked = True
$oNet.checked = True
$oSilver.checked = True
$oAir.checked = True
$oShock.checked = True
$oIrfan.checked = True
$oXn.checked = True
$oGreen.checked = True
$oShare.checked = True
$oFoxit.checked = True
$oSuma.checked = True
$oCute.checked = True
$oMal.checked = True
$oSpy.checked = True
$oDrop.checked = True
$oDrive.checked = True
$oBit.checked = True
$oEver.checked = True
$oKey.checked = True
$oEvery.checked = True
$oRevo.checked = True
$oGlary.checked = True
$oRar.checked = True
$oPython.checked = True
$oFile.checked = True
$oNote.checked = True
$oJdk.checked = True
$oWinScp.checked = True
$oPutty.checked = True
$oWinMerge.checked = True
$oEclipse.checked = True

Sleep ( 3000 )
BlockInput ( 1 )


_IEFormSubmit ( $oForm )

Sleep ( 5000 )
Send ( "{TAB 2}" & "{ENTER}", 0 )
BlockInput ( 0 )
Sleep ( 5000 )
RunWait ( @ComSpec & ' /c powershell -command "Get-ChildItem -Path $env:USERPROFILE\Downloads\* -Recurse | Unblock-file"' )
$down = @UserProfileDir & "\downloads"
FileChangeDir ( $down )
$search = FileFindFirstFile ( "Ninite*.exe" )
$files = FileFindNextFile($search)
ShellExecute ( $files, "", $down )
$nin = WinWait ( "Ninite", "Close" )
WinActivate ( $nin )
WinWaitActive ( $nin )
ControlClick ( $nin, "", 2 )
