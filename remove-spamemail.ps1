      

<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.145
	 Created on:   	1/22/2018 3:38 PM
	 Created by:   	WMH02
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		Will import all proofpoint generated csv files containing spam messages flagged by the proofpoint spam filter but were 
    still sent to the intended recipients and will remove the spam from the users' office 365 mailboxes to a "spam" mailbox.
#>

$pass = Get-Content "$env:USERPROFILE\Documents\o365adm.txt"
$key = (3, 4, 2, 3, 56, 34, 254, 222, 1, 1, 2, 23, 42, 54, 33, 233, 1, 34, 2, 7, 6, 5, 35, 43)
$pass = ConvertTo-SecureString $pass -Key $key
$cred2 = New-Object System.Management.Automation.PSCredential -ArgumentList 'null@null.com', $pass
# connect to office 365 exchange
$connect = Connect-MsolService -Credential $cred2
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $cred2 -Authentication Basic -AllowRedirection
Import-PSSession $Session -Prefix oo365
#imports csv files.
$files = Get-ChildItem "\\cdcsrvr1\Depts\Engineering Solutions\Spam_script\*.csv"
foreach ($file in $files)
{
	$csv = Import-Csv $file.FullName -Delimiter ','
	#removes spam using the subject and sender address specified in csv object to identify the message from the mailbox which is also identified in the csv object.
	foreach ($cs in $csv)
	{
		$plus = $cs.Sender.Indexof('+')
		$equ = $cs.sender.Indexof('=')
		$bogusemail = $false
		If (($plus -eq -1) -and ($equ -eq -1))
		{
			$thesender = $cs.Sender
		}
		Else
		{
			$trace = Get-oo365MessageTrace -MessageId $cs.Message_ID
			$thesender = $trace.SenderAddress
		}
		$date = [datetime]$($cs.Date).Substring(0,10)
		$multrec = $cs.Recipients.Indexof(',')
		If ($multrec -eq -1)
		{
			<#
			Try
			{
				$users = Get-oo365User $cs.Recipients -ErrorAction Stop
			}
			Catch
			{
				$users = Get-oo365DistributionGroupmember $cs.Recipients
			}
			#>
			$iii = $null
			$iii = Get-oo365DistributionGroup $cs.Recipients -erroraction SilentlyContinue
            If ($iii -eq $null)
            {
                $users = get-oo365mailbox $cs.Recipients
                Write-Host "Is not distribution group.  Below are mailboxes"
                $users
            }
            Else
            {
                $users = Get-oo365DistributionGroupmember $cs.Recipients
                 Write-Host "Is not distribution group.  Below are mailboxes"
                 $users
            }
            If ($users -is [PSobject])
            {
                If ($cs.Subject -eq '')
			    {
					$users.Primarysmtpaddress
				    $searchres = Search-oo365Mailbox -Identity $users.Primarysmtpaddress -SearchQuery "From:$thesender Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                    $searchres
			    }
			    Else
			    {
                    $users
				    $searchres = Search-oo365Mailbox -Identity $users.Primarysmtpaddress -SearchQuery "From:$thesender Subject:`"$($cs.Subject)`" Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                    If ($searchres.ResultItemsCount -eq 0)
                    {
                        $searchres = Search-oo365Mailbox -Identity $users.PrimarySmtpAddress -SearchQuery "From:$thesender Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                        $searchres
                    }
                    Else
                    {
                        $searchres
                    }
			    }
            }
            Else
            {
                Foreach ($user in $users)
                {
                    If ($cs.Subject -eq '')
			        {
				        $searchres = Search-oo365Mailbox -Identity $user.PrimarySmtpAddress -SearchQuery "From:$thesender Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                        $searchres
			        }
			        Else
			        {
				        $searchres = Search-oo365Mailbox -Identity $user.PrimarySmtpAddress -SearchQuery "From:$thesender Subject:`"$($cs.Subject)`" Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                        If ($searchres.ResultItemsCount -eq 0)
                        {
                            $searchres = Search-oo365Mailbox -Identity $user.PrimarySmtpAddress -SearchQuery "From:$thesender Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                            $searchres
                        }
                        Else
                        {
                            $searchres
                        }
			        }
                }
            }
		}
		Else
		{
			$allrecepts = $cs.Recipients.Split(',')
			foreach ($rec in $allrecepts)
			{
                $iii = $null
                $iii = Get-oo365DistributionGroup $rec -erroraction SilentlyContinue
                If ($iii -eq $null)
                {
                    $users = get-oo365mailbox $rec
                }
                Else
                {
                    $users = Get-oo365DistributionGroupmember $rec
                }
                If ($users -is [PSObject])
                {
                    $users
				    If ($cs.Subject -eq '')
				    {
					    $searchres = Search-oo365Mailbox -Identity $users.Primarysmtpaddress -SearchQuery "From:$thesender Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
				    }
				    Else
				    {
					    $searchres = Search-oo365Mailbox -Identity $users.Primarysmtpaddress -SearchQuery "From:$thesender Subject:`"$($cs.Subject)`" Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                        If ($searchres.ResultItemsCount -eq 0)
                        {
                            $searchres = Search-oo365Mailbox -Identity $users.PrimarySmtpAddress -SearchQuery "From:$thesender Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                            $searchres
                        }
                        Else
                        {
                            $searchres
                        }
				    }
                }
                Else
                {
                    Foreach ($user in $users)
                    {
                        $user
                        If ($cs.Subject -eq '')
			            {
				            $searchres = Search-oo365Mailbox -Identity $user.PrimarySmtpAddress -SearchQuery "From:$thesender Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                            $searchres
			            }
			            Else
			            {
				            $searchres = Search-oo365Mailbox -Identity $user.PrimarySmtpAddress -SearchQuery "From:$thesender Subject:`"$($cs.Subject)`" Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                            If ($searchres.ResultItemsCount -eq 0)
                            {
                                $searchres = Search-oo365Mailbox -Identity $user.PrimarySmtpAddress -SearchQuery "From:$thesender Received:$($date.Month)/$($date.Day)/$($date.Year)" -TargetMailbox "ISTEmailReview@AcuityBrands.com" -TargetFolder "Inbox" -SearchDumpster -DeleteContent -Force -Confirm:$false
                                $searchres
                            }
                            Else
                            {
                                $searchres
                            }
			            }
                    }
                }
			}
		}
	}
}

Get-PSSession | Remove-PSSession
