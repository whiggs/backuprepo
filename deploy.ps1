       <#
    .DESCRIPTION
	Automates the configuration of my operating system after the custom Windows 10 image I created (along with unattend xml file 
	for complete automation) is deployed.  Batch script which downloads and runs script put in “start” folder of offline image.
	This allows me to edit and update the script as I see fit without needing to continuously replace the script in the offline 
	image.  Configuration of autologon is performed locally for security purposes.
#>
Function Show-DesktopIcons{
$ErrorActionPreference = "SilentlyContinue"
If ($Error) {$Error.Clear()}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
If (Test-Path $RegistryPath) {
	$Res = Get-ItemProperty -Path $RegistryPath -Name "HideIcons"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "HideIcons" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "HideIcons").HideIcons
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "HideIcons" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons"
If (-Not(Test-Path $RegistryPath)) {
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer" -Name "HideDesktopIcons" -Force | Out-Null
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons" -Name "NewStartPanel" -Force | Out-Null
}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel"
If (-Not(Test-Path $RegistryPath)) {
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons" -Name "NewStartPanel" -Force | Out-Null
}
If (Test-Path $RegistryPath) {
	## -- My Computer
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}")."{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Control Panel
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}")."{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- User's Files
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}")."{59031a47-3f72-44a7-89c5-5595fe6b30ee}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Recycle Bin
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}")."{645FF040-5081-101B-9F08-00AA002F954E}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Network
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}")."{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
}
If ($Error) {$Error.Clear()}
}
Function Set-AutoLogon{

    [CmdletBinding()]
    Param(
        
        [Parameter(Mandatory=$True,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$DefaultUsername,

        [Parameter(Mandatory=$True,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$DefaultPassword,

        [Parameter(Mandatory=$False,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [String[]]$AutoLogonCount,

        [Parameter(Mandatory=$False,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [String[]]$Script
                
    )

    Begin
    {
        #Registry path declaration
        $RegPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
        $RegROPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce"
    
    }
    
    Process
    {

        try
        {
            #setting registry values
            Set-ItemProperty $RegPath "AutoAdminLogon" -Value "1" -type String  
            Set-ItemProperty $RegPath "DefaultUsername" -Value "$DefaultUsername" -type String  
            Set-ItemProperty $RegPath "DefaultPassword" -Value "$DefaultPassword" -type String
            if($AutoLogonCount)
            {
                
                Set-ItemProperty $RegPath "AutoLogonCount" -Value "$AutoLogonCount" -type DWord
            
            }
            else
            {

                Set-ItemProperty $RegPath "AutoLogonCount" -Value "1" -type DWord

            }
            if($Script)
            {
                
                Set-ItemProperty $RegROPath "(Default)" -Value "$Script" -type String
            
            }
            else
            {
            
                Set-ItemProperty $RegROPath "(Default)" -Value "" -type String
            
            }        
        }

        catch
        {

            Write-Output "An error had occured $Error"
            
        }
    }
    
    End
    {
        
        #End

    }

}
Function Install-RSATTools
{
    $VerbosePreference = 'Continue'

    $x86 = 'https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_WS2016-x86.msu'
    $x64 = 'https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_WS2016-x64.msu'

    switch ($env:PROCESSOR_ARCHITECTURE)
    {
        'x86' {$version = $x86}
        'AMD64' {$version = $x64}
    }

    Write-Verbose -Message "OS Version is $env:PROCESSOR_ARCHITECTURE"
    Write-Verbose -Message "Now Downloading RSAT Tools installer"

    $Filename = $version.Split('/')[-1]
    Invoke-WebRequest -Uri $version -UseBasicParsing -OutFile "$env:TEMP\$Filename" 
    
    Write-Verbose -Message "Starting the Windows Update Service to install the RSAT Tools "
    
    Start-Process -FilePath wusa.exe -ArgumentList "$env:TEMP\$Filename /quiet" -Wait -Verbose
    
    Write-Verbose -Message "RSAT Tools are now be installed"
    
    Remove-Item "$env:TEMP\$Filename" -Verbose
    
    Write-Verbose -Message "Script Cleanup complete"
    
    Write-Verbose -Message "Remote Administration"
}
workflow Download-AllGalleryModules
{
mkdir C:\modules
mkdir C:\scripts

 
$modules = Find-Module * -IncludeDependencies | Sort-Object Name

foreach -parallel -throttlelimit 25 ($module in $modules) 
    { Save-Module $module.Name -Path C:\modules -Force }

$scripts = Find-Script * -IncludeDependencies | Sort-Object Name

foreach -parallel -throttlelimit 25 ($script in $scripts) 
    { Save-Script $script.Name -Path C:\scripts -Force }

}
function Use-RunAs 
{    
    # Check if script is running as Adminstrator and if not use RunAs 
    # Use Check Switch to check if admin 
     
    param([Switch]$Check) 
     
    $IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()` 
        ).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") 
         
    if ($Check) { return $IsAdmin }     
 
    if ($MyInvocation.ScriptName -ne "") 
    {  
        if (-not $IsAdmin)  
        {  
            try 
            {  
                $arg = "-file `"$($MyInvocation.ScriptName)`"" 
                Start-Process "$psHome\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop'  
            } 
            catch 
            { 
                Write-Warning "Error - Failed to restart script with runas"  
                break               
            } 
            exit # Quit this session of powershell 
        }  
    }  
    else  
    {  
        Write-Warning "Error - Script must be saved as a .ps1 file first"  
        break  
    }  
} 
 function Test-LocalCredential {
    [CmdletBinding()]
    Param
    (
        [string]$UserName,
        [string]$ComputerName,
        [string]$Password
    )
    if (!($UserName) -or !($Password)) {
        Write-Warning 'Test-LocalCredential: Please specify both user name and password'
    } else {
        Add-Type -AssemblyName System.DirectoryServices.AccountManagement
        $DS = New-Object System.DirectoryServices.AccountManagement.PrincipalContext('machine',$ComputerName)
        $DS.ValidateCredentials($UserName, $Password)
    }
}
 
 
 
 
# Code goes at very bottom
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
    Use-RunAs
    If (!(Test-Path "$env:APPDATA\hold.txt"))
    {
    $hol = 0
   DISM /Online /Enable-Feature /FeatureName:NetFx3 /All
      Get-PackageProvider -Name NuGet -ForceBootstrap
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
    Try
    {
        Install-module Carbon -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Carbon -Force -AllowClobber
    }
Set-RegistryKeyValue -Path 'hklm:\Software\Microsoft\Windows\CurrentVersion\Run' -Name "deploy" -String '%APPDATA%\go.bat' -Expand
#Download-AllGalleryModules
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
& "C:\ProgramData\chocolatey\redirects\RefreshEnv.cmd"
echo '@powershell -Executionpolicy bypass -Command "Start-Process powershell -ArgumentList $env:USERPROFILE\Desktop\beg.ps1"' | Out-File -filepath "$env:APPDATA\go.bat" -Encoding ascii
 # . "$env:APPDATA\Set-autologon.ps1"
  #Set-Autologon -DefaultUsername "$env:USERDOMAIN\$env:USERNAME" -DefaultPassword $password -AutoLogonCount "6" -Script "$env:APPDATA\go.bat"
# choco install boxstarter -y
& "C:\ProgramData\chocolatey\redirects\RefreshEnv.cmd"

$obj = Get-WmiObject -Class "Win32_TerminalServiceSetting" -Namespace root\cimv2\terminalservices
$obj.SetAllowTsConnections(1,1) | out-null
$obj2 = Get-WmiObject -class Win32_TSGeneralSetting -Namespace root\cimv2\terminalservices -ComputerName . -Filter "TerminalName='RDP-tcp'"
$obj2.SetUserAuthenticationRequired(0) | out-null
Invoke-Webrequest https://gist.githubusercontent.com/whiggs/306d5b36349b463b720cb78796907777/raw/076e7e7956561cbbeb69c04552d4904ccb0d8a08/winup.ps1 -outfile $env:APPDATA\boxstarter.ps1
Invoke-Webrequest "https://ninite.com/.net4.6.2-7zip-air-cccp-chrome-eclipse-evernote-everything-filezilla-firefox-foxit-glary-greenshot-handbrake-irfanview-java8-jdk8-jdkx8-keepass2-klitecodecs-notepadplusplus-operaChromium-putty-python-revo-sharex-shockwave-silverlight-sumatrapdf-teamviewer12-vscode-winmerge-winrar-winscp/ninite.exe" -Outfile "$env:USERPROFILE\Desktop\ninite.exe"
Start-Process "$env:USERPROFILE\Desktop\ninite-silent.exe" -Wait
$hol = "1" | Out-File "$env:APPDATA\hold.txt"
Restart-Computer -Force
}
Else
{
	If ($(Get-Content "$env:APPDATA\hold.txt") -eq 1)
	{
		Remove-Item "$env:APPDATA\hold.txt" -Force
Try
    {
        Install-module PSWindowsUpdate -Force -ErrorAction Stop
    }
Catch
    {
        Install-module PSWindowsUpdate -Force -AllowClobber
}
Try
    {
        Install-module CustomizeWindows10 -Force -ErrorAction Stop
    }
Catch
    {
        Install-module CustomizeWindows10 -Force -AllowClobber
}
Try
{
	Install-Module Autobrowse -Force -ErrorAction Stop
}
Catch
{
	Install-Module Autobrowse -Force -AllowClobber
}
Try
{
	Install-Module Patchy -Force -ErrorAction Stop
}
Catch
{
	Install-Module Patchy -Force -AllowClobber
}
Try
{
	Install-Module SecureSettings -Force -ErrorAction Stop
}
Catch
{
	Install-Module SecureSettings -Force -AllowClobber
}
Try
{
	Install-Module Winformal -Force -ErrorAction Stop
}
Catch
{
	Install-Module Winformal -Force -AllowClobber
}
Try
{
	Install-Module WinformPS -Force -ErrorAction Stop
}
Catch
{
	Install-Module WinformPS -Force -AllowClobber
}
Try
{
	Install-Module ShowUI -Force -ErrorAction Stop
}
Catch
{
	Install-Module ShowUI -Force -AllowClobber
}
Try
{
	Install-Module IsePackV2 -Force -ErrorAction Stop
}
Catch
{
	Install-Module IsePackV2 -Force -AllowClobber
}
Try
{
	Install-Module TaskScheduler -Force -ErrorAction Stop
}
Catch
{
	Install-Module TaskScheduler -Force -AllowClobber
}
Try
{
	Install-Module MenuShell -Force -ErrorAction Stop
}
Catch
{
	Install-Module MenuShell -Force -AllowClobber
}
Try
{
	Install-Module CodeCraft -Force -ErrorAction Stop
}
Catch
{
	Install-Module CodeCraft -Force -AllowClobber
}
Try
{
	Install-Module PinnedItem -Force -ErrorAction Stop
}
Catch
{
	Install-Module PinnedItem -Force -AllowClobber
}
Install-module Azurerm
Install-module AzureAD
Install-module MSonline
Install-module powershellise-preview
Install-module ISEsteroids
Install-script connect-o365 -force -confirm:$false
Install-module OutlookConnector
Show-DesktopIcons
#Enable-ExplorerThisPC
#Enable-Windows7VolumeMixer
#Enable-ShowFileExtension
#Enable-ShowHiddenFiles
choco upgrade sysinternals -y
choco upgrade lockhunter -y
choco upgrade sudo -y
# choco upgrade capture2text -y
#choco install sqlite -y
choco upgrade wget -y
choco upgrade youtube-dl -y
choco upgrade conemu -y
#choco install slickrun -y
Install-RSATTools
$hol = "2" | Out-File "$env:APPDATA\hold.txt"
Restart-Computer -Force
}
Else
{
Remove-Item "$env:APPDATA\hold.txt" -Force
<#
cd $env:USERPROFILE\Documents
mkdir git
cd git
git clone https://github.com/p-e-w/maybe
Start-Process "C:\Python27\python.exe" -ArgumentList '-m pip install --upgrade pip' -Wait
Start-Process "C:\Python27\Scripts\pip.exe" -ArgumentList 'install maybe' -Wait
Start-Process "C:\Python27\Scripts\pip.exe" -ArgumentList 'install howdoi' -Wait
npm install -g how2
npm install -g mklicense
npm install --global pageres-cli
npm install -g wat
npm install -g bitly-client
npm install --global imgur-uploader-cli
npm install -g localtunnel
npm i wappalyzer -g
npm install --global wallpaper-cli
npm install -g share-cli
npm install -g remote-share-cli
npm install --global trash-cli
npm install --global empty-trash-cli
npm install -g trashss
npm install --global del-cli
npm install --global cpy-cli
npm install --global file-type-cli
npm install -g doctoc
npm i -g cli-github
npm install -g gistup
npm install --global find-versions-cli
npm install --global kill-tabs
npm install hget -g
#>
Remove-Item "$env:APPDATA\go.bat" -Force
echo "cd /d %~dp0" 'PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command ".\boxstarter.ps1"' | Out-file -filepath "$env:APPDATA\go.bat" -Encoding ascii
Write-Host 'Restarting Computer soon.  Save Work.'
Start-Sleep 10
Restart-Computer -force
}
} 
