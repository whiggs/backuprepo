      Function Set-AutoLogon{

    [CmdletBinding()]
    Param(
        
        [Parameter(Mandatory=$True,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$DefaultUsername,

        [Parameter(Mandatory=$True,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$DefaultPassword,

        [Parameter(Mandatory=$False,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [String[]]$AutoLogonCount,

        [Parameter(Mandatory=$False,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [String[]]$Script
                
    )

    Begin
    {
        #Registry path declaration
        $RegPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
        $RegROPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce"
    
    }
    
    Process
    {

        try
        {
            #setting registry values
            Set-ItemProperty $RegPath "AutoAdminLogon" -Value "1" -type String  
            Set-ItemProperty $RegPath "DefaultUsername" -Value "$DefaultUsername" -type String  
            Set-ItemProperty $RegPath "DefaultPassword" -Value "$DefaultPassword" -type String
            if($AutoLogonCount)
            {
                
                Set-ItemProperty $RegPath "AutoLogonCount" -Value "$AutoLogonCount" -type DWord
            
            }
            else
            {

                Set-ItemProperty $RegPath "AutoLogonCount" -Value "1" -type DWord

            }
            if($Script)
            {
                
                Set-ItemProperty $RegROPath "(Default)" -Value "$Script" -type String
            
            }
            else
            {
            
                Set-ItemProperty $RegROPath "(Default)" -Value "" -type String
            
            }        
        }

        catch
        {

            Write-Output "An error had occured $Error"
            
        }
    }
    
    End
    {
        
        #End

    }

}
function Use-RunAs 
{    
    # Check if script is running as Adminstrator and if not use RunAs 
    # Use Check Switch to check if admin 
     
    param([Switch]$Check) 
     
    $IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()` 
        ).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") 
         
    if ($Check) { return $IsAdmin }     
 
    if ($MyInvocation.ScriptName -ne "") 
    {  
        if (-not $IsAdmin)  
        {  
            try 
            {  
                $arg = "-file `"$($MyInvocation.ScriptName)`"" 
                Start-Process "$psHome\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop'  
            } 
            catch 
            { 
                Write-Warning "Error - Failed to restart script with runas"  
                break               
            } 
            exit # Quit this session of powershell 
        }  
    }  
    else  
    {  
        Write-Warning "Error - Script must be saved as a .ps1 file first"  
        break  
    }  
} 
 
 function Test-LocalCredential {
    [CmdletBinding()]
    Param
    (
        [string]$UserName,
        [string]$ComputerName = $env:COMPUTERNAME,
        [string]$Password
    )
    if (!($UserName) -or !($Password)) {
        Write-Warning 'Test-LocalCredential: Please specify both user name and password'
    } else {
        Add-Type -AssemblyName System.DirectoryServices.AccountManagement
        $DS = New-Object System.DirectoryServices.AccountManagement.PrincipalContext('machine',$ComputerName)
        $DS.ValidateCredentials($UserName, $Password)
    }
}
 
 
 
 
# Code goes at very bottom
Use-RunAs
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
$log = $env:APPDATA + '\hold.txt'
If (!(Test-path -LiteralPath $log)) {
  $text = "1"
  $text | out-file $log 
    DISM /Online /Enable-Feature /FeatureName:NetFx3 /All
    iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
C:\ProgramData\chocolatey\helpers\functions\Update-SessionEnvironment.ps1
echo "cd /d %~dp0" 'PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command ".\boxstarter.ps1"' | Out-file -filepath "$env:APPDATA\go.bat" -Encoding ascii
     $OUTPU= [System.Windows.Forms.MessageBox]::Show("Configure autologin?" , "choose" , 4)
     Install-module Carbon
     Set-RegistryKeyValue -Path 'hklm:\Software\Microsoft\Windows\CurrentVersion\Run' -Name "deploy" -String '%APPDATA%\go.bat' -Expand
     If ($OUTPU -eq "YES") {
         $cred = Get-Credential -Credential "$env:USERNAME"
         $this = $cred.GetNetworkCredential().password
         ConvertFrom-SecureString -SecureString $cred.Password | Out-File -FilePath "$env:APPDATA\secpasure.txt"
         Set-AutoLogon -DefaultUsername "$env:USERNAME" -DefaultPassword "$this"
     }

$OUTPUT= [System.Windows.Forms.MessageBox]::Show("Disable UAC?" , "choose" , 4)
If ($OUTPUT -eq "YES") {
    Set-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name EnableLUA  -Value 0
    "IT happened" | Out-file "$env:APPDATA\select.txt"
}
Invoke-Webrequest "https://download.microsoft.com/download/D/5/C/D5C98AB0-35CC-45D9-9BA5-B18256BA2AE6/NDP462-KB3151802-Web.exe" -outfile "$env:USERPROFILE\Downloads\net462.exe"
   Start-Process "$env:USERPROFILE\Downloads\net462.exe" -Argumentlist "/q" -wait
}
$ret = Get-Content $log
If ($ret -eq "1") {
    del $log
$text = "2"
$text | Out-file $log
    choco install boxstarter -y
C:\ProgramData\chocolatey\helpers\functions\Update-SessionEnvironment.ps1
Restart-Computer -Force}
ElseIf ($ret -eq "2") {
    Get-PackageProvider -Name NuGet -ForceBootstrap
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
  Invoke-Webrequest "https://gist.githubusercontent.com/DemonDante/59d851c627fe16ac109f6d00922bb1ab/raw/6d362269a746edd3f527e0dcd96f463e27118e03/set-autologon.ps1" -outfile "$env:APPDATA\set-autologon.ps1"
  #echo 'Invoke-Expression $(Invoke-Webrequest "https://gist.githubusercontent.com/DemonDante/fb39757fdaf02071b47b24f234714318/raw/7e3fec0f2b6ca906415fbd476332352ec81d62dc/boxstarter.ps1")' | Out-File -filepath "$env:APPDATA\boxstarter.ps1" -Encoding ascii
 # . "$env:APPDATA\Set-autologon.ps1"
If (Test-path "$env:APPDATA\Autologon.exe") {
    $securstr = Get-Content "$env:APPDATA\secpasure.txt"
    $gathpass = ConvertTo-SecureString $securstr
    $cred = New-Object System.Management.Automation.PSCredential ("$env:USERNAME", $gathpass)
    Start-Process "$env:APPDATA\autologon.exe" -Argumentlist "$env:USERNAME $env:USERDOMAIN $cred.GetNetworkCredential().password" -Wait
}

$obj = Get-WmiObject -Class "Win32_TerminalServiceSetting" -Namespace root\cimv2\terminalservices
    $obj.SetAllowTsConnections(1,1) | out-null
    $obj2 = Get-WmiObject -class Win32_TSGeneralSetting -Namespace root\cimv2\terminalservices -ComputerName . -Filter "TerminalName='RDP-tcp'"
    $obj2.SetUserAuthenticationRequired(0) | out-null
del $log
$text = "3"
$text | Out-file $log
Restart-computer -force
}
Elseif ($ret -eq "3") {
Install-module Autoload
Install-module Beaver
Install-module BurntToast
Install-module CertificatePS
Install-module cMDT
Install-module CustomizeWindows10
Install-module DeployImage
Install-module Dimmo
Install-module IseHg
Install-module ISEScriptingGeek
Install-module Kickstart
Install-module MarkdownPS
Install-module OneDrive
Install-module Pester -Force
Install-module platyPS
Install-module PowerShellCookbook
Install-module PowerShellGet
Install-module PowerShellGetGUI
Install-module PowerShellISE-preview
Install-module Proxx.Toast
Install-module PSBookmark
Install-module PSConfig
Install-module Pscx
Install-module PSDeploy
Install-module PSGist
Install-module PsGoogle
Install-module PsISEProjectExplorer
Install-module PSReadline -Force
Install-module PSScriptAnalyzer
Install-module PSSudo
Install-module PSWindowsUpdate
Install-module PSWord
Install-module ISESteroids
Install-module SendEmail
Install-module ShowUI
Install-module WakeOnLan
Install-module WindowsImageTools
Install-module Winformsps
Install-Script -Name MessageCenter
Install-Script -Name ADLockoutViewer
Install-Script -Name Test-Credential
Install-Script -Name Update-Windows
Install-Script -Name Install-WinrmHttps
Install-Script -Name POSH-DiskHealth 
Install-Script -Name Get-ModuleConflict
Install-Script -Name Connect-Mstsc
Install-Script -Name Get-Parameter
Install-Script -Name New-PSSchtask
del $log
$text = "4"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "4") {
  Get-WUInstall -autoreboot -acceptall
  Start-Sleep 5
  del $log
$text = "5"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "5") {
    $another = "http://www.carifred.com/tech_tool_store/TechToolStore.exe"
$outward = $env:USERPROFILE + '\Downloads\toolstore.exe'
Invoke-WebRequest $another -OutFile $outward
Start-Process -FilePath $outward -Wait
 del $log
$text = "6"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "6") {
    choco install flashplayerplugin -y
choco install javaruntime -y
choco install sysinternals -y
choco install adobereader -y
choco install lockhunter -y
choco install sudo -y
choco install paket.powershell -y
choco install conemu -y

 del $log
$text = "7"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "7") {
         del $log
$text = "8"
$text | Out-file $log
    Invoke-Webrequest "https://gist.githubusercontent.com/DemonDante/6fdd9962040ddfea6c095d5feb2450e8/raw/54b33bb9a6436c4ddc456d8699271d5a5c64a7cc/uvk.au3" -outfile "$env:APPDATA\uvk.au3"
    Start-Process -Filepath "${env:ProgramFiles(x86)}\AutoIt3\Aut2Exe\Aut2exe.exe" -ArgumentList "/in $env:APPDATA\uvk.au3" -Wait
    Start-Process -FilePath "$env:APPDATA\uvk.exe" -Wait
   Restart-Computer -force
}

Else {
    Add-Type -AssemblyName System.Windows.Forms

$mess = [System.Windows.Forms.MessageBox]::Show('Installation complete')
If (Test-Path -Path "$env:APPDATA\select.txt") {Set-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name EnableLUA  -Value 1}
Remove-RegistryKeyValue -Path 'hkcu:\Software\Microsoft\Windows\CurrentVersion\Run' -Name "deploy"
}
