
#include <Array.au3>
#include <IE.au3>
$holpage = 1


$oie = _IECreate("https://github.com/login")
_IELoadWait($oie)
$form = _IEFormGetCollection($oie, 1)
$user = _IEGetObjById($oie, "login_field")
_IEFormElementSetValue($user, "whiggs")
$pass = _IEGetObjById($oie, "password")
_IEFormElementSetValue($pass, "Rayman!234567")
_IEFormSubmit($form)
_IELoadWait($oie)
$nav = "https://gist.github.com/whiggs"
While 1

	_IENavigate($oie, $nav)
	_IELoadWait($oie)
	Sleep(1000)
	$links = _IELinkGetCollection($oie)
	Local $array[@extended]
	$i = 0
	For $link In $links
		$array[$i] = $link.href
		$i = $i + 1
	Next
	$array = _ArrayUnique($array)
	;_ArrayDisplay($array)
	$nextpage = ""
	For $i = 0 To UBound($array) - 1 Step 1
		If StringRegExp($array[$i], "^https://gist\.github\.com/whiggs/[A-Za-z0-9]{18,}$") Then
			_IENavigate($oie, $array[$i])
			_IELoadWait($oie)
			$html = _IEDocReadHTML($oie)
			#cs
					$win = WinWait ( "[CLASS:Notepad]" )
					WinActivate ( $win )
					WinWaitActive ( $win )
					ControlSetText ( $win, "", "[CLASS:Edit; INSTANCE:1]", $html )
					MsgBox ( 1, "", "Whenever you are ready" )
			#ce


			$split = StringSplit($html, @CRLF)
			$titleind = _ArraySearch($split, "<title>", Default, Default, Default, 1)
			$filename = StringRegExpReplace($split[$titleind], '<.?title>', "")
			$file = FileOpen(@MyDocumentsDir & "\" & StringStripWS($filename, 3), 130)
			;MsgBox ( 1, "", $filename )
			$contind = _ArraySearch($split, '<textarea name="gist[content]"', Default, Default, Default, 1)
			$stopind = _ArraySearch($split, "</textarea>", $contind, Default, Default, 1)
			$split[$contind] = StringRegExpReplace($split[$contind], '<textarea name="gist\[content\]".+aria-hidden="true">', "")
			$split[$stopind] = StringRegExpReplace($split[$stopind], "</textarea>", "")
			For $o = $contind To $stopind Step 1
				FileWriteLine($file, StringReplace(StringReplace($split[$o], "&lt;", "<"), "&gt;", ">"))
			Next
			FileClose($file)
		EndIf
	Next
	$pages = _ArrayFindAll($array, "?page=", Default, Default, Default, 1)
	;_ArrayDisplay ( $pages )
	If UBound($pages) = 1 And Int(StringRight($array[$pages[0]], 1)) > $holpage Then
		$holpage = $holpage + 1
		$nav = $array[$pages[0]]
		ContinueLoop
	EndIf
	If UBound($pages) = 1 And Int(StringRight($array[$pages[0]], 1)) < $holpage Then
		ExitLoop
	EndIf
	If UBound($pages) > 1 Then
		If Int(StringRight($array[$pages[1]], 1)) < Int(StringRight($array[$pages[2]], 1)) Then
			$holpage = $holpage + 1
			$nav = $array[$pages[2]]
			ContinueLoop
		Else
			$holpage = $holpage + 1
			$nav = $array[$pages[1]]
			ContinueLoop
		EndIf
	EndIf
WEnd
