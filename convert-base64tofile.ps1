function convert-base64tofile
{
  [CmdletBinding()]
  param (
    [Parameter(Mandatory = $true)]
    [ValidatePattern('^([a-zA-Z]:\\|\\\\)(((?![<>:"/\\|?*]).)+((?<![ .])\\)?)*\.[a-zA-Z]+$')]
    [String]$File,
    [Parameter(Mandatory = $true)]
    [String]$base64string
  )
  [IO.File]::WriteAllBytes($File, [Convert]::FromBase64String($base64string))
}