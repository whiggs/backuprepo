      $Boxstarter.RebootOk=$true
Enable-MicrosoftUpdate # enable microsoft update
if (Test-PendingReboot) { Invoke-Reboot }
Install-WindowsUpdate -getUpdatesFromMS -acceptEula -SuppressReboots
if (Test-PendingReboot) { Invoke-Reboot }
Start-process $env:ComSpec -ArgumentList "/c winrm quickconfig /q" -Wait
<#
   netsh advfirewall firewall add rule name="Winrm HTTPS" protocol=TCP dir=in localport=5986 action=allow
netsh advfirewall firewall add rule name="Winrm HTTPS2" protocol=UDP dir=in localport=5986 action=allow
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Basic=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Digest=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Kerberos=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Negotiate=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Certificate=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{CredSSP=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client @{TrustedHost=`"*`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client @{AllowUnencrypted=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{Basic=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{Kerberos=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{Negotiate=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{Certificate=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{CredSSP=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{CbtHardeningLevel=`"None`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service @{IPv4Filter=`"*`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service @{EnableCompatibilityHttpListener=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service @{EnableCompatibilityHttpsListener=`"True`"}" -Wait -Verb Runas
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service @{AllowUnencrypted=`"True`"}" -Wait -Verb Runas
#>
