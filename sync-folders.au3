      ; *** Start added by AutoIt3Wrapper ***
#include <AutoItConstants.au3>
; *** End added by AutoIt3Wrapper ***
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Res_requestedExecutionLevel=highestAvailable
#AutoIt3Wrapper_Add_Constants=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.15.0 (Beta)
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
; *** Start added by AutoIt3Wrapper ***
#include <FileConstants.au3>
; *** End added by AutoIt3Wrapper ***
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.15.0 (Beta)
 Author:         myName

 Script Function:
    Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <File.au3>
#include <Array.au3>
#include <Date.au3>
#include <Console.au3>
#include <Crypt.au3>
#include <Constants.au3>

Cout ("")
_Crypt_Startup ()
$a = FileSelectFolder ( "Select destination (the folder you want to merge the files into)", "" )
$b = FileSelectFolder ( "Select source (the folder you want to remove files from and delete)", "" )
$dest = _FileListToArrayRec ( $a, "*", $FLTA_FILESFOLDERS, $FLTAR_RECUR )
$source = _FileListToArrayRec ( $b, "*", $FLTA_FILESFOLDERS, $FLTAR_RECUR )
For $i = 1 To $source[0] Step 1
    $index = _ArraySearch ( $dest, $source[$i] )
    If @error Then
        SetError (0)
        Cout ( "Copying " & $source[$i] & @CRLF )
        If IsFile ( $b & "\" & $source[$i] ) = 0 Then
            DirCopy ( $b & "\" & $source[$i], $a & "\" & $source[$i], 1 )
        Else
            FileCopy ( $b & "\" & $source[$i], $a & "\" & $source[$i], 9 )
        EndIf
    Else
        If IsFile ( $b & "\" & $source[$i] ) = 0 Then
            ContinueLoop
        Else
			#cs
            $hash = Run ( @ComSpec & " /c fciv -add " & '"' & $a & "\" & $dest[$index] & '"' & " -sha1", $checksumpath, @SW_HIDE, $STDOUT_CHILD )
            ProcessWaitClose ( $hash )
            $hash2 = Run ( @ComSpec & " /c fciv -add " & '"' & $b & "\" & $source[$i] & '"' & " -sha1", $checksumpath, @SW_HIDE, $STDOUT_CHILD )
            ProcessWaitClose ( $hash2 )
            $hashstring = StringTrimLeft ( StringTrimRight ( StdoutRead ( $hash ), 2 ), 58 )
            $hashstring2 = StringTrimLeft ( StringTrimRight ( StdoutRead ( $hash2 ), 2 ), 58 )
            $firstsplit = StringSplit ( $hashstring, " " )
            $secondsplit = StringSplit ( $hashstring2, " " )
            $num = 0
			#ce
			$hash = _Crypt_HashFile ( $a & "\" & $dest[$index], $CALG_SHA1 )
			$hash2 = _Crypt_HashFile ( $b & "\" & $source[$i], $CALG_SHA1 )
            If StringCompare ( $hash, $hash2 ) <> 0 Then
				$desttime2 = _Date_Time_GetFileTime ( $a & "\" & $dest[$index] )
                $sourcetime2 = _Date_Time_GetFileTime ( $b & "\" & $source[$i] )
                $compare2 = _Date_Time_CompareFileTime ( $desttime2[2], $sourcetime2[2] )
                Do
                    Cout ( "The file to be copied from the source (" & $source[$i] & ") has the same file name as a file in the destination, but the hash values are different." & @CRLF )
					If $compare2 = -1 Then
						Cout ( "The file in the source directory (" & $source[$i] & ") has a more recent time stamp than the file in the destination." & @CRLF )
					Else
						Cout ( "The file in the destination directory has a more recent time stamp than the file in the source directory." & @CRLF )
					EndIf
					Cout ( "What do you want to do?" & @CRLF & "[R]ename   [D]elete   [M]ove(Replace)   [N]othing" & @CRLF )
                    $user = Getch ()
                Until $user == "r" Or $user == "R" Or $user == "d" Or $user == "D" Or $user == "m" Or $user == "M" Or $user == "N" Or $user == "n"
                Cout (@CRLF)
                Select
                    Case $user == "r" Or $user == "R"
                        $holdstring = ""
                        $filepathstring = StringSplit ( $source[$i], "\" )
                        If @error Then
                            SetError (0)
                            $justfile = $source[$i]
                        Else
                            $justfile = $filepathstring[$filepathstring[0]]
							For $hh = 1 To $filepathstring[0] - 1 Step 1
								$holdstring = $holdstring & $filepathstring[$hh] & "\"
							Next
							$holdstring = StringTrimRight ( $holdstring, 1 )
						EndIf


                        $rename = StringSplit ( $justfile, "." )
                        $newname = ""
                        $num = 0
                            Do
								$newname = ""
                                $num += 1
								For $f = 1 To $rename[0] Step 1
									If $f = $rename[0] - 1 Then
										$newname = $newname & $rename[$f] & "(" & $num & ")."
									Else
										$newname = $newname & $rename[$f] & "."
									EndIf
								Next
								$newname = StringTrimRight ( $newname, 1 )
                                $foldhold = ""
                                If $holdstring == "" Then
                                    $foldhold = $newname
                                Else
                                    $foldhold = $holdstring & "\" & $newname
                                EndIf


                            Until Not FileExists ( $a & "\" & $foldhold )
                            Cout ( "Copying " & $foldhold & @CRLF )
                           ; Do
                                FileCopy ( $b & "\" & $source[$i], $a & "\" & $foldhold, 9 )
                            ;Until Not FileExists ( $b & "\" & $source[$i] ) And FileExists ( $a & "\" & $foldhold )


                    Case $user == "d" Or $user == "D"
                        Cout ( "Deleting " & $source[$i] & @CRLF )
                        Do
                            FileDelete ( $b & "\" & $source[$i] )
                        Until Not FileExists ( $b & "\" & $source[$i] )
                    Case $user == "m" Or $user == "M"
                        Cout ( "Copying " & $source[$i] & @CRLF )
                        ;Do
                            FileCopy ( $b & "\" & $source[$i], $a & "\" & $source[$i], 9 )
                       ;Until Not FileExists ( $b & "\" & $source[$i] )
					Case $user == "N" Or $user == "n"
						ContinueLoop
                EndSelect
            Else
                $desttime = _Date_Time_GetFileTime ( $a & "\" & $dest[$index] )
                $sourcetime = _Date_Time_GetFileTime ( $b & "\" & $source[$i] )
                $compare = _Date_Time_CompareFileTime ( $desttime[2], $sourcetime[2] )
				#cs
                If $compare = 0 Or $compare = 1 Then
                    Cout ( "Deleting " & $source[$i] & @CRLF )
                    Do
                        FileDelete ( $b & "\" & $source[$i] )
				Until Not FileExists ( $b & "\" & $source[$i] )
				#ce

                If $compare = -1 Then
                    Cout ( "Copying " & $source[$i] & @CRLF )
                    ;Do
                        FileCopy ( $b & "\" & $source[$i], $a & "\" & $source[$i], 9 )
                    ;Until Not FileExists ( $b & "\" & $source[$i] )
                Else
                    Cout ( "Skipping file " & $source[$i] & @CRLF )
                EndIf
            EndIf

        EndIf
    EndIf
Next
_Crypt_Shutdown ()
;DirRemove ( $b, $DIR_REMOVE )


Func IsFile($sFilePath)
	Return Number(FileExists($sFilePath) And StringInStr(FileGetAttrib($sFilePath), "D", 2, 1) = 0)
EndFunc   ;==>IsFile
Func _GetFilename($sFilePath)
    Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
    Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
    If IsObj($oColFiles) Then
        For $oObjectFile In $oColFiles
            Return $oObjectFile.FileName
        Next
    EndIf
    Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilename

Func _GetFilenameExt($sFilePath)
    Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
    Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
    If IsObj($oColFiles) Then
        For $oObjectFile In $oColFiles
            Return $oObjectFile.Extension
        Next
    EndIf
    Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenameExt

Func _GetFilenameInt($sFilePath)
    Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
    Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
    If IsObj($oColFiles) Then
        For $oObjectFile In $oColFiles
            Return $oObjectFile.Name
        Next
    EndIf
    Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenameInt

Func _GetFilenameDrive($sFilePath)
    Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
    Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
    If IsObj($oColFiles) Then
        For $oObjectFile In $oColFiles
            Return StringUpper($oObjectFile.Drive)
        Next
    EndIf
    Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenameDrive

Func _GetFilenamePath($sFilePath)
    Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
    Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
    If IsObj($oColFiles) Then
        For $oObjectFile In $oColFiles
            Return $oObjectFile.Path
        Next
    EndIf
    Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenamePath
