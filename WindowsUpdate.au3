#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Change2CUI=y
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.15.0 (Beta)
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
$ssDefault = 0
$ssManagedServer = 1
$ssWindowsUpdate = 2
$ssOthers = 3


$intSearchStartChar = 1


Dim $strTitle


$updateSession = ObjCreate("Microsoft.Update.Session")
$updateSearcher = $updateSession.CreateupdateSearcher()

$updateSearcher.ServerSelection = $ssWindowsUpdate
$searchResult = $updateSearcher.Search("IsInstalled=0 and Type='Software'")

ConsoleWrite ( "List of applicable items on the machine:" & @CRLF )

For $i = 0 To $searchResult.Updates.Count - 1 Step 1
    $update = $searchResult.Updates.Item($i)
    ConsoleWrite ( $i + 1 & " > " & $update.Title & @CRLF )
Next

If $searchResult.Updates.Count = 0 Then
    ConsoleWrite ( "There are no applicable updates." & @CRLF )
    Exit
EndIf

ConsoleWrite ( "Creating collection of updates to download:" & @CRLF )

$updatesToDownload = ObjCreate("Microsoft.Update.UpdateColl")

For $i = 0 to $searchResult.Updates.Count - 1 Step 1
    $update = $searchResult.Updates.Item($i)
    $addThisUpdate = false
    If $update.InstallationBehavior.CanRequestUserInput = true Then
        ConsoleWrite ( $i + 1 & " > skipping: " & $update.Title & " because it requires user input" & @CRLF )
    Else
        If $update.EulaAccepted = false Then
            ConsoleWrite ( $i + 1 & " > note: " & $update.Title & " has a license agreement that must be accepted:" & @CRLF )
            ConsoleWrite ( $update.EulaText & @CRLF )
            ConsoleWrite ( "Accepting agreement" & @CRLF )
			$update.AcceptEula()
            $addThisUpdate = true
        Else
            $addThisUpdate = true
        EndIf
    EndIf
    If $addThisUpdate = true Then
        ConsoleWrite ( $i + 1 & " > adding: " & $update.Title & @CRLF )
        $updatesToDownload.Add($update)
    EndIf
Next

If $updatesToDownload.Count = 0 Then
    ConsoleWrite ( "All applicable updates were skipped." & @CRLF )
    Exit
EndIf

ConsoleWrite ( "Downloading updates..." & @CRLF )

$downloader = $updateSession.CreateUpdateDownloader()
$downloader.Updates = $updatesToDownload
$downloader.Download()

$updatesToInstall = ObjCreate("Microsoft.Update.UpdateColl")

$rebootMayBeRequired = false

ConsoleWrite ( "Successfully downloaded updates:" & @CRLF )

For $i = 0 To $searchResult.Updates.Count - 1 Step 1
    $update = $searchResult.Updates.Item($i)
    If $update.IsDownloaded = true Then
        ConsoleWrite ( $i + 1 & " > " & $update.Title & @CRLF )
        $updatesToInstall.Add($update)
        If $update.InstallationBehavior.RebootBehavior > 0 Then
            $rebootMayBeRequired = true
        EndIf
    EndIf
Next

If $updatesToInstall.Count = 0 Then
    ConsoleWrite ( "No updates were successfully downloaded." & @CRLF )
    Exit
EndIf

If $rebootMayBeRequired = true Then
    ConsoleWrite ( "These updates may require a reboot." & @CRLF )
EndIf

    ConsoleWrite ( "Installing updates..." & @CRLF )
    $installer = $updateSession.CreateUpdateInstaller()
    $installer.Updates = $updatesToInstall
    $installationResult = $installer.Install()


    ConsoleWrite ( "Installation Result: " & $installationResult.ResultCode & @CRLF )
    ConsoleWrite ( "Reboot Required: " & $installationResult.RebootRequired & @CRLF )
    ConsoleWrite ( "Listing of updates installed and individual installation results:" & @CRLF )

    For $i = 0 to $updatesToInstall.Count - 1 Step 1
        ConsoleWrite ( $i + 1 & " > " & $updatesToInstall.Item($i).Title & ": " & $installationResult.GetUpdateResult($i).ResultCode & @CRLF )
    Next
