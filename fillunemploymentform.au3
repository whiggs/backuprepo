#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Add_Constants=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.15.0
 Author:         whiggs

 Script Function:
	I was recently put into a situation where I needed to claim unemployment benefits.  I don't know how many of you have ever had to do this before, but, as part of the process,
	you are required to submit a "Weekly Work Search" report, detailing at least 3 different jobs that you applied to during the week you are trying to claim unemployment benefits
	for.  However, for some reason (I guess the government felt that they didn't want to make the process too easy for people claiming unemployment), the ability to paste text
	into the input boxes is disabled in the form, requiring that individuals attempting to fill out the form type in all of the information manually.  As you can imagine, I found
	this to be stupid, a waste of time, and exactly the kind of asinine thing the government would do to people who are currently at a low point in their lives, so I decided
	to give whoever made this decision the proverbial middle finger by writing this script.  This solution allows you to, rather than copy and paste the information for the jobs you have applied for
	directly into the online form, you can instead paste the information into an excel spreadsheet, and when you have saved the information for the 3 jobs that you applied for
	in the spreadsheet, you run this script, and the script will prompt you to select the spreadsheet you just created, open internet explorer to the webpage containing the form,
	will input your information to access the form, and will manually type out all of the information the form requires, including your name, phone number, the date for the week
	you are submitting the report for, and all of the information for the jobs you applied for contained in	the spreadsheet.  The excel spreadsheet must be structured as follows:
	Columns "A", "B", and "C" will contain the information for each of the jobs you applied for, row 1 of each column should contain the name of the company you applied to, row
	2 of each column should contain the phone number for the company you applied to (no special formatting, just input the numbers for the phone number in this row), row 3 of
	each column will contain the street address for the company you applied to, row 4 of each column will contain the secondary address (suite number/letter) for the company
	you applied to if applicable (if not applicable, leave blank and the script will skip over it), row 5 for each column will contain the city of the company you applied to,
	row 6 for each column will contain the state of the company you applied to (two letter abbreviation only), row 7 of each column will contain the zip code of the company you
	applied to, rows 8 and 9 for each column should be left blank (the script assumes you are filling out the form for the previous week and will calculate the date 6 days prior
	to the current date and fill in that date as the date you applied for the position), row 10 of each column should contain the method by which you applied for the position (I
	usually just put "internet" for this row), row 11 of each column should contain the title of the position which you applied for, and finally, row 12 of each column should
	contain the current status of your submission of the job application (I usually put "still waiting", but other valid options are "spoke to hiring manager", "scheduled onsite
	interview", "had phone interview", etc.)  This script has saved me a lot of time and completely circumvents the online form's attempt prevent you from pasting the information
	into the form.  If you find yourself in the unfortunate position of needing to fill out the weekly job search form to collect your weekly unemployment benefits, then you can
	make the process much easier by running this script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <Array.au3>
#include <Excel.au3>
#include <Date.au3>
#include <Crypt.au3>
#include <IE.au3>
_Crypt_Startup ()
If Not FileExists ( @TempDir & "\unemploy.ini" ) Then
If Not IsDeclared("sInputBoxAnswer") Then Local $sInputBoxAnswer
	$sInputBoxAnswer = InputBox("Enter full name","Enter your Full name",""," M")
	Select
		Case @Error = 0 ;OK - The string returned is valid
			IniWrite ( @TempDir & "\unemploy.ini", "Settings", "Name", $sInputBoxAnswer )
		Case @Error = 1 ;The Cancel button was pushed
			Exit
		Case @Error = 3 ;The InputBox failed to open
			Exit
	EndSelect
	$sInputBoxAnswer2 = InputBox("Enter phone number","Enter your phone number",""," M")
	Select
		Case @Error = 0 ;OK - The string returned is valid
			$new = ""
			For $h = 1 To StringLen ( $sInputBoxAnswer2 ) Step 1
				If StringMid ( $sInputBoxAnswer2, $h, 1 ) = "0" Then
					$new = $new & StringMid ( $sInputBoxAnswer2, $h, 1 )
				Else
					If Int ( StringMid ( $sInputBoxAnswer2, $h, 1 ) ) <> 0 Then
						$new = $new & StringMid ( $sInputBoxAnswer2, $h, 1 )
					Else
						ContinueLoop
					EndIf
				EndIf
			Next
			IniWrite ( @TempDir & "\unemploy.ini", "Settings", "Phone", $new )
		Case @Error = 1 ;The Cancel button was pushed
			Exit
		Case @Error = 3 ;The InputBox failed to open
			Exit
	EndSelect
	$sInputBoxAnswer3 = InputBox("Social security number","Enter your Social Security number (don't worry, the value will be encrypted on disk)",""," M")
	Select
		Case @Error = 0 ;OK - The string returned is valid
			$new = ""
			For $h = 1 To StringLen ( $sInputBoxAnswer3 ) Step 1
				If StringMid ( $sInputBoxAnswer3, $h, 1 ) = "0" Then
					$new = $new & StringMid ( $sInputBoxAnswer3, $h, 1 )
				Else
					If Int ( StringMid ( $sInputBoxAnswer3, $h, 1 ) ) <> 0 Then
						$new = $new & StringMid ( $sInputBoxAnswer3, $h, 1 )
					Else
						ContinueLoop
					EndIf
				EndIf
			Next
			IniWrite ( @TempDir & "\unemploy.ini", "Settings", "SSN", _Crypt_EncryptData ( StringToBinary ( $new ), "7D11Ej9Tuq831Fo7IT3g8ySwVRpoX14Kj39f4Z61024R7o6kzw04YNlPzmLXbFVp4v51IpkoGvcytMuIwEju1LTc7nh9sAiDVn", $CALG_AES_256 ) )
		Case @Error = 1 ;The Cancel button was pushed
			Exit
		Case @Error = 3 ;The InputBox failed to open
			Exit
	EndSelect
	$sInputBoxAnswer4 = InputBox("PIN","Enter your PIN for the Department of labor website",""," M")
	Select
		Case @Error = 0 ;OK - The string returned is valid
			IniWrite ( @TempDir & "\unemploy.ini", "Settings", "PIN", $sInputBoxAnswer4 )
		Case @Error = 1 ;The Cancel button was pushed
			Exit
		Case @Error = 3 ;The InputBox failed to open
			Exit
	EndSelect
EndIf
$name = IniRead ( @TempDir & "\unemploy.ini", "Settings", "Name", "" )
$phone = IniRead ( @TempDir & "\unemploy.ini", "Settings", "Phone", "" )
$ssn = BinaryToString ( _Crypt_DecryptData ( IniRead ( @TempDir & "\unemploy.ini", "Settings", "SSN", "" ), "7D11Ej9Tuq831Fo7IT3g8ySwVRpoX14Kj39f4Z61024R7o6kzw04YNlPzmLXbFVp4v51IpkoGvcytMuIwEju1LTc7nh9sAiDVn", $CALG_AES_256 ) )
$pin = IniRead ( @TempDir & "\unemploy.ini", "Settings", "PIN", "" )

_Crypt_Shutdown ()
$sel = FileOpenDialog ( "Select the info file", @DesktopDir, "Excel Books (*.xlsx)", 3 )
If $sel = Null Or $sel = "" Then
	Exit
EndIf
$diff = @WDAY - @MDAY
$year = @YEAR
$month = @MON
$day = @MDAY
If @WDAY < 7 Then
	If Int ( @MDAY ) <= @WDAY Then
		If Int ( @MON ) = 1 Then
			$daysinmon = _DateDaysInMonth ( @YEAR - 1, 12 )
			$year = $year - 1
			$month = 12
		Else
			$daysinmon = _DateDaysInMonth ( @YEAR, @MON - 1 )
			$month = $month - 1
		EndIf
		If @WDAY - Int ( @MDAY ) = 0 Then
			$day = $daysinmon
		Else
			$diff = @wday - Int ( @MDAY )
			$day = $daysinmon - $diff
		EndIf
	Else
		$day = Int ( @MDAY ) - @WDAY
	EndIf
EndIf
If StringLen ( $day ) = 1 Then
	$day = "0" & $day
EndIf
If StringLen ( $month ) = 1 Then
	$month = "0" & $month
EndIf

$date = _NowCalcDate ( )
$newdate = _DateAdd ( "D", -6, $date )
$newdate = _DateTimeFormat ( $newdate, 2 )
$split = StringSplit ( $newdate, "/" )
If StringLen ( $split[1] ) = 1 Then
	$split[1] = "0" & $split[1]
EndIf
If StringLen ( $split[2] ) = 1 Then
	$split[2] = "0" & $split[2]
EndIf

$oex = _Excel_Open ()
$book = _Excel_BookOpen ( $oex, $sel )

;$win = WinWait ( "[Class:Notepad]" )
;$win = WinWait ( "Weekly Work Search Entry/Inquiry - Google Chrome" )
;WinActivate ( $win )
;WinWaitActive ( $win )
$oie = _IECreate ( "https://www.dol.state.ga.us/Access/Service/SebWorkSearch" )
_IELoadWait ( $oie )
Sleep ( 3000 )
$form = _IEFormGetObjByName ( $oie, "SebWorkSearchActionForm" )
$ssnpre = _IEFormElementGetObjByName ( $form, "ssnPrefix" )
$ssnmiddle = _IEFormElementGetObjByName ( $form, "ssnMiddle" )
$ssnlast = _IEFormElementGetObjByName ( $form, "ssnSuffix" )
_IEFormElementSetValue ( $ssnpre, StringLeft ( $ssn, 3 ) )
_IEFormElementSetValue ( $ssnmiddle, StringMid ( $ssn, 4, 2 ) )
_IEFormElementSetValue ( $ssnlast, StringRight ( $ssn, 4 ) )
$pinin = _IEFormElementGetObjByName ( $form, "pin" )
_IEFormElementSetValue ( $pinin, $pin )
$button = _IEFormElementGetObjByName ( $form, "pageAction" )
_IEAction ( $button, "click" )
_IELoadWait ( $oie )
Sleep ( 2000 )
$range = _Excel_RangeRead ( $book, Default, "A1:A12" )
$form2 = _IEFormGetCollection ( $oie, 0 )
$nameinput = _IEFormElementGetObjByName ( $form2, "name" )
_IEFormElementSetValue ( $nameinput, $name )
$phonearea = _IEFormElementGetObjByName ( $form2, "phoneAreaCode" )
_IEFormElementSetValue ( $phonearea, StringLeft ( $phone, 3 ) )
$phonemid = _IEFormElementGetObjByName ( $form2, "phoneMiddleThree" )
_IEFormElementSetValue ( $phonemid, StringMid ( $phone, 4, 3 ) )
$phonelast = _IEFormElementGetObjByName ( $form2, "phoneLastFour" )
_IEFormElementSetValue ( $phonelast, StringRight ( $phone, 4 ) )
$monin = _IEFormElementGetObjByName ( $form2, "wedMonth" )
_IEFormElementSetValue ( $monin, $month )

$dayin = _IEFormElementGetObjByName ( $form2, "wedDay" )
_IEFormElementSetValue ( $dayin, $day )
$yearin = _IEFormElementGetObjByName ( $form2, "wedYear" )
_IEFormElementSetValue ( $yearin, StringRight ( $year, 2 ) )
$emp1 = _IEFormElementGetObjByName ( $form2, "emplr1Name" )
_IEFormElementSetValue ( $emp1, $range[0] )
$phonearea1 = _IEFormElementGetObjByName ( $form2, "emplr1PhoneAreaCode" )
_IEFormElementSetValue ( $phonearea1, StringLeft ( $range[1], 3 ) )
$phonemid1 = _IEFormElementGetObjByName ( $form2, "emplr1PhoneMiddleThree" )
_IEFormElementSetValue ( $phonemid1, StringMid ( $range[1], 4, 3 ) )
$phonelast1 = _IEFormElementGetObjByName ( $form2, "emplr1PhoneLastFour" )
_IEFormElementSetValue ( $phonelast1, StringRight ( $range[1], 4 ) )
$streetadd1 = _IEFormElementGetObjByName ( $form2, "emplr1StreetAddrr1" )
_IEFormElementSetValue ( $streetadd1, $range[2] )
If $range[3] <> Null And $range[3] <> "" Then
	$secondadd1 = _IEFormElementGetObjByName ( $form2, "emplr1StreetAddrr2" )
	_IEFormElementSetValue ( $secondadd1, $range[3] )
EndIf

$city1 = _IEFormElementGetObjByName ( $form2, "emplr1City" )
_IEFormElementSetValue ( $city1, $range[4] )
$state1 = _IEFormElementGetObjByName ( $form2, "emplr1State" )
_IEFormElementSetValue ( $state1, $range[5] )
$zip1 = _IEFormElementGetObjByName ( $form2, "emplr1Zip" )
_IEFormElementSetValue ( $zip1, $range[6] )
$moncont1 = _IEFormElementGetObjByName ( $form2, "emplr1MonContacted" )
_IEFormElementSetValue ( $moncont1, $split[1] )
$daycont1 = _IEFormElementGetObjByName ( $form2, "emplr1DayContacted" )
_IEFormElementSetValue ( $daycont1, $split[2] )
$yearcont1 = _IEFormElementGetObjByName ( $form2, "emplr1YrContacted" )
_IEFormElementSetValue ( $yearcont1, StringRight ( $split[3], 2 ) )
_IEFormElementRadioSelect ( $form2, "Y", "emplr1NewOrFollowUp" )
$applyman1 = _IEFormElementGetObjByName ( $form2, "emplr1AppliedManner" )
_IEFormElementSetValue ( $applyman1, $range[9] )
_IEFormElementRadioSelect ( $form2, "Y", "emplr1ResumeTaken" )
$jobtit1 = _IEFormElementGetObjByName ( $form2, "emplr1JobApplied" )
_IEFormElementSetValue ( $jobtit1, $range[10] )
$result1 = _IEFormElementGetObjByName ( $form2, "emplr1ContactResult" )
_IEFormElementSetValue ( $result1, $range[11] )
$range = _Excel_RangeRead ( $book, Default, "B1:B12" )
$emp2 = _IEFormElementGetObjByName ( $form2, "emplr2Name" )
_IEFormElementSetValue ( $emp2, $range[0] )
$phonearea2 = _IEFormElementGetObjByName ( $form2, "emplr2PhoneAreaCode" )
_IEFormElementSetValue ( $phonearea2, StringLeft ( $range[1], 3 ) )
$phonemid2 = _IEFormElementGetObjByName ( $form2, "emplr2PhoneMiddleThree" )
_IEFormElementSetValue ( $phonemid2, StringMid ( $range[1], 4, 3 ) )
$phonelast2 = _IEFormElementGetObjByName ( $form2, "emplr2PhoneLastFour" )
_IEFormElementSetValue ( $phonelast2, StringRight ( $range[1], 4 ) )
$streetadd2 = _IEFormElementGetObjByName ( $form2, "emplr2StreetAddrr1" )
_IEFormElementSetValue ( $streetadd2, $range[2] )
If $range[3] <> Null And $range[3] <> "" Then
	$secondadd2 = _IEFormElementGetObjByName ( $form2, "emplr2StreetAddrr2" )
	_IEFormElementSetValue ( $secondadd2, $range[3] )
EndIf

$city2 = _IEFormElementGetObjByName ( $form2, "emplr2City" )
_IEFormElementSetValue ( $city2, $range[4] )
$state2 = _IEFormElementGetObjByName ( $form2, "emplr2State" )
_IEFormElementSetValue ( $state2, $range[5] )
$zip2 = _IEFormElementGetObjByName ( $form2, "emplr2Zip" )
_IEFormElementSetValue ( $zip2, $range[6] )
$moncont2 = _IEFormElementGetObjByName ( $form2, "emplr2MonContacted" )
_IEFormElementSetValue ( $moncont2, $split[1] )
$daycont2 = _IEFormElementGetObjByName ( $form2, "emplr2DayContacted" )
_IEFormElementSetValue ( $daycont2, $split[2] )
$yearcont2 = _IEFormElementGetObjByName ( $form2, "emplr2YrContacted" )
_IEFormElementSetValue ( $yearcont2, StringRight ( $split[3], 2 ) )
_IEFormElementRadioSelect ( $form2, "Y", "emplr2NewOrFollowUp" )
$applyman2 = _IEFormElementGetObjByName ( $form2, "emplr2AppliedManner" )
_IEFormElementSetValue ( $applyman2, $range[9] )
_IEFormElementRadioSelect ( $form2, "Y", "emplr2ResumeTaken" )
$jobtit2 = _IEFormElementGetObjByName ( $form2, "emplr2JobApplied" )
_IEFormElementSetValue ( $jobtit2, $range[10] )
$result2 = _IEFormElementGetObjByName ( $form2, "emplr2ContactResult" )
_IEFormElementSetValue ( $result2, $range[11] )
$range = _Excel_RangeRead ( $book, Default, "C1:C12" )
$emp3 = _IEFormElementGetObjByName ( $form2, "emplr3Name" )
_IEFormElementSetValue ( $emp3, $range[0] )
$phonearea3 = _IEFormElementGetObjByName ( $form2, "emplr3PhoneAreaCode" )
_IEFormElementSetValue ( $phonearea3, StringLeft ( $range[1], 3 ) )
$phonemid3 = _IEFormElementGetObjByName ( $form2, "emplr3PhoneMiddleThree" )
_IEFormElementSetValue ( $phonemid3, StringMid ( $range[1], 4, 3 ) )
$phonelast3 = _IEFormElementGetObjByName ( $form2, "emplr3PhoneLastFour" )
_IEFormElementSetValue ( $phonelast3, StringRight ( $range[1], 4 ) )
$streetadd3 = _IEFormElementGetObjByName ( $form2, "emplr3StreetAddrr1" )
_IEFormElementSetValue ( $streetadd3, $range[2] )
If $range[3] <> Null And $range[3] <> "" Then
	$secondadd3 = _IEFormElementGetObjByName ( $form2, "emplr3StreetAddrr2" )
	_IEFormElementSetValue ( $secondadd3, $range[3] )
EndIf

$city3 = _IEFormElementGetObjByName ( $form2, "emplr3City" )
_IEFormElementSetValue ( $city3, $range[4] )
$state3 = _IEFormElementGetObjByName ( $form2, "emplr3State" )
_IEFormElementSetValue ( $state3, $range[5] )
$zip3 = _IEFormElementGetObjByName ( $form2, "emplr3Zip" )
_IEFormElementSetValue ( $zip3, $range[6] )
$moncont3 = _IEFormElementGetObjByName ( $form2, "emplr3MonContacted" )
_IEFormElementSetValue ( $moncont3, $split[1] )
$daycont3 = _IEFormElementGetObjByName ( $form2, "emplr3DayContacted" )
_IEFormElementSetValue ( $daycont3, $split[2] )
$yearcont3 = _IEFormElementGetObjByName ( $form2, "emplr3YrContacted" )
_IEFormElementSetValue ( $yearcont3, StringRight ( $split[3], 2 ) )
_IEFormElementRadioSelect ( $form2, "Y", "emplr3NewOrFollowUp" )
$applyman3 = _IEFormElementGetObjByName ( $form2, "emplr3AppliedManner" )
_IEFormElementSetValue ( $applyman3, $range[9] )
_IEFormElementRadioSelect ( $form2, "Y", "emplr3ResumeTaken" )
$jobtit3 = _IEFormElementGetObjByName ( $form2, "emplr3JobApplied" )
_IEFormElementSetValue ( $jobtit3, $range[10] )
$result3 = _IEFormElementGetObjByName ( $form2, "emplr3ContactResult" )
_IEFormElementSetValue ( $result3, $range[11] )
$sub = _IEFormElementGetObjByName ( $form2, "pageAction" )
_IEAction ( $sub, "click" )
_Excel_BookClose ( $book )
_Excel_Close ( $oex )
