      function Use-RunAs 
{    
    # Check if script is running as Adminstrator and if not use RunAs 
    # Use Check Switch to check if admin 
     
    param([Switch]$Check) 
     
    $IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()` 
        ).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") 
         
    if ($Check) { return $IsAdmin }     
 
    if ($MyInvocation.ScriptName -ne "") 
    {  
        if (-not $IsAdmin)  
        {  
            try 
            {  
                $arg = "-file `"$($MyInvocation.ScriptName)`"" 
                Start-Process "$psHome\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop'  
            } 
            catch 
            { 
                Write-Warning "Error - Failed to restart script with runas"  
                break               
            } 
            exit # Quit this session of powershell 
        }  
    }  
    else  
    {  
        Write-Warning "Error - Script must be saved as a .ps1 file first"  
        break  
    }  
} 
function Show-MsgBox
{

 [CmdletBinding()]
    param(
    [Parameter(Position=0, Mandatory=$true)] [string]$Prompt,
    [Parameter(Position=1, Mandatory=$false)] [string]$Title ="",
    [Parameter(Position=2, Mandatory=$false)] [ValidateSet("Information", "Question", "Critical", "Exclamation")] [string]$Icon ="Information",
    [Parameter(Position=3, Mandatory=$false)] [ValidateSet("OKOnly", "OKCancel", "AbortRetryIgnore", "YesNoCancel", "YesNo", "RetryCancel")] [string]$BoxType ="OkOnly",
    [Parameter(Position=4, Mandatory=$false)] [ValidateSet(1,2,3)] [int]$DefaultButton = 1
    )
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic") | Out-Null
switch ($Icon) {
            "Question" {$vb_icon = [microsoft.visualbasic.msgboxstyle]::Question }
            "Critical" {$vb_icon = [microsoft.visualbasic.msgboxstyle]::Critical}
            "Exclamation" {$vb_icon = [microsoft.visualbasic.msgboxstyle]::Exclamation}
            "Information" {$vb_icon = [microsoft.visualbasic.msgboxstyle]::Information}}
switch ($BoxType) {
            "OKOnly" {$vb_box = [microsoft.visualbasic.msgboxstyle]::OKOnly}
            "OKCancel" {$vb_box = [microsoft.visualbasic.msgboxstyle]::OkCancel}
            "AbortRetryIgnore" {$vb_box = [microsoft.visualbasic.msgboxstyle]::AbortRetryIgnore}
            "YesNoCancel" {$vb_box = [microsoft.visualbasic.msgboxstyle]::YesNoCancel}
            "YesNo" {$vb_box = [microsoft.visualbasic.msgboxstyle]::YesNo}
            "RetryCancel" {$vb_box = [microsoft.visualbasic.msgboxstyle]::RetryCancel}}
switch ($Defaultbutton) {
            1 {$vb_defaultbutton = [microsoft.visualbasic.msgboxstyle]::DefaultButton1}
            2 {$vb_defaultbutton = [microsoft.visualbasic.msgboxstyle]::DefaultButton2}
            3 {$vb_defaultbutton = [microsoft.visualbasic.msgboxstyle]::DefaultButton3}}
$popuptype = $vb_icon -bor $vb_box -bor $vb_defaultbutton
$ans = [Microsoft.VisualBasic.Interaction]::MsgBox($prompt,$popuptype,$title)
return $ans
} #end function
Use-Runas
Get-WUInstall -autoreboot -acceptall -MicrosoftUpdate
Start-Sleep 10
Remove-RegistryKeyValue -Path 'hklm:\Software\Microsoft\Windows\CurrentVersion\Run' -Name "deploy"
Import-Module "C:\files\autoit\install\AutoItX\AutoItX.psd1"
Initialize-AU3
Invoke-WebRequest "https://live.sysinternals.com/autologon.exe" -OutFile "$env:APPDATA\auto.exe"
Start-Process "$env:APPDATA\auto.exe" -ArgumentList "-accepteula"
Wait-AU3Win -Title "Autologon - Sysinternals" -Text "Domain:"
Show-AU3WinActivate -Title "Autologon - Sysinternals" -Text "Domain:"
Wait-AU3WinActive -Title "Autologon - Sysinternals" -Text "Domain:"
Invoke-AU3ControlClick -Title "Autologon - Sysinternals" -Control 2 -Text "Domain:"
Wait-AU3Win -Title "Autologon" -Text "AutoLogon is disabled."
Show-AU3WinActivate -Title "Autologon" -Text "AutoLogon is disabled."
Wait-AU3WinActive -Title "Autologon" -Text "AutoLogon is disabled."
Invoke-AU3ControlClick -Title "Autologon" -Control 2 -Text "AutoLogon is disabled."
Set-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name EnableLUA  -Value 1
$passwordfile = "C:\files\creds.txt"
$securepassword = (Cat $passwordfile | ConvertTo-SecureString)
$plainpassword = (New-Object System.Management.Automation.PSCredential arborpharma01\providynadmin, $securepassword).GetNetworkCredential().Password
rasdial "Arbor VPN" providynadmin $plainpassword /Domain:Arborpharma01
start-process "C:\files\pkkg.exe"
