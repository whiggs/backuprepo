function convert-filetobase64
{
  [CmdletBinding()]
  param (
    [Parameter(Mandatory = $true)]
    [ValidatePattern('^([a-zA-Z]:\\|\\\\)(((?![<>:"/\\|?*]).)+((?<![ .])\\)?)*\.[a-zA-Z]+$')]
    [ValidateScript({ Test-Path $_ -PathType Leaf })]
    [String]$File
  )
  $base64string = [Convert]::ToBase64String([IO.File]::ReadAllBytes($File))
  return $base64string
}