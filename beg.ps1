<#
    .DESCRIPTION
	Automates the configuration of my operating system after the custom Windows 10 image I created (along with unattend xml file 
	for complete automation) is deployed.  Batch script which downloads and runs script put in “start” folder of offline image.
	This allows me to edit and update the script as I see fit without needing to continuously replace the script in the offline 
	image.  Configuration of autologon is performed locally for security purposes.
#>
Function Show-DesktopIcons{
$ErrorActionPreference = "SilentlyContinue"
If ($Error) {$Error.Clear()}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
If (Test-Path $RegistryPath) {
	$Res = Get-ItemProperty -Path $RegistryPath -Name "HideIcons"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "HideIcons" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "HideIcons").HideIcons
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "HideIcons" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons"
If (-Not(Test-Path $RegistryPath)) {
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer" -Name "HideDesktopIcons" -Force | Out-Null
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons" -Name "NewStartPanel" -Force | Out-Null
}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel"
If (-Not(Test-Path $RegistryPath)) {
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons" -Name "NewStartPanel" -Force | Out-Null
}
If (Test-Path $RegistryPath) {
	## -- My Computer
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}")."{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Control Panel
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}")."{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- User's Files
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}")."{59031a47-3f72-44a7-89c5-5595fe6b30ee}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Recycle Bin
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}")."{645FF040-5081-101B-9F08-00AA002F954E}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Network
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}")."{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
}
If ($Error) {$Error.Clear()}
}
Function Get-PendingReboot
{
[CmdletBinding()]
param(
	[Parameter(Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
	[Alias("CN","Computer")]
	[String[]]$ComputerName="$env:COMPUTERNAME",
	[String]$ErrorLog
	)

Begin {  }## End Begin Script Block
Process {
  Foreach ($Computer in $ComputerName) {
	Try {
	    ## Setting pending values to false to cut down on the number of else statements
	    $CompPendRen,$PendFileRename,$Pending,$SCCM = $false,$false,$false,$false
                        
	    ## Setting CBSRebootPend to null since not all versions of Windows has this value
	    $CBSRebootPend = $null
						
	    ## Querying WMI for build version
	    $WMI_OS = Get-WmiObject -Class Win32_OperatingSystem -Property BuildNumber, CSName -ComputerName $Computer -ErrorAction Stop

	    ## Making registry connection to the local/remote computer
	    $HKLM = [UInt32] "0x80000002"
	    $WMI_Reg = [WMIClass] "\\$Computer\root\default:StdRegProv"
						
	    ## If Vista/2008 & Above query the CBS Reg Key
	    If ([Int32]$WMI_OS.BuildNumber -ge 6001) {
		    $RegSubKeysCBS = $WMI_Reg.EnumKey($HKLM,"SOFTWARE\Microsoft\Windows\CurrentVersion\Component Based Servicing\")
		    $CBSRebootPend = $RegSubKeysCBS.sNames -contains "RebootPending"		
	    }
							
	    ## Query WUAU from the registry
	    $RegWUAURebootReq = $WMI_Reg.EnumKey($HKLM,"SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\")
	    $WUAURebootReq = $RegWUAURebootReq.sNames -contains "RebootRequired"
						
	    ## Query PendingFileRenameOperations from the registry
	    $RegSubKeySM = $WMI_Reg.GetMultiStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\Session Manager\","PendingFileRenameOperations")
	    $RegValuePFRO = $RegSubKeySM.sValue

	    ## Query JoinDomain key from the registry - These keys are present if pending a reboot from a domain join operation
	    $Netlogon = $WMI_Reg.EnumKey($HKLM,"SYSTEM\CurrentControlSet\Services\Netlogon").sNames
	    $PendDomJoin = ($Netlogon -contains 'JoinDomain') -or ($Netlogon -contains 'AvoidSpnSet')

	    ## Query ComputerName and ActiveComputerName from the registry
	    $ActCompNm = $WMI_Reg.GetStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\ComputerName\ActiveComputerName\","ComputerName")            
	    $CompNm = $WMI_Reg.GetStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName\","ComputerName")

	    If (($ActCompNm -ne $CompNm) -or $PendDomJoin) {
	        $CompPendRen = $true
	    }
						
	    ## If PendingFileRenameOperations has a value set $RegValuePFRO variable to $true
	    If ($RegValuePFRO) {
		    $PendFileRename = $true
	    }

	    ## Determine SCCM 2012 Client Reboot Pending Status
	    ## To avoid nested 'if' statements and unneeded WMI calls to determine if the CCM_ClientUtilities class exist, setting EA = 0
	    $CCMClientSDK = $null
	    $CCMSplat = @{
	        NameSpace='ROOT\ccm\ClientSDK'
	        Class='CCM_ClientUtilities'
	        Name='DetermineIfRebootPending'
	        ComputerName=$Computer
	        ErrorAction='Stop'
	    }
	    ## Try CCMClientSDK
	    Try {
	        $CCMClientSDK = Invoke-WmiMethod @CCMSplat
	    } Catch [System.UnauthorizedAccessException] {
	        $CcmStatus = Get-Service -Name CcmExec -ComputerName $Computer -ErrorAction SilentlyContinue
	        If ($CcmStatus.Status -ne 'Running') {
	            Write-Warning "$Computer`: Error - CcmExec service is not running."
	            $CCMClientSDK = $null
	        }
	    } Catch {
	        $CCMClientSDK = $null
	    }

	    If ($CCMClientSDK) {
	        If ($CCMClientSDK.ReturnValue -ne 0) {
		        Write-Warning "Error: DetermineIfRebootPending returned error code $($CCMClientSDK.ReturnValue)"          
		    }
		    If ($CCMClientSDK.IsHardRebootPending -or $CCMClientSDK.RebootPending) {
		        $SCCM = $true
		    }
	    }
            
	    Else {
	        $SCCM = $null
	    }

	    ## Creating Custom PSObject and Select-Object Splat
	    $SelectSplat = @{
	        Property=(
	            'Computer',
	            'CBServicing',
	            'WindowsUpdate',
	            'CCMClientSDK',
	            'PendComputerRename',
	            'PendFileRename',
	            'PendFileRenVal',
	            'RebootPending'
	        )}
	    New-Object -TypeName PSObject -Property @{
	        Computer=$WMI_OS.CSName
	        CBServicing=$CBSRebootPend
	        WindowsUpdate=$WUAURebootReq
	        CCMClientSDK=$SCCM
	        PendComputerRename=$CompPendRen
	        PendFileRename=$PendFileRename
	        PendFileRenVal=$RegValuePFRO
	        RebootPending=($CompPendRen -or $CBSRebootPend -or $WUAURebootReq -or $SCCM -or $PendFileRename)
	    } | Select-Object @SelectSplat

	} Catch {
	    Write-Warning "$Computer`: $_"
	    ## If $ErrorLog, log the file to a user specified location/path
	    If ($ErrorLog) {
	        Out-File -InputObject "$Computer`,$_" -FilePath $ErrorLog -Append
	    }				
	}			
  }## End Foreach ($Computer in $ComputerName)			
}## End Process

End {  }## End End

}## End Function Get-PendingReboot
Function Set-AutoLogon{

    [CmdletBinding()]
    Param(
        
        [Parameter(Mandatory=$True,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$DefaultUsername,

        [Parameter(Mandatory=$True,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$DefaultPassword,

        [Parameter(Mandatory=$False,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [String[]]$AutoLogonCount,

        [Parameter(Mandatory=$False,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [String[]]$Script
                
    )

    Begin
    {
        #Registry path declaration
        $RegPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
        $RegROPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce"
    
    }
    
    Process
    {

        try
        {
            #setting registry values
            Set-ItemProperty $RegPath "AutoAdminLogon" -Value "1" -type String  
            Set-ItemProperty $RegPath "DefaultUsername" -Value "$DefaultUsername" -type String  
            Set-ItemProperty $RegPath "DefaultPassword" -Value "$DefaultPassword" -type String
	    Set-ItemProperty $RegPath -Name "ForceAutoLogon" -Value 1 -Force
		Set-ItemProperty $RegPath -Name "ForceUnlockLogon" -Value 1 -Force
            if($AutoLogonCount)
            {
                
                Set-ItemProperty $RegPath "AutoLogonCount" -Value "$AutoLogonCount" -type DWord
            
            }
            else
            {

                Set-ItemProperty $RegPath "AutoLogonCount" -Value "1" -type DWord

            }
            if($Script)
            {
                
                Set-ItemProperty $RegROPath "(Default)" -Value "$Script" -type String
            
            }
            else
            {
            
                Set-ItemProperty $RegROPath "(Default)" -Value "" -type String
            
            }        
        }

        catch
        {

            Write-Output "An error had occured $Error"
            
        }
    }
    
    End
    {
        
        #End

    }

}
Function Install-RSATTools
{
	[CmdletBinding()]
	Param()
	$VerbosePreference = 'Continue'
	$systeminfo = Get-WmiObject -Namespace Root\CIMV2 -Class Win32_OperatingSystem -Property "BuildNumber"
	If ([int]$systeminfo.Buildnumber -eq 17134)
	{
		$x86 = 'https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_WS_1803-x86.msu'
		$x64 = 'https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_WS_1803-x64.msu'
	}
	elseif ([int]$systeminfo.Buildnumber -eq 16299)
	{
		$x86 = 'https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_WS_1709-x86.msu'
		$x64 = 'https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_WS_1709-x64.msu'
	}
	Else
	{
		$x86 = 'https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_WS2016-x86.msu'
		$x64 = 'https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_WS2016-x64.msu'
	}
	switch ($env:PROCESSOR_ARCHITECTURE)
	{
		'x86' { $version = $x86 }
		'AMD64' { $version = $x64 }
	}
	
	Write-Verbose -Message "OS Version is $env:PROCESSOR_ARCHITECTURE"
	Write-Verbose -Message "Now Downloading RSAT Tools installer"
	
	$Filename = $version.Split('/')[-1]
	Invoke-WebRequest -Uri $version -UseBasicParsing -OutFile "$env:TEMP\$Filename"
	
	Write-Verbose -Message "Starting the Windows Update Service to install the RSAT Tools "
	
	Start-Process -FilePath wusa.exe -ArgumentList "$env:TEMP\$Filename /quiet" -Wait -Verbose
	
	Write-Verbose -Message "RSAT Tools are now be installed"
	
	Remove-Item "$env:TEMP\$Filename" -Verbose
	
	Write-Verbose -Message "Script Cleanup complete"
	
	Write-Verbose -Message "Remote Administration"
}
workflow Download-AllGalleryModules
{
mkdir C:\modules
mkdir C:\scripts


$modules = Find-Module * -IncludeDependencies | Sort-Object Name

foreach -parallel -throttlelimit 25 ($module in $modules) 
    { Save-Module $module.Name -Path C:\modules -Force }

$scripts = Find-Script * -IncludeDependencies | Sort-Object Name

foreach -parallel -throttlelimit 25 ($script in $scripts) 
    { Save-Script $script.Name -Path C:\scripts -Force }

}
function Use-RunAs 
{    
    # Check if script is running as Adminstrator and if not use RunAs 
    # Use Check Switch to check if admin 
     
    param([Switch]$Check) 
     
    $IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()` 
        ).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") 
         
    if ($Check) { return $IsAdmin }     
 
    if ($MyInvocation.ScriptName -ne "") 
    {  
        if (-not $IsAdmin)  
        {  
            try 
            {  
                $arg = "-file `"$($MyInvocation.ScriptName)`"" 
                Start-Process "$psHome\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop'  
            } 
            catch 
            { 
                Write-Warning "Error - Failed to restart script with runas"  
                break               
            } 
            exit # Quit this session of powershell 
        }  
    }  
    else  
    {  
        Write-Warning "Error - Script must be saved as a .ps1 file first"  
        break  
    }  
} 
 function Test-LocalCredential {
    [CmdletBinding()]
    Param
    (
        [string]$UserName,
        [string]$ComputerName,
        [string]$Password
    )
    if (!($UserName) -or !($Password)) {
        Write-Warning 'Test-LocalCredential: Please specify both user name and password'
    } else {
        Add-Type -AssemblyName System.DirectoryServices.AccountManagement
        $DS = New-Object System.DirectoryServices.AccountManagement.PrincipalContext('machine',$ComputerName)
        $DS.ValidateCredentials($UserName, $Password)
    }
}
function get-scriptdir
{
<#
	.SYNOPSIS
		Get-ScriptDirectory returns the proper location of the script.

	.OUTPUTS
		System.String
	
	.NOTES
		Returns the correct path within a packaged executable.
#>
	[OutputType([string])]
	param ()
	if ($null -ne $hostinvocation)
	{
		Split-Path $hostinvocation.MyCommand.path
	}
	else
	{
		Split-Path $script:MyInvocation.MyCommand.Path
	}
}
 
 
 
# Code goes at very bottom

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
    Use-RunAs
    $dir = get-scriptdir
    If (!(Test-Path "$env:APPDATA\hold.txt"))
    {
DISM /Online /Enable-Feature /FeatureName:NetFx3 /All /NoRestart /Quiet
   Dism /online /Enable-feature /FeatureName:Containers /All /NoRestart /Quiet
   Dism /online /Enable-feature /FeatureName:TFTP /All /NoRestart /Quiet
   Dism /online /Enable-feature /FeatureName:TelnetClient /All /NoRestart /Quiet
   Dism /online /Enable-feature /FeatureName:Microsoft-Windows-Subsystem-Linux /All /NoRestart /Quiet
   Get-WindowsOptionalFeature -Online | where-object {$_.FeatureName -like "*Defender*"} | % {Dism /online /Disable-Feature /FeatureName:"$($_.FeatureName)" /NoRestart /Quiet}
   Start-process $env:ComSpec -ArgumentList "/c winrm quickconfig /q" -Wait
   <#
   netsh advfirewall firewall add rule name="Winrm HTTPS" protocol=TCP dir=in localport=5986 action=allow
netsh advfirewall firewall add rule name="Winrm HTTPS2" protocol=UDP dir=in localport=5986 action=allow
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Basic=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Digest=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Kerberos=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Negotiate=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{Certificate=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client/auth @{CredSSP=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client @{TrustedHost=`"*`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/client @{AllowUnencrypted=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{Basic=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{Kerberos=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{Negotiate=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{Certificate=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{CredSSP=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service/auth @{CbtHardeningLevel=`"None`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service @{IPv4Filter=`"*`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service @{EnableCompatibilityHttpListener=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service @{EnableCompatibilityHttpsListener=`"True`"}" -Wait
Start-process $env:ComSpec -ArgumentList "/c winrm set winrm/config/service @{AllowUnencrypted=`"True`"}" -Wait
#>
      Get-PackageProvider -Name NuGet -ForceBootstrap
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
Set-ItemProperty -Path 'hklm:\Software\Microsoft\Windows\CurrentVersion\Run' -name "deploy" -Value '%APPDATA%\go.bat' -type 'ExpandString'
#Download-AllGalleryModules
. { iwr -useb http://boxstarter.org/bootstrapper.ps1 } | iex; get-boxstarter -Force
& "C:\ProgramData\chocolatey\redirects\RefreshEnv.cmd"
echo '@powershell -Executionpolicy bypass -Command "Start-Process powershell -ArgumentList $env:USERPROFILE\Desktop\beg.ps1"' | Out-File -filepath "$env:APPDATA\go.bat" -Encoding ascii
 # . "$env:APPDATA\Set-autologon.ps1"
  #Set-Autologon -DefaultUsername "$env:USERDOMAIN\$env:USERNAME" -DefaultPassword $password -AutoLogonCount "6" -Script "$env:APPDATA\go.bat"
choco upgrade sysinternals -y
& "C:\ProgramData\chocolatey\redirects\RefreshEnv.cmd"
$obj = Get-WmiObject -Class "Win32_TerminalServiceSetting" -Namespace root\cimv2\terminalservices
$obj.SetAllowTsConnections(1,1) | out-null
$obj2 = Get-WmiObject -class Win32_TSGeneralSetting -Namespace root\cimv2\terminalservices -ComputerName . -Filter "TerminalName='RDP-tcp'"
$obj2.SetUserAuthenticationRequired(0) | out-null
Invoke-Webrequest https://gist.githubusercontent.com/thelastofreed/e94596e2a53f22a40c79e7801458f694/raw/3a7ca032634a946843bd5a7f01c15f2b6c3acecb/mywinup.ps1 -outfile $env:APPDATA\boxstarter.ps1
choco install powershell -y
echo 1 | Out-File "$env:APPDATA\hold.txt"
Restart-Computer -Force
}
Else
{
	If ($(Get-Content "$env:APPDATA\hold.txt") -eq 1)
	{
		Remove-Item "$env:APPDATA\hold.txt" -Force
		Invoke-WebRequest "http://download.microsoft.com/download/B/E/6/BE63E3A5-5D1C-43E7-9875-DFA2B301EC70/adk/adksetup.exe" -OutFile "$dir\adksetup.exe"


Start-Process "$dir\adksetup.exe" -ArgumentList "/features + /q" -Wait -WindowStyle Hidden
Do
{
	Start-Sleep 2
}
Until ((Get-Process).Name -notcontains "adksetup.exe")

Invoke-WebRequest "https://download.microsoft.com/download/E/F/A/EFA17CF0-7140-4E92-AC0A-D89366EBD79E/adkwinpeaddons/adkwinpesetup.exe" -OutFile "$dir\adkwinpesetup.exe"
Start-Process "$dir\adkwinpesetup.exe" -ArgumentList "/features + /q" -Wait -WindowStyle Hidden
Do
{
	Start-Sleep 2
}
Until ((Get-Process).Name -notcontains "adkwinpesetup.exe")
Invoke-WebRequest "https://download.microsoft.com/download/3/3/9/339BE62D-B4B8-4956-B58D-73C4685FC492/MicrosoftDeploymentToolkit_x64.msi" -OutFile "$dir\MicrosoftDeploymentToolkit_x64.msi"
Start-Process "msiexec.exe" -ArgumentList "/i `"$dir\MicrosoftDeploymentToolkit_x64.msi`" /qb" -Wait -WindowStyle Hidden
Invoke-webrequest https://github.com/git-for-windows/git/releases/download/v2.20.1.windows.1/Git-2.20.1-64-bit.exe -outfile $env:USERPROFILE\Desktop\git.exe
Start-Process $env:USERPROFILE\Desktop\git.exe -argumentlist "/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-" -Wait
Remove-item $env:USERPROFILE\Desktop\git.exe -force
Invoke-webrequest https://nodejs.org/dist/v11.10.0/node-v11.10.0-x64.msi -outfile $env:USERPROFILE\Desktop\node.msi
Start-process msiexec.exe -Argumentlist "/i `"$env:USERPROFILE\Desktop\node.msi`" /qb" -wait
remove-item $env:USERPROFILE\Desktop\node.msi -force
Invoke-webrequest https://releases.hashicorp.com/vagrant/2.2.3/vagrant_2.2.3_x86_64.msi -outfile $env:USERPROFILE\Desktop\vagrant.msi
Start-process msiexec.exe -Argumentlist "/i `"$env:USERPROFILE\Desktop\vagrant.msi`" /qb" -wait
remove-item $env:USERPROFILE\Desktop\vagrant.msi -force
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PSWindowsUpdate))
{
Try
    {
        Install-module PSWindowsUpdate -Force -ErrorAction Stop
    }
Catch
    {
        Install-module PSWindowsUpdate -Force -AllowClobber
}
}
Else
{
	Update-Module PSWindowsUpdate -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\CustomizeWindows10))
{
Try
    {
        Install-module CustomizeWindows10 -Force -ErrorAction Stop
    }
Catch
    {
        Install-module CustomizeWindows10 -Force -AllowClobber
}
}
Else
{
	Update-Module CustomizeWindows10 -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Autobrowse))
{
Try
{
	Install-Module Autobrowse -Force -ErrorAction Stop
}
Catch
{
	Install-Module Autobrowse -Force -AllowClobber
}
}
Else
{
	Update-Module Autobrowse -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Patchy))
{
Try
{
	Install-Module Patchy -Force -ErrorAction Stop
}
Catch
{
	Install-Module Patchy -Force -AllowClobber
}
}
Else
{
	Update-Module Patchy -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\SecureSettings))
{
Try
{
	Install-Module SecureSettings -Force -ErrorAction Stop
}
Catch
{
	Install-Module SecureSettings -Force -AllowClobber
}
}
Else
{
	Update-Module SecureSettings -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Microsoft.PowerShell.GraphicalTools))
{
Try
{
	Install-Module Microsoft.PowerShell.GraphicalTools -Force -ErrorAction Stop
}
Catch
{
	Install-Module Microsoft.PowerShell.GraphicalTools -Force -AllowClobber
}
}
Else
{
	Update-Module Microsoft.PowerShell.GraphicalTools -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Microsoft.PowerShell.ConsoleGuiTools))
{
Try
{
	Install-Module Microsoft.PowerShell.ConsoleGuiTools -Force -ErrorAction Stop
}
Catch
{
	Install-Module Microsoft.PowerShell.ConsoleGuiTools -Force -AllowClobber
}
}
Else
{
	Update-Module Microsoft.PowerShell.ConsoleGuiTools -force
}

If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Plaster))
{
Try
{
	Install-Module Plaster -Force -ErrorAction Stop
}
Catch
{
	Install-Module Plaster -Force -AllowClobber
}
}
Else
{
	Update-Module Plaster -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\WindowsCompatibility))
{
Try
{
	Install-Module WindowsCompatibility -Force -ErrorAction Stop
}
Catch
{
	Install-Module WindowsCompatibility -Force -AllowClobber
}
}
Else
{
	Update-Module WindowsCompatibility -force
}



If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\AADInternals))
{
Try
{
	Install-Module AADInternals -Force -ErrorAction Stop
}
Catch
{
	Install-Module AADInternals -Force -AllowClobber
}
}
Else
{
	Update-Module AADInternals -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Winformal))
{
Try
{
	Install-Module Winformal -Force -ErrorAction Stop
}
Catch
{
	Install-Module Winformal -Force -AllowClobber
}
}
Else
{
	Update-Module Winformal -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\WinformPS))
{
Try
{
	Install-Module WinformPS -Force -ErrorAction Stop
}
Catch
{
	Install-Module WinformPS -Force -AllowClobber
}
}
Else
{
	Update-Module WinformPS -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PowerShellProTools))
{
Install-module PowerShellProTools -force -allowclobber
}
Else
{
Update-module PowerShellProTools -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Microsoft.Online.SharePoint.PowerShell))
{
Try
{
	Install-Module Microsoft.Online.SharePoint.PowerShell -Force -ErrorAction Stop
}
Catch
{
	Install-Module Microsoft.Online.SharePoint.PowerShell -Force -AllowClobber
}
}
Else
{
	Update-Module Microsoft.Online.SharePoint.PowerShell -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\ShowUI))
{
Try
{
	Install-Module ShowUI -Force -ErrorAction Stop
}
Catch
{
	Install-Module ShowUI -Force -AllowClobber
}
}
Else
{
	Update-Module ShowUI -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\IsePackV2))
{
Try
{
	Install-Module IsePackV2 -Force -ErrorAction Stop
}
Catch
{
	Install-Module IsePackV2 -Force -AllowClobber
}
}
Else
{
	Update-Module IsePackV2 -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\TaskScheduler))
{
Try
{
	Install-Module TaskScheduler -Force -ErrorAction Stop
}
Catch
{
	Install-Module TaskScheduler -Force -AllowClobber
}
}
Else
{
	Update-Module TaskScheduler -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\MenuShell))
{
Try
{
	Install-Module MenuShell -Force -ErrorAction Stop
}
Catch
{
	Install-Module MenuShell -Force -AllowClobber
}
}
Else
{
	Update-Module MenuShell -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\CodeCraft))
{
Try
{
	Install-Module CodeCraft -Force -ErrorAction Stop
}
Catch
{
	Install-Module CodeCraft -Force -AllowClobber
}
}
Else
{
	Update-Module CodeCraft -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PinnedItem))
{
Try
{
	Install-Module PinnedItem -Force -ErrorAction Stop
}
Catch
{
	Install-Module PinnedItem -Force -AllowClobber
}
}
Else
{
	Update-Module PinnedItem -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Az))
{
Try
{
	Install-module Az -Force -ErrorAction Stop
}
Catch
{
	Install-module Az -Force -AllowClobber
}
}
Else
{
	Update-Module Az -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PowerShellGet))
{
Try
{
	Install-module PowerShellGet -Force -ErrorAction Stop
}
Catch
{
	Install-module PowerShellGet -Force -AllowClobber
}
}
Else
{
	Install-Module PowerShellGet -force -allowclobber
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\AzureAD))
{
Install-module AzureAD -AllowClobber -force
}
Else
{
update-module AzureAD -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\MSonline))
{
Install-module MSonline -AllowClobber -force
}
Else
{
update-module MSonline -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\UniversalDashboard.Community))
{
install-module UniversalDashboard.Community -AcceptLicense -Force -AllowClobber
}
Else
{
Update-module UniversalDashboard.Community -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\powershellise-preview))
{
Install-module powershellise-preview -AllowClobber -force
}
Else
{
update-module powershellise-preview -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\psdepend))
{
Install-module psdepend -AllowClobber -force
}
Else
{
update-module psdepend -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\ISEsteroids))
{
Install-module ISEsteroids -AllowClobber -force
}
Else
{
update-module ISEsteroids -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\OutlookConnector))
{
Install-module OutlookConnector -AllowClobber -force
}
Else
{
update-module OutlookConnector -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PoshRSJob))
{
Install-module PoshRSJob -AllowClobber -force
}
Else
{
update-module PoshRSJob -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\SecretServer))
{
Install-module SecretServer -AllowClobber -force
}
Else
{
update-module SecretServer -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\WFTools))
{
Install-module WFTools -AllowClobber -force
}
Else
{
update-module WFTools -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PSSlack))
{
Install-module PSSlack -AllowClobber -force
}
Else
{
update-module PSSlack -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PSDeploy))
{
Install-module PSDeploy -AllowClobber -force
}
Else
{
update-module PSDeploy -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\BuildHelpers))
{
Install-module BuildHelpers -AllowClobber -force
}
Else
{
update-module BuildHelpers -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Exchange_AddIn))
{
Install-Module Exchange_AddIn -AllowClobber -force
}
Else
{
update-module Exchange_AddIn -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Pode))
{
Install-Module Pode -AllowClobber -force
}
Else
{
Update-module Pode -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Azure))
{
Install-Module Azure -AllowClobber -force
}
Else
{
update-module Azure -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\SharePointPnPPowerShellOnline))
{
Install-Module SharePointPnPPowerShellOnline -AllowClobber -force
}
Else
{
update-module SharePointPnPPowerShellOnline
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PSSudo))
{
Install-Module PSSudo -AllowClobber -force
}
Else
{
update-module PSSudo -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\SpeculationControl))
{
Install-Module SpeculationControl -AllowClobber -force
}
Else
{
update-module SpeculationControl -force
}
Install-Module Packagemanagement -AllowClobber -Force
Install-Module powershellget -AllowClobber -Force
Install-script connect-o365 -force -confirm:$false
Show-DesktopIcons
#Enable-ExplorerThisPC
#Enable-Windows7VolumeMixer
#Enable-ShowFileExtension
#Enable-ShowHiddenFiles
choco upgrade lockhunter -y
choco upgrade sudo -y
choco upgrade nodist -y
#choco upgrade capture2text -y
#choco install sqlite -y
choco upgrade duck -y
choco upgrade wget -y
choco upgrade youtube-dl -y
choco upgrade path-copy-copy -y
choco upgrade conemu -y
choco upgrade kubernetes-cli -y
choco upgrade minikube -y
#Invoke-webrequest "http://download.microsoft.com/download/3/1/E/31EC1AAF-3501-4BB4-B61C-8BD8A07B4E8A/adk/adksetup.exe" -outfile "$env:USERPROFILE\Downloads\adksetup.exe"
#Start-process "$env:USERPROFILE\Downloads\adksetup.exe" -Argumentlist '/quiet /installpath "C:\Program Files (x86)\Windows Kits\10" /features +' -wait
#Start-sleep 15
#Do
#{
#    Start-sleep 2
#    $process = Get-process
#}
#Until ($process.ProcessName -notcontains "adksetup")
#choco install slickrun -y
#Install-RSATTools
echo 2 | Out-File "$env:APPDATA\hold.txt"
Restart-Computer -Force
}
Else
{
Remove-Item "$env:APPDATA\hold.txt" -Force

start-process $env:COMSPEC -argumentlist "/c setx DOWNLOAD %USERPROFILE%\Downloads" -Wait
start-process $env:COMSPEC -argumentlist "/c setx DESKTOP %USERPROFILE%\Desktop" -Wait
start-process $env:COMSPEC -argumentlist "/c setx DOCUMENT %USERPROFILE%\Documents" -Wait
cd $env:USERPROFILE\Documents
mkdir git
cd git
git clone https://github.com/p-e-w/maybe
Start-Process "C:\Python27\python.exe" -ArgumentList '-m pip install --upgrade pip' -Wait
Start-Process "C:\Python27\Scripts\pip.exe" -ArgumentList 'install maybe' -Wait
Start-Process "C:\Python27\Scripts\pip.exe" -ArgumentList 'install howdoi' -Wait
Start-Process "C:\Python27\Scripts\pip.exe" -ArgumentList 'install mkdocs' -Wait
npm install -g how2
npm install -g mklicense
npm install ocr-space-api -g
npm install --global pageres-cli
npm install -g wat
npm i -g @pnp/office365-cli
npm install -g bitly-client
npm install --global imgur-uploader-cli
npm install -g localtunnel
npm i wappalyzer -g
npm install --global wallpaper-cli
npm install -g share-cli
npm install -g remote-share-cli
npm install --global trash-cli
npm install --global empty-trash-cli
npm install -g trashss
npm install --global del-cli
npm install --global cpy-cli
npm install --global file-type-cli
npm install -g doctoc
npm i -g cli-github
npm install -g gistup
npm install --global find-versions-cli
npm install --global kill-tabs
npm install hget -g
npm install -g yo
npm install -g grunt
npm install -g grunt-cli
npm install -g nativefier
npm install -g generator-oraclejet

Remove-Item "$env:APPDATA\go.bat" -Force
echo "cd /d %~dp0" 'PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command ".\boxstarter.ps1"' | Out-file -filepath "$env:APPDATA\go.bat" -Encoding ascii
Write-Host 'Restarting Computer soon.  Save Work.'
Start-Sleep 10
#Restart-Computer -force
Start-process "$env:USERPROFILE\Downloads\cinstall.exe" -Argumentlist "/r" -Wait
}
}