      #create a folder to store the box assemblies
If (!(Test-Path "$env:ProgramData\boxlib"))
{
    New-item -Name "boxlib" -Path $env:PROGRAMDATA -ItemType Directory -Force
}
#Check to see if assemblies are already present, and download custom nuget package I created containing the box windows sdk and its
# dependencies using the package management module
If (!(Test-Path "C:\ProgramData\boxlib\Box.Dependencies"))
{
    Get-PackageProvider -Name NuGet -ForceBootstrap
Get-PackageProvider -Name Powershellget -ForceBootstrap
Register-PSRepository -Name "boxdependency" -SourceLocation "https://www.myget.org/F/boxdependency/api/v2" -InstallationPolicy Trusted
save-Package -Name Box.dependencies -path "$env:ProgramData\boxlib" -ProviderName "PowershellGet"
}
Get-childitem "$env:ProgramData\boxlib\*.dll" -Recurse | % {[reflection.assembly]::LoadFrom($_.FullName)}
$cli = Get-Content "C:\Users\wmh02\OneDrive - Acuity Brands, Inc\box-cli.json" | ConvertFrom-Json
$boxconfig = New-Object -TypeName Box.V2.Config.Boxconfig($cli.boxAppSettings.clientID, $cli.boxAppSettings.clientSecret, $cli.enterpriseID, $cli.boxAppSettings.appAuth.privateKey, $cli.boxAppSettings.appAuth.passphrase, $cli.boxAppSettings.appAuth.publicKeyID)
$boxJWT = New-Object -TypeName Box.V2.JWTAuth.BoxJWTAuth($boxconfig)
$boxjwt
$tokenreal = $boxJWT.AdminToken()
$adminclient = $boxjwt.AdminClient($tokenreal)
$adminclient
