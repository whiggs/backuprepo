      Function Set-AutoLogon{

    [CmdletBinding()]
    Param(
        
        [Parameter(Mandatory=$True,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$DefaultUsername,

        [Parameter(Mandatory=$True,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [String[]]$DefaultPassword,

        [Parameter(Mandatory=$False,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [String[]]$AutoLogonCount,

        [Parameter(Mandatory=$False,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [String[]]$Script
                
    )

    Begin
    {
        #Registry path declaration
        $RegPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
        $RegROPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce"
    
    }
    
    Process
    {

        try
        {
            #setting registry values
            Set-ItemProperty $RegPath "AutoAdminLogon" -Value "1" -type String  
            Set-ItemProperty $RegPath "DefaultUsername" -Value "$DefaultUsername" -type String  
            Set-ItemProperty $RegPath "DefaultPassword" -Value "$DefaultPassword" -type String
            if($AutoLogonCount)
            {
                
                Set-ItemProperty $RegPath "AutoLogonCount" -Value "$AutoLogonCount" -type DWord
            
            }
            else
            {

                Set-ItemProperty $RegPath "AutoLogonCount" -Value "1" -type DWord

            }
            if($Script)
            {
                
                Set-ItemProperty $RegROPath "(Default)" -Value "$Script" -type String
            
            }
            else
            {
            
                Set-ItemProperty $RegROPath "(Default)" -Value "" -type String
            
            }        
        }

        catch
        {

            Write-Output "An error had occured $Error"
            
        }
    }
    
    End
    {
        
        #End

    }

}
function Use-RunAs 
{    
    # Check if script is running as Adminstrator and if not use RunAs 
    # Use Check Switch to check if admin 
     
    param([Switch]$Check) 
     
    $IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()` 
        ).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") 
         
    if ($Check) { return $IsAdmin }     
 
    if ($MyInvocation.ScriptName -ne "") 
    {  
        if (-not $IsAdmin)  
        {  
            try 
            {  
                $arg = "-file `"$($MyInvocation.ScriptName)`"" 
                Start-Process "$psHome\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop'  
            } 
            catch 
            { 
                Write-Warning "Error - Failed to restart script with runas"  
                break               
            } 
            exit # Quit this session of powershell 
        }  
    }  
    else  
    {  
        Write-Warning "Error - Script must be saved as a .ps1 file first"  
        break  
    }  
} 
 
# Code goes at very bottom
$user = $env:USERPROFILE
$domain = $env:USERDOMAIN
Use-RunAs
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
$log = $env:APPDATA + '\hold.txt'
If (!(Test-path -LiteralPath $log)) {
    DISM /Online /Enable-Feature /FeatureName:NetFx3 /All
  $text = "1"
  $text | out-file $log
      Get-PackageProvider -Name NuGet -ForceBootstrap
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
    Try
    {
        Install-module Carbon -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Carbon -Force -AllowClobber
    }
   Set-RegistryKeyValue -Path 'hklm:\Software\Microsoft\Windows\CurrentVersion\Run' -Name "deploy" -String '%APPDATA%\go.bat' -Expand
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
C:\ProgramData\chocolatey\helpers\functions\Update-SessionEnvironment.ps1
}
$ret = Get-Content $log
If ($ret -eq "1") {
  Invoke-Webrequest "https://gist.githubusercontent.com/DemonDante/59d851c627fe16ac109f6d00922bb1ab/raw/6d362269a746edd3f527e0dcd96f463e27118e03/set-autologon.ps1" -outfile "$env:APPDATA\set-autologon.ps1"
  echo "cd /d %~dp0" 'PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command ".\boxstarter.ps1"' | Out-file -filepath "$env:APPDATA\go.bat" -Encoding ascii
 $OUTPU= [System.Windows.Forms.MessageBox]::Show("Configure autologin?" , "choose" , 4)
If ($OUTPU -eq "YES") {
Start-Process "\\live.sysinternals.com\tools\Autologon.exe" -Wait
}
choco install boxstarter -y
C:\ProgramData\chocolatey\helpers\functions\Update-SessionEnvironment.ps1
$OUTPUT= [System.Windows.Forms.MessageBox]::Show("Disable UAC?" , "choose" , 4)
If ($OUTPUT -eq "YES") {
    Set-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name EnableLUA  -Value 0
    "IT happened" | Out-file "$env:APPDATA\select.txt"
}
$obj = Get-WmiObject -Class "Win32_TerminalServiceSetting" -Namespace root\cimv2\terminalservices
    $obj.SetAllowTsConnections(1,1) | out-null
    $obj2 = Get-WmiObject -class Win32_TSGeneralSetting -Namespace root\cimv2\terminalservices -ComputerName . -Filter "TerminalName='RDP-tcp'"
    $obj2.SetUserAuthenticationRequired(0) | out-null
del $log
$text = "2"
$text | Out-file $log
choco install powershell -y
Restart-Computer -force
}
Elseif ($ret -eq "2") {
    Try
    {
        Install-module Autoload -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Autoload -Force -AllowClobber
    }
    Try
    {
        Install-module Beaver -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Beaver -Force -AllowClobber
    }
    Try
    {
        Install-module BurntToast -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module BurntToast -Force -AllowClobber
    }
    Try
    {
        Install-module CertificatePS -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module CertificatePS -Force -AllowClobber
    }
    Try
    {
        Install-module cMDT -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module cMDT -Force -AllowClobber
    }
    Try
    {
        Install-module CustomizeWindows10 -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module CustomizeWindows10 -Force -AllowClobber
    }
    Try
    {
        Install-module DeployImage -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module DeployImage -Force -AllowClobber
    }
    Try
    {
        Install-module Dimmo -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Dimmo -Force -AllowClobber
    }
    Try
    {
        Install-module IseHg -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module IseHg -Force -AllowClobber
    }
    Try
    {
        Install-module ISEScriptingGeek -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module ISEScriptingGeek -Force -AllowClobber
    }
    Try
    {
        Install-module Kickstart -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Kickstart -Force -AllowClobber
    }
    Try
    {
        Install-module MarkdownPS -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module MarkdownPS -Force -AllowClobber
    }
    Try
    {
        Install-module OneDrive -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module OneDrive -Force -AllowClobber
    }
    Try
    {
        Install-module Pester -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Pester -Force -AllowClobber
    }
    Try
    {
        Install-module platyPS -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module platyPS -Force -AllowClobber
    }
    Try
    {
        Install-module PowerShellCookbook -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PowerShellCookbook -Force -AllowClobber
    }
    Try
    {
        Install-module PowerShellGet -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PowerShellGet -Force -AllowClobber
    }
    Try
    {
        Install-module PowerShellGetGUI -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PowerShellGetGUI -Force -AllowClobber
    }
    Try
    {
        Install-module PowerShellISE-preview -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PowerShellISE-preview -Force -AllowClobber
    }
    Try
    {
        Install-module Proxx.Toast -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Proxx.Toast -Force -AllowClobber
    }
    Try
    {
        Install-module PSBookmark -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSBookmark -Force -AllowClobber
    }
    Try
    {
        Install-module PSConfig -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSConfig -Force -AllowClobber
    }
    Try
    {
        Install-module Pscx -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Pscx -Force -AllowClobber
    }
    Try
    {
        Install-module PSDeploy -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSDeploy -Force -AllowClobber
    }
    Try
    {
        Install-module PSGist -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSGist -Force -AllowClobber
    }
    Try
    {
        Install-module PsGoogle -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PsGoogle -Force -AllowClobber
    }
    Try
    {
        Install-module PsISEProjectExplorer -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PsISEProjectExplorer -Force -AllowClobber
    }
    Try
    {
        Install-module PSReadline -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSReadline -Force -AllowClobber
    }
    Try
    {
        Install-module PSScriptAnalyzer -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSScriptAnalyzer -Force -AllowClobber
    }
    Try
    {
        Install-module PSSudo -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSSudo -Force -AllowClobber
    }
    Try
    {
        Install-module PSWindowsUpdate -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSWindowsUpdate -Force -AllowClobber
    }
    Try
    {
        Install-module PSWord -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module PSWord -Force -AllowClobber
    }
    Try
    {
        Install-module ISESteroids -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module ISESteroids -Force -AllowClobber
    }
    Try
    {
        Install-module SendEmail -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module SendEmail -Force -AllowClobber
    }
    Try
    {
        Install-module ShowUI -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module ShowUI -Force -AllowClobber
    }
    Try
    {
        Install-module WakeOnLan -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module WakeOnLan -Force -AllowClobber
    }
    Try
    {
        Install-module WindowsImageTools -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module WindowsImageTools -Force -AllowClobber
    }
    Try
    {
        Install-module Winformsps -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module Winformsps -Force -AllowClobber
    }
    Try
    {
        Install-module ARTools -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module ARTools -Force -AllowClobber
    }
    Try
    {
        Install-module AU -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module AU -Force -AllowClobber
    }
    Try
    {
        Install-module poshinternals -Force -ErrorAction Stop
    }
    Catch
    {
        Install-module poshinternals -Force -AllowClobber
    }
Install-Script -Name MessageCenter
Install-Script -Name ADLockoutViewer
Install-Script -Name Test-Credential
Install-Script -Name Update-Windows
Install-Script -Name Install-WinrmHttps
Install-Script -Name POSH-DiskHealth 
Install-Script -Name Get-ModuleConflict
Install-Script -Name Connect-Mstsc
Install-Script -Name Get-Parameter
Install-Script -Name New-PSSchtask
Enable-ExplorerThisPC
Enable-Windows7VolumeMixer
#Add-WUServiceManager -ServiceID "7971f918-a847-4430-9279-4a52d1efe18d" -Confirm:$false
del $log
$text = "3"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "3") {
  Get-WUInstall -autoreboot -acceptall
  Start-Sleep 5
  choco install boxstarter -y
  del $log
$text = "4"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "4") {
Invoke-WebRequest "https://www.autoitscript.com/autoit3/scite/download/SciTE4AutoIt3.exe" -OutFile "$env:USERPROFILE\Downloads\scite.exe"
Start-Process -FilePath "$env:USERPROFILE\Downloads\scite.exe" -ArgumentList "/S" -Wait
choco install flashplayerplugin -y
choco install javaruntime -y
choco install sysinternals -y
choco install lockhunter -y
choco install sudo -y
choco install conemu -y
choco install atom -y
 del $log
$text = "6"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "6") {
    Invoke-Webrequest "https://gist.githubusercontent.com/whiggs/6fdd9962040ddfea6c095d5feb2450e8/raw/54b33bb9a6436c4ddc456d8699271d5a5c64a7cc/uvk.au3" -outfile "$env:APPDATA\uvk.au3"
    Start-Process -Filepath "${env:ProgramFiles(x86)}\AutoIt3\Aut2Exe\Aut2exe.exe" -ArgumentList "/in $env:APPDATA\uvk.au3" -Wait
    Start-Process -FilePath "$env:APPDATA\uvk.exe" -Wait
    Start-Sleep 5
del $log
$text = "7"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "7") {
    Invoke-WebRequest "https://download.microsoft.com/download/1/D/8/1D8B5022-5477-4B9A-8104-6A71FF9D98AB/WindowsTH-RSAT_TP5_Update-x64.msu" -OutFile "$env:APPDATA\RSAT.msu"
$item = $env:APPDATA + "\RSAT.msu"
del $log
$text = "8"
$text | Out-file $log
wusa $item /quiet /forcerestart
}
Elseif ($ret -eq "8") {
    $another = "http://www.carifred.com/tech_tool_store/TechToolStore.exe"
$outward = $env:USERPROFILE + '\Downloads\toolstore.exe'
Invoke-WebRequest $another -OutFile $outward
Start-Process -FilePath $outward -Wait
 del $log
$text = "9"
$text | Out-file $log
Restart-Computer -force
}
Elseif ($ret -eq "9") {
    Start-Process '\\WILLSERVER\toshiba\temp\PathCopyCopy12.0.exe' -ArgumentList '/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-' -Wait
    Start-Process "$env:USERPROFILE\Downloads\wpilauncher.exe" -Wait
    Start-process '\\WILLSERVER\Newdisk\test\MICROSOFT Office PRO Plus 2016 v16.0.4266.1003 RTM + Activator [TechTools.NET]\office\setup64.exe' -Wait
     del $log
$text = "done"
$text | Out-file $log
Restart-Computer -force
}
Else {
    Add-Type -AssemblyName System.Windows.Forms

$mess = [System.Windows.Forms.MessageBox]::Show('Installation complete')
If (Test-Path -Path "$env:APPDATA\select.txt") {Set-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System -Name EnableLUA  -Value 1}
Remove-RegistryKeyValue -Path 'hklm:\Software\Microsoft\Windows\CurrentVersion\Run' -Name "deploy"
}

