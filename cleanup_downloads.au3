; *** Start added by AutoIt3Wrapper ***
#include <FileConstants.au3>
; *** End added by AutoIt3Wrapper ***
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Res_SaveSource=y
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Add_Constants=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

	AutoIt Version: 3.3.15.0 (Beta)
	Author:         myName

	Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <File.au3>
#include <Date.au3>
#include <Misc.au3>
#include <Array.au3>
#include <Constants.au3>
#include <GUIConstantsEx.au3>
#include <ListViewConstants.au3>
#include <WindowsConstants.au3>
#include <ButtonConstants.au3>
#include <GuiListView.au3>
#include <GuiListViewex.au3>
#include <WinAPIConv.au3>
Local $hDLL = DllOpen("user32.dll")
Global $vData, $sMsg, $aLV_List_Left, $aRet, $iEditMode = 0
ProgressOn("Gathering file list", "Please Wait", "Please Wait", Default, Default, 18)
$files = _FileListToArrayRec(@UserProfileDir & "\Downloads", "*(?)*", $FLTAR_FILES)

If @error Then
	SetError(0)
	ProgressOff()
	DllClose($hDLL)
	MsgBox($MB_OK + $MB_ICONHAND, "No duplicate files", 'There were no files in your downloads folder which appear to be duplicates (determined by the presence of "(n)" at the end of the file name, where "n" is a digit).  Try running this tool again when there are duplicate files in your downloads folder.')
	Exit
Else
	_ArrayDelete($files, 0)
	Global $arr[UBound($files)][3]
	For $i = 0 To UBound($files) - 1 Step 1
		$arr[$i][0] = $files[$i]
		$arr[$i][1] = FileGetSize(@UserProfileDir & "\Downloads\" & $files[$i])
		$oldsize = FileGetSize(@UserProfileDir & "\Downloads\" & $files[$i])
		$ext = _GetFilenameExt(@UserProfileDir & "\Downloads\" & $files[$i])
		ConsoleWrite(_GetFilename(@UserProfileDir & "\Downloads\" & $files[$i]) & "." & $ext & @CRLF)
		$spli1 = StringSplit($files[$i], "(")
		If $spli1[0] > 2 Then
			SetError(3)
		EndIf
		If @error Then
			DllClose($hDLL)
			MsgBox($MB_OK + $MB_ICONHAND, "error", "There is a file with multiple sets of parentheses." & @CRLF & $files[$i])
			ShellExecute ( "explorer.exe", @UserProfileDir & "\Downloads", @SystemDir )
			Sleep ( 4000 )
			Send ( $files[$i], 0 )
			Exit
		EndIf
		;$spli1 = StringSplit ( _GetFilename(@UserProfileDir & "\Downloads\" & $files[$i]) & "." & $ext, "(" )
		$spli2 = StringSplit($spli1[2], ")")
		$newname = StringStripWS($spli1[1], 3) & StringStripWS($spli2[2], 3)
		$sea = _ArraySearch($arr, $newname, 0, 0, 0, 0, 1, 1)
		If @error Then
			SetError(0)
			If FileExists(@UserProfileDir & "\Downloads\" & $newname) Then
				$newsize = FileGetSize(@UserProfileDir & "\Downloads\" & $newname)
				$diff = Null
				If $oldsize > $newsize Then
					$diff = $oldsize - $newsize
				Else
					$diff = $newsize - $oldsize
				EndIf
				$text = Null
				Switch $diff
					Case 0
						$text = "0"
					Case 1 To 1023
						$text = $diff & " bytes"
					Case 1024 To 1048575
						$divide = $diff / 1024
						$text = $divide & " Kilobytes"
					Case Else
						$divide = $diff / 1048576
						$text = $divide & " megabytes"
				EndSwitch
				$arr[$i][2] = $text

				_ArrayAdd($arr, $newname & "|" & FileGetSize(@UserProfileDir & "\Downloads\" & $newname) & "|" & $text)
			Else
				ContinueLoop
			EndIf
		Else
			ContinueLoop
		EndIf
	Next

	;$arr = _ArrayUnique ( $arr )
	_ArraySort($arr)
	ProgressOff()
	#cs
		$string = ""
		$del = 0
		For $i = 0 To UBound ( $files ) - 1 Step 1

		$ext = _GetFilenameExt ( @UserProfileDir & "\Downloads\" & $files[$i] )
		$spli1 = StringSplit ( _GetFilename(@UserProfileDir & "\Downloads\" & $files[$i]) & "." & $ext, "(" )
		$spli2 = StringSplit ( $spli1[2], ")" )
		$newname = StringStripWS ( $spli1[1], 3 ) & StringStripWS ( $spli2[2], 3 ) & "." & $ext
		If FileExists ( @UserProfileDir & "\Downloads\" & $newname ) Then
		$string = $string & $newname & ";"
		EndIf

		Next

		_ArrayAdd($files, StringTrimRight($string, 1), Default, ";")
		$files = _ArrayUnique($files)
		_ArrayDelete ( $files, 0 )
		_ArraySort($files)

		Local $new[UBound ( $files )][5]
		For $i = 0 To UBound ( $files ) - 1 Step 1
		$ext = _GetFilenameExt ( @UserProfileDir & "\Downloads\" & $files[$i] )
		$split = StringSplit ( $files[$i], "(" )
		If @error Then
		SetError ( 0 )
		$new[$i][0] = $files[$i]
		$new[$i][1] = "0"
		Else
		$new[$i][0] = StringStripWS ( $split[1], 3 ) & "." & $ext
		$new[$i][1] = StringLeft ( $split[2], 1 )
		EndIf
		$new[$i][1] = FileGetSize ( @UserProfileDir & "\Downloads\" & $files[$i] )
		Next
	#ce
	$Form1 = GUICreate("Duplicate Downloads", 609, 407, 190, 141)
	$ListView1 = GUICtrlCreateListView("File Name|Size|Size Difference", 26, 32, 561, 329, BitOR($LVS_REPORT, $LVS_SHOWSELALWAYS), BitOR($WS_EX_CLIENTEDGE, $LVS_EX_GRIDLINES, $LVS_EX_FULLROWSELECT))
	GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 0, 250)
	GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 1, 145)
	GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 2, 145)
	_GUICtrlListView_AddArray($ListView1, $arr)

	;Global $listarr = _GUIListViewEx_ReadToArray($ListView1)
	$iLV_Left_Index = _GUIListViewEx_Init($ListView1) ;, $listarr )
	_GUIListViewEx_SetEditStatus($iLV_Left_Index, 0)
	$ListView1context = GUICtrlCreateContextMenu($ListView1)
	$MenuItem1 = GUICtrlCreateMenuItem("Open/launch file(s)", $ListView1context)
	$MenuItem2 = GUICtrlCreateMenuItem("Open in explorer", $ListView1context)
	$MenuItem3 = GUICtrlCreateMenuItem("Delete item(s)", $ListView1context)
	$Button1 = GUICtrlCreateButton("Process", 260, 368, 89, 33)
	GUICtrlSetCursor(-1, 0)


	;GUIRegisterMsg($WM_COMMAND, "itemselect")
	GUISetState()


	#cs
		Local $aHdrData[][] = [["Col 0", "Col 1", "Col 2"], _
		["", "", ""], _
		["", "", ""]]
		_GUIListViewEx_LoadHdrData($iLV_Left_Index, $aHdrData)
	#ce

	_GUIListViewEx_MsgRegister()

	Local $nMsg
	While 1
		If _IsPressed("2E", $hDLL) Then
			While _IsPressed("2E", $hDLL)
				Sleep(50)
			WEnd
			$stri = "Are you sure you want to delete the following files?" & @CRLF
			$in2 = _GUICtrlListView_GetSelectedIndices($ListView1, True)
			Local $filearray[UBound($in2) - 1]
			For $ty = 1 To $in2[0] Step 1
				$filearray[$ty - 1] = _GUICtrlListView_GetItemText($ListView1, $in2[$ty])
				$stri = $stri & _GUICtrlListView_GetItemText($ListView1, $in2[$ty]) & @CRLF
			Next
			If Not IsDeclared("iMsgBoxAnswer") Then Local $iMsgBoxAnswer
			$iMsgBoxAnswer = MsgBox($MB_YESNO + $MB_DEFBUTTON2 + $MB_ICONQUESTION, "Confirm", $stri)
			If $iMsgBoxAnswer = $IDYES Then
				_GUICtrlListView_BeginUpdate($ListView1)
				$filearray = _ArrayUnique($filearray)
				For $ty = 0 To UBound($filearray) - 1 Step 1
					Do
						FileDelete(@UserProfileDir & "\Downloads\" & $filearray[$ty])
					Until Not FileExists(@UserProfileDir & "\Downloads\" & $filearray[$ty])
					$temp = ""
					For $gg = 0 To UBound($arr) - 1 Step 1
						If $arr[$gg][0] = $filearray[$ty] Then
							$temp = $temp & $gg & ";"
						Else
							ContinueLoop
						EndIf
					Next
					_ArrayDelete($arr, StringTrimRight($temp, 1))
				Next


				_GUICtrlListView_DeleteAllItems($ListView1)
				_GUICtrlListView_AddArray($ListView1, $arr)
				_GUICtrlListView_EndUpdate($ListView1)
			EndIf
		EndIf



		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $GUI_EVENT_PRIMARYDOWN
				ToolTip("")
			Case $GUI_EVENT_CLOSE
				DllClose($hDLL)
				Exit


			Case $MenuItem1
				$index = _GUICtrlListView_GetSelectedIndices($ListView1, True)
				For $i = 1 To $index[0] Step 1
					$filename = _GUICtrlListView_GetItemText($ListView1, $index[$i])
					If StringRight($filename, 3) = "exe" Then
						ShellExecute("Start.exe", '/wait "' & @UserProfileDir & '\Downloads\' & $filename & '"', "C:\Program Files\Sandboxie")
					ElseIf StringRight($filename, 3) = "msi" Then
						ShellExecute("Start.exe", '/wait msiexec.exe /i "' & @UserProfileDir & '\Downloads\' & $filename & '"', "C:\Program Files\Sandboxie")
					Else
						ShellExecute($filename, "", @UserProfileDir & "\Downloads")
					EndIf
				Next

			Case $MenuItem2
				If _GUICtrlListView_GetSelectedCount($ListView1) > 1 Then
					$pos = MouseGetPos()
					If Not IsDeclared("sToolTipAnswer") Then Local $sToolTipAnswer
					$sToolTipAnswer = ToolTip("Only one item can be selected when using this function.", $pos[0], $pos[1], "error", 3, 1)
				Else



					If WinExists("Downloads", "Downloads") Then
						WinActivate("Downloads", "Downloads")
						WinWaitActive("Downloads", "Downloads")
					Else
						Run("explorer " & @UserProfileDir & "\Downloads")
						$win = WinWait("Downloads", "Downloads")
						WinActive($win)
						WinWaitActive($win)
					EndIf
					Sleep(100)
					ControlFocus("Downloads", "Downloads", "[CLASS:DirectUIHWND; INSTANCE:3]")
					Sleep(200)
					$tempsplit = StringSplit(_GUICtrlListView_GetItemText($ListView1, _GUICtrlListView_GetSelectedIndices($ListView1)), "")
					For $y = 1 To $tempsplit[0] Step 1
						If $tempsplit[$y] = "!" Then
							Send("{!}", 0)
						ElseIf $tempsplit[$y] = "^" Then
							Send("{^}", 0)
						ElseIf $tempsplit[$y] = "+" Then
							Send("{+}", 0)
						ElseIf $tempsplit[$y] = "#" Then
							Send("{#}", 0)
						Else
							Send($tempsplit[$y], 0)
						EndIf
					Next
				EndIf

			Case $MenuItem3
				$stri = "Are you sure you want to delete the following files?" & @CRLF
				$in2 = _GUICtrlListView_GetSelectedIndices($ListView1, True)
				Local $filearray[UBound($in2) - 1]
				For $ty = 1 To $in2[0] Step 1
					$filearray[$ty - 1] = _GUICtrlListView_GetItemText($ListView1, $in2[$ty])
					$stri = $stri & _GUICtrlListView_GetItemText($ListView1, $in2[$ty]) & @CRLF
				Next
				If Not IsDeclared("iMsgBoxAnswer") Then Local $iMsgBoxAnswer
				$iMsgBoxAnswer = MsgBox($MB_YESNO + $MB_DEFBUTTON2 + $MB_ICONQUESTION, "Confirm", $stri)
				If $iMsgBoxAnswer = $IDYES Then
					_GUICtrlListView_BeginUpdate($ListView1)
					$filearray = _ArrayUnique($filearray)
					For $ty = 0 To UBound($filearray) - 1 Step 1
						Do
							FileDelete(@UserProfileDir & "\Downloads\" & $filearray[$ty])
						Until Not FileExists(@UserProfileDir & "\Downloads\" & $filearray[$ty])
						$temp = ""
						For $gg = 0 To UBound($arr) - 1 Step 1
							If $arr[$gg][0] = $filearray[$ty] Then
								$temp = $temp & $gg & ";"
							Else
								ContinueLoop
							EndIf
						Next
						_ArrayDelete($arr, StringTrimRight($temp, 1))
					Next


					_GUICtrlListView_DeleteAllItems($ListView1)
					_GUICtrlListView_AddArray($ListView1, $arr)
					_GUICtrlListView_EndUpdate($ListView1)
				EndIf
			Case $Button1
				$st = 0
				GUISetState(@SW_HIDE, $Form1)
				ProgressOn("Gathering file list", "Please Wait", "Please Wait", Default, Default, 18)
				For $i = 0 To UBound($arr) - 1 Step 1
					ProgressSet(($i / UBound($arr)) * 100, _GetFilename(@UserProfileDir & "\Downloads\" & $arr[$i][0]), "Moving Files")
					$ext = _GetFilenameExt(@UserProfileDir & "\Downloads\" & $arr[$i][0])
					$pa = StringInStr($arr[$i][0], "(")
					If $pa = 0 Then
						ContinueLoop
					Else
						$spli1 = StringSplit(_GetFilename(@UserProfileDir & "\Downloads\" & $arr[$i][0]) & "." & $ext, "(")
						If @error Then
							SetError(0)
							ContinueLoop
						Else
							$spli2 = StringSplit($spli1[2], ")")
							$newname = StringStripWS($spli1[1], 3) & StringStripWS($spli2[2], 3)
						EndIf
						If $arr[$i][0] = $newname Then
							ContinueLoop
						EndIf

						If Not FileExists(@UserProfileDir & "\Downloads\" & $newname) Then
							Do
								FileMove(@UserProfileDir & "\Downloads\" & $arr[$i][0], @UserProfileDir & "\Downloads\" & $newname)
							Until FileExists(@UserProfileDir & "\Downloads\" & $newname)

						Else
							$dup = FileGetTime(@UserProfileDir & "\Downloads\" & $arr[$i][0])
							$one = $dup[0] & "/" & $dup[1] & "/" & $dup[2] & " " & $dup[3] & ":" & $dup[4] & ":" & $dup[5]
							$ol = FileGetTime(@UserProfileDir & "\Downloads\" & $newname)
							$two = $ol[0] & "/" & $ol[1] & "/" & $ol[2] & " " & $ol[3] & ":" & $ol[4] & ":" & $ol[5]
							$comp1 = _DateDiff("n", $one, _NowCalc())
							$comp2 = _DateDiff("n", $two, _NowCalc())
							If $comp1 > $comp2 Then

								Do
									FileDelete(@UserProfileDir & "\Downloads\" & $arr[$i][0])
								Until Not FileExists(@UserProfileDir & "\Downloads\" & $arr[$i][0])

								;MsgBox ( 1, "", $arr[$i][0] & " is older than " & $newname)
							Else
								FileMove(@UserProfileDir & "\Downloads\" & $arr[$i][0], @UserProfileDir & "\Downloads\" & $newname, 9)
								;MsgBox ( 1, "", $newname & " is older than " & $arr[$i][0])
							EndIf
						EndIf
						#cs
							$min = 0
							If StringIsSpace(StringMid($files[$i], Int($pa) - 1, 1)) Then
							$min = 2
							Else
							$min = 1
							EndIf

							If Not FileExists(@UserProfileDir & "\Downloads\" & StringMid($files[$i], 1, Int($pa) - $min) & "." & $ext) Then
							FileMove(@UserProfileDir & "\Downloads\" & $files[$i], @UserProfileDir & "\Downloads\" & StringMid($files[$i], 1, Int($pa) - $min) & "." & $ext)
							$fin[$st] = StringMid($files[$i], 1, Int($pa) - $min) & "." & $ext
							$st += 1
							Else
							$dup = _Date_Time_GetFileTime(@UserProfileDir & "\Downloads\" & $files[$i])
							$ol = _Date_Time_GetFileTime(@UserProfileDir & "\Downloads\" & StringMid($files[$i], 1, Int($pa) - $min) & "." & $ext)
							$comp = _Date_Time_CompareFileTime($dup[2], $ol[2])
							If $comp = -1 Then
							Do
							FileDelete(@UserProfileDir & "\Downloads\" & $files[$i])
							Until Not FileExists(@UserProfileDir & "\Downloads\" & $files[$i])
							$fin[$st] = StringMid($files[$i], 1, Int($pa) - $min) & "." & $ext
							$st += 1
							Else
							FileMove(@UserProfileDir & "\Downloads\" & $files[$i], @UserProfileDir & "\Downloads\" & StringMid($files[$i], 1, Int($pa) - $min) & "." & $ext, 9)
							$fin[$st] = StringMid($files[$i], 1, Int($pa) - $min) & "." & $ext
							$st += 1
							EndIf
							EndIf
						#ce
					EndIf
				Next
				DllClose($hDLL)
				Exit

		EndSwitch
		$vRet = _GUIListViewEx_EventMonitor()
		If @error Then
			MsgBox($MB_SYSTEMMODAL, "Error", "Event error: " & @error)
		EndIf
		Switch @extended
			Case 1
				If $vRet <> "" Then
					$before = $vRet[1][2]
					$after = $vRet[1][3]
					If FileExists(@UserProfileDir & "\Downloads\" & $after) Then
						If Not IsDeclared("iMsgBoxAnswer") Then Local $iMsgBoxAnswer
						$iMsgBoxAnswer = MsgBox($MB_YESNO + $MB_ICONQUESTION, "Continue?", "The file name you have selected already exists.  Do you want to overwrite this file?")
						If $iMsgBoxAnswer = $IDYES Then
							#cs
								$str = ""
								For $n = 0 To UBound ( $arr ) - 1 Step 1
								$exention = _GetFilenameExt ( @UserProfileDir & "\Downloads\"
								If $arr[$n][0] = $after Then
								$str = $str & $n & ";"
								EndIf
								Next
								$str = $str & $vRet[1][0]
							#ce
							;$split2 = StringSplit ( $str, ";", $STR_NOCOUNT )
							Local $holarr[UBound($arr) - 1][3]
							;$sub = 0
							For $n = 0 To UBound($arr) - 1 Step 1
								If $n < $vRet[1][0] Then
									;If $arr[$n][0] = $after Then

									$holarr[$n][0] = $arr[$n][0]
									$holarr[$n][1] = $arr[$n][1]
									$holarr[$n][2] = $arr[$n][2]
								ElseIf $n > $vRet[1][0] Then
									$holarr[$n - 1][0] = $arr[$n][0]
									$holarr[$n - 1][1] = $arr[$n][1]
									$holarr[$n - 1][2] = $arr[$n][2]
								Else
									ContinueLoop
								EndIf
							Next
							$arr = $holarr
							_GUICtrlListView_BeginUpdate($ListView1)
							_GUICtrlListView_DeleteAllItems($ListView1)
							_GUICtrlListView_AddArray($ListView1, $arr)
							_GUICtrlListView_EndUpdate($ListView1)
							If FileExists(@UserProfileDir & "\Downloads\" & $before) Then
								Do
									FileMove(@UserProfileDir & "\Downloads\" & $before, @UserProfileDir & "\Downloads\" & $after, $FC_OVERWRITE)
								Until FileExists(@UserProfileDir & "\Downloads\" & $after) And Not FileExists(@UserProfileDir & "\Downloads\" & $before)
							EndIf
						EndIf
					Else
						Local $holarr[UBound($arr) - 1][3]
						;$sub = 0
						For $n = 0 To UBound($arr) - 1 Step 1
							If $n < $vRet[1][0] Then
								;If $arr[$n][0] = $after Then

								$holarr[$n][0] = $arr[$n][0]
								$holarr[$n][1] = $arr[$n][1]
								$holarr[$n][2] = $arr[$n][2]
							ElseIf $n > $vRet[1][0] Then
								$holarr[$n - 1][0] = $arr[$n][0]
								$holarr[$n - 1][1] = $arr[$n][1]
								$holarr[$n - 1][2] = $arr[$n][2]
							Else
								ContinueLoop
							EndIf
						Next
						$arr = $holarr
						_GUICtrlListView_BeginUpdate($ListView1)
						_GUICtrlListView_DeleteAllItems($ListView1)
						_GUICtrlListView_AddArray($ListView1, $arr)
						_GUICtrlListView_EndUpdate($ListView1)
						If FileExists(@UserProfileDir & "\Downloads\" & $before) Then
							Do
								FileMove(@UserProfileDir & "\Downloads\" & $before, @UserProfileDir & "\Downloads\" & $after, $FC_OVERWRITE)
							Until FileExists(@UserProfileDir & "\Downloads\" & $after) And Not FileExists(@UserProfileDir & "\Downloads\" & $before)
						EndIf
					EndIf


				EndIf


		EndSwitch

	WEnd




EndIf
Func _GetFilename($sFilePath)
	Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
	Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
	If IsObj($oColFiles) Then
		For $oObjectFile In $oColFiles
			Return $oObjectFile.FileName
		Next
	EndIf
	Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilename

Func _GetFilenameExt($sFilePath)
	Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
	Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
	If IsObj($oColFiles) Then
		For $oObjectFile In $oColFiles
			Return $oObjectFile.Extension
		Next
	EndIf
	Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenameExt

Func _GetFilenameInt($sFilePath)
	Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
	Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
	If IsObj($oColFiles) Then
		For $oObjectFile In $oColFiles
			Return $oObjectFile.Name
		Next
	EndIf
	Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenameInt

Func _GetFilenameDrive($sFilePath)
	Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
	Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
	If IsObj($oColFiles) Then
		For $oObjectFile In $oColFiles
			Return StringUpper($oObjectFile.Drive)
		Next
	EndIf
	Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenameDrive

Func _GetFilenamePath($sFilePath)
	Local $oWMIService = ObjGet("winmgmts:{impersonationLevel = impersonate}!\\" & "." & "\root\cimv2")
	Local $oColFiles = $oWMIService.ExecQuery("Select * From CIM_Datafile Where Name = '" & StringReplace($sFilePath, "\", "\\") & "'")
	If IsObj($oColFiles) Then
		For $oObjectFile In $oColFiles
			Return $oObjectFile.Path
		Next
	EndIf
	Return SetError(1, 1, 0)
EndFunc   ;==>_GetFilenamePath
#cs
	Func itemselect($hWnd, $nMsg, $wParam, $lParam)
	$nNotifyCode = _WinAPI_HiWord ($wParam)
	$nID = _WinAPI_LoWord($wParam)
	$hCtrl = $lParam
	Select
	Case $nID = $ListView1
	Switch $nNotifyCode
	Case $LVN_ITEMACTIVATE
	If _GUICtrlListView_GetSelectedCount($ListView1) > 1 Then
	GUICtrlSetState($MenuItem2, $GUI_DISABLE)
	Else
	GUICtrlSetState($MenuItem2, $GUI_ENABLE)
	EndIf
	EndSwitch
	EndSelect
	EndFunc   ;==>itemselect
#ce

