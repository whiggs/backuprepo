      @ECHO off
title backup bookmarks on Winodows Vista/7

REM  Recommended to run during user logoff in GPO (User Configuration\Policies\Windows Settings\scripts)

REM Path for the user's network file share
SET userprivatedrivepath=%HOMESHARE%\

REM Path for bookmark backup folder in user's network file share
SET userbookmarkspath=%userprivatedrivepath%bookmark_backups

REM bookmark locations on user's computer
SET bookmark_chrome=%USERPROFILE%\AppData\Local\Google\Chrome\User Data\Default
SET bookmark_ie=%USERPROFILE%\favorites


REM create hidden folder to backup bookmarks to
IF NOT EXIST "%userbookmarkspath%" (
	mkdir "%userbookmarkspath%"
	attrib +h "%userbookmarkspath%"
)

REM bookmark backup path for the computer the user logged on to
SET userbookmarkspathcomputer=%userbookmarkspath%\%computername%

REM create folder to backup a computer's bookmarks to
IF NOT EXIST "%userbookmarkspathcomputer%" (
	mkdir "%userbookmarkspathcomputer%"
)


REM backup bookmarks
ROBOCOPY "%bookmark_chrome%" "%userbookmarkspathcomputer%\chrome" Bookmarks /COPY:DAT /DCOPY:T /R:1 /W:30 /NP
ROBOCOPY "%bookmark_ie%" "%userbookmarkspathcomputer%\ie" /E /MIR /COPY:DAT /DCOPY:T /R:1 /W:30 /NP
