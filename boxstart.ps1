      Function Show-DesktopIcons{
$ErrorActionPreference = "SilentlyContinue"
If ($Error) {$Error.Clear()}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced"
If (Test-Path $RegistryPath) {
	$Res = Get-ItemProperty -Path $RegistryPath -Name "HideIcons"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "HideIcons" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "HideIcons").HideIcons
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "HideIcons" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons"
If (-Not(Test-Path $RegistryPath)) {
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer" -Name "HideDesktopIcons" -Force | Out-Null
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons" -Name "NewStartPanel" -Force | Out-Null
}
$RegistryPath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel"
If (-Not(Test-Path $RegistryPath)) {
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons" -Name "NewStartPanel" -Force | Out-Null
}
If (Test-Path $RegistryPath) {
	## -- My Computer
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}")."{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Control Panel
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}")."{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- User's Files
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}")."{59031a47-3f72-44a7-89c5-5595fe6b30ee}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{59031a47-3f72-44a7-89c5-5595fe6b30ee}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Recycle Bin
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}")."{645FF040-5081-101B-9F08-00AA002F954E}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{645FF040-5081-101B-9F08-00AA002F954E}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	## -- Network
	$Res = Get-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}"
	If (-Not($Res)) {
		New-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
	$Check = (Get-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}")."{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}"
	If ($Check -NE 0) {
		New-ItemProperty -Path $RegistryPath -Name "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}" -Value "0" -PropertyType DWORD -Force | Out-Null
	}
}
If ($Error) {$Error.Clear()}
}

$Boxstarter.RebootOk=$true # Allow reboots
$Boxstarter.NoPassword=$false # machine has login password
$Boxstarter.AutoLogin=$true # Encrypt and temp store password for auto-logins after reboot
Update-ExecutionPolicy Unrestricted # allows powershell to run scripts
Disable-UAC # Temporarily disable UAC so it does not get in the way during the execution of script
DISM /Online /Enable-Feature /FeatureName:NetFx3 /All
Set-ExplorerOptions -showHidenFilesFoldersDrives -showProtectedOSFiles -showFileExtensions -DisableOpenFileExplorerToQuickAccess -DisableShowRecentFilesInQuickAccess -DisableShowFrequentFoldersInQuickAccess # configures windows explorer.
Enable-RemoteDesktop -DoNotRequireUserLevelAuthentication # enable remote desktop
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Carbon))
{
Try
    {
        Install-module Carbon -Force -ErrorAction Stop
    }
Catch
    {
        Install-module Carbon -Force -AllowClobber
}
}
Else
{
	Update-Module Carbon -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PSWindowsUpdate))
{
Try
    {
        Install-module PSWindowsUpdate -Force -ErrorAction Stop
    }
Catch
    {
        Install-module PSWindowsUpdate -Force -AllowClobber
}
}
Else
{
	Update-Module PSWindowsUpdate -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\CustomizeWindows10))
{
Try
    {
        Install-module CustomizeWindows10 -Force -ErrorAction Stop
    }
Catch
    {
        Install-module CustomizeWindows10 -Force -AllowClobber
}
}
Else
{
	Update-Module CustomizeWindows10 -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Autobrowse))
{
Try
{
	Install-Module Autobrowse -Force -ErrorAction Stop
}
Catch
{
	Install-Module Autobrowse -Force -AllowClobber
}
}
Else
{
	Update-Module Autobrowse -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Patchy))
{
Try
{
	Install-Module Patchy -Force -ErrorAction Stop
}
Catch
{
	Install-Module Patchy -Force -AllowClobber
}
}
Else
{
	Update-Module Patchy -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\SecureSettings))
{
Try
{
	Install-Module SecureSettings -Force -ErrorAction Stop
}
Catch
{
	Install-Module SecureSettings -Force -AllowClobber
}
}
Else
{
	Update-Module SecureSettings -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Winformal))
{
Try
{
	Install-Module Winformal -Force -ErrorAction Stop
}
Catch
{
	Install-Module Winformal -Force -AllowClobber
}
}
Else
{
	Update-Module Winformal -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\WinformPS))
{
Try
{
	Install-Module WinformPS -Force -ErrorAction Stop
}
Catch
{
	Install-Module WinformPS -Force -AllowClobber
}
}
Else
{
	Update-Module WinformPS -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\ShowUI))
{
Try
{
	Install-Module ShowUI -Force -ErrorAction Stop
}
Catch
{
	Install-Module ShowUI -Force -AllowClobber
}
}
Else
{
	Update-Module ShowUI -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\IsePackV2))
{
Try
{
	Install-Module IsePackV2 -Force -ErrorAction Stop
}
Catch
{
	Install-Module IsePackV2 -Force -AllowClobber
}
}
Else
{
	Update-Module IsePackV2 -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\TaskScheduler))
{
Try
{
	Install-Module TaskScheduler -Force -ErrorAction Stop
}
Catch
{
	Install-Module TaskScheduler -Force -AllowClobber
}
}
Else
{
	Update-Module TaskScheduler -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\MenuShell))
{
Try
{
	Install-Module MenuShell -Force -ErrorAction Stop
}
Catch
{
	Install-Module MenuShell -Force -AllowClobber
}
}
Else
{
	Update-Module MenuShell -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\CodeCraft))
{
Try
{
	Install-Module CodeCraft -Force -ErrorAction Stop
}
Catch
{
	Install-Module CodeCraft -Force -AllowClobber
}
}
Else
{
	Update-Module CodeCraft -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PinnedItem))
{
Try
{
	Install-Module PinnedItem -Force -ErrorAction Stop
}
Catch
{
	Install-Module PinnedItem -Force -AllowClobber
}
}
Else
{
	Update-Module PinnedItem -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Azurerm))
{
Try
{
	Install-module Azurerm -Force -ErrorAction Stop
}
Catch
{
	Install-module Azurerm -Force -AllowClobber
}
}
Else
{
	Update-Module Azurerm -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PowerShellGet))
{
Try
{
	Install-module PowerShellGet -Force -ErrorAction Stop
}
Catch
{
	Install-module PowerShellGet -Force -AllowClobber
}
}
Else
{
	Install-Module PowerShellGet -force -allowclobber
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\AzureAD))
{
Install-module AzureAD -AllowClobber -force
}
Else
{
update-module AzureAD -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\MSonline))
{
Install-module MSonline -AllowClobber -force
}
Else
{
update-module MSonline -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\powershellise-preview))
{
Install-module powershellise-preview -AllowClobber -force
}
Else
{
update-module powershellise-preview -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\psdepend))
{
Install-module psdepend -AllowClobber -force
}
Else
{
update-module psdepend -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\ISEsteroids))
{
Install-module ISEsteroids -AllowClobber -force
}
Else
{
update-module ISEsteroids -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\OutlookConnector))
{
Install-module OutlookConnector -AllowClobber -force
}
Else
{
update-module OutlookConnector -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PoshRSJob))
{
Install-module PoshRSJob -AllowClobber -force
}
Else
{
update-module PoshRSJob -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\SecretServer))
{
Install-module SecretServer -AllowClobber -force
}
Else
{
update-module SecretServer -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\WFTools))
{
Install-module WFTools -AllowClobber -force
}
Else
{
update-module WFTools -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PSSlack))
{
Install-module PSSlack -AllowClobber -force
}
Else
{
update-module PSSlack -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PSDeploy))
{
Install-module PSDeploy -AllowClobber -force
}
Else
{
update-module PSDeploy -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\BuildHelpers))
{
Install-module BuildHelpers -AllowClobber -force
}
Else
{
update-module BuildHelpers -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Exchange_AddIn))
{
Install-Module Exchange_AddIn -AllowClobber -force
}
Else
{
update-module Exchange_AddIn -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\Azure))
{
Install-Module Azure -AllowClobber -force
}
Else
{
update-module Azure -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\SharePointPnPPowerShellOnline))
{
Install-Module SharePointPnPPowerShellOnline -AllowClobber -force
}
Else
{
update-module SharePointPnPPowerShellOnline
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\PSSudo))
{
Install-Module PSSudo -AllowClobber -force
}
Else
{
update-module PSSudo -force
}
If (!(Test-Path $env:ProgramFiles\WindowsPowershell\Modules\SpeculationControl))
{
Install-Module SpeculationControl -AllowClobber -force
}
Else
{
update-module SpeculationControl -force
}
net user Administrator Rayman12 /ACTIVE:YES
Install-Module Packagemanagement -AllowClobber -Force
Install-script connect-o365 -force -confirm:$false
Show-DesktopIcons
choco install sysinternals -y
choco install lockhunter -y
choco install sudo -y
choco install nodist -y
choco install duck -y
choco install wget -y
choco install youtube-dl -y
choco install conemu -y
